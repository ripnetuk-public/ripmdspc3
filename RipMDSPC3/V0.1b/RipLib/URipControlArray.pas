unit URipControlArray;

interface
uses comCtrls, sysUtils, classes, forms, graphics, stdCtrls, extCtrls, controls, URipStrip;

type TRipControlArray = class(TPanel)
  private
    FControls: TStrings;
    FControlList: TStringList;
    FControlX: integer;
    FBorder: integer;
    FControlSpacing: integer;
    procedure ControlsChange(Sender: TObject);
    procedure createControls;
    procedure freeControls;
    function getValue(strTag: String): String;
    function getControl(strTag: String): TEdit;
    procedure setValue(strTag: String; const Value: String);
  protected
    procedure resize; override;
  public
    constructor Create(AOwner: TComponent); override;
    procedure sitOn(APanel: TPanel);
    property Controls: TStrings read FControls;
    destructor destroy; override;
    property Values[strTag: String]:String read getValue write setValue;
    procedure readFromRipParams;
end;

implementation
uses URipPArams;
{ TRipControlArray }

procedure TRipControlArray.ControlsChange(Sender: TObject);
begin
  FControlX := 0;
  createControls;
end;

constructor TRipControlArray.Create(AOwner: TComponent);
begin
  inherited;
  FBorder := 4;
  FControlSpacing := 22;
  FControls := TStringList.create;
  TStringList(FControls).onChange := ControlsChange;
  FControlList := TStringList.create;
  bevelInner := bvNone;
  bevelOuter := bvNone;
end;

destructor TRipControlArray.destroy;
begin
  FControls.free;
  inherited;
end; {destroy}

procedure TRipControlArray.sitOn(APanel: TPanel);
begin
  parent := APanel;
  align := alClient;
end;

procedure TRipControlArray.freeControls;
begin
  while FControlList.count > 0 do
  begin
    TObject(FControlList.objects[0]).free;
    FControlList.delete(0);
  end; {while}
end; {destroyControls}

procedure TRipControlArray.createControls;
var i:integer;
    newLabel: TLabel;
    newControl: TEdit;
    s: String;
    bolReDo: boolean;
    strTag: String;
    strCaption: String;
begin
  freeControls;
  bolReDo := false;
  for i := 0 to Controls.count-1 do
  begin
    s := controls[i];
    strTag := stripSemiList(s);
    strCaption := stripSemiList(S);
    newControl := TEdit.create(self);
    newControl.parent := self;
    newControl.setBounds(FControlX+8, i*22+4, Width - FControlX-12, 22);
    newControl.showHint := true;
    newControl.hint := strTag;
    newLabel := TLabel.create(newControl);
    newLabel.autoSize := true;
    newLabel.Caption := strCaption;
    newLabel.focusControl := newControl;
    if newLabel.width > FControlX then
    begin
      FControlX := newLabel.width;
      bolReDo := true;
    end; {if}
    newLabel.parent := self;
    newLabel.setBounds(4,i*22+4,FControlX, 22);
    FControlList.addObject(strTag, newControl);
  end; {for}
  if bolReDo then
  begin
    freeControls;
    createControls;
  end; {if}
end; {createControls}
procedure TRipControlArray.resize;
begin
  inherited;
  FControlX := 0;
  createControls;
end;

function TRipControlArray.getControl(strTag: String):TEdit;
var idx: integer;
begin
  idx := FControlList.indexOf(strTag);
  if idx = -1 then
    raise exception.create('No such tag.'+strTag);
  result := TEdit(FCOntrolList.objects[idx]);
end; {getControl}

function TRipControlArray.getValue(strTag: String): String;
begin
  result := getControl(strTag).Text;
end;

procedure TRipControlArray.setValue(strTag: String; const Value: String);
var c:Tedit;
begin
  c := getControl(strTag);
  c.Text := Value;
end;

procedure TRipControlArray.readFromRipParams;
var i:integer;
    s: String;
begin
  for i := 0 to FControlList.count-1 do
  begin
    s := RipParams.values[FControlList[i]];
    if s <> '' then
      values[FControlList[i]] := s;
  end; {for}
end;

end.
