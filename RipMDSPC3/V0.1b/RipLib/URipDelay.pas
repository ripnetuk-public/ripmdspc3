unit URipDelay;

interface

procedure ripDelay(ms: cardinal);

implementation

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Grids, StdCtrls, URipPort, ComCtrls, ExtCtrls;

procedure ripDelay(ms: cardinal);
var tick: cardinal;
begin;
  tick := getTickCount + ms;
  repeat
    application.processMessages;
  until getTickCount >= tick;
end; {ripDelay}


end.
