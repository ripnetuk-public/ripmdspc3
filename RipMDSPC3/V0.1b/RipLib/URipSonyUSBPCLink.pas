unit URipSonyUSBPCLink;

interface
uses SysUtils, classes, extCtrls;

const SONY_GUID: TGUID = '{A5917DC1-F0EC-11D3-B44D-00104B9B53FE}';
      READ_INTERVAL = 10;
      RIP_TIMEOUT = 8000; {2s}

type TRipUSBPacket = class
  private
    function getAsString: String;
  protected
    function getDebugInfo: String; virtual;
    function extractString(intStart, intEnd: integer):String;
    procedure setSpecificFieldsFromBuf; virtual; {Called on sibclass object after a packet is received and a specific sub class is created. Only override in recv packets }
    procedure setBufFromSpecificFields; virtual; {Called before sendPacket - does nothing in base TPacket, only override in send packets. }
  public
    buf: array[0..128] of byte;
    size: cardinal;
    constructor CreateFromPacket(APacket: TRipUSBPacket);
    procedure setBuf(aBuf: array of byte);
    property asString: String read getAsString;
    property debugInfo: String read getDebugInfo;
end; {type}

type TRipUSBPacketClass = class of TRipUSBPacket;

type TRPRecvUnknown = class(TRipUSBPAcket);

type
  TRipSonyUSBPCLink = class;

  TPacketReceivedEvent = procedure(Sender: TRipSonyUSBPCLink; APacket: TRipUSBPacket) of object;

  TRipSonyUSBPCLink = class(TComponent)
  private
    FReadPacketBuffer: TRipUSBPacket;
    FTimer: TTimer;
    FDeviceFileName: String;
    FHandleRead: integer;
    FHandleWrite: integer;
    FOnPacketReceived: TPacketReceivedEvent;
    function getDeviceFileName: String;
    procedure TimerTimer(Sender: TObject);
    procedure ReadPacket; {Private as set off automatically by timer}
  public
    constructor Create(AOwner: TComponent); override;
    destructor destroy; override;
    property DeviceFileName: String read getDeviceFileName;
    property HandleRead: integer read FHandleRead;
    property HandleWrite: integer read FHandleWrite;
    procedure openFiles;
    procedure closeFiles;
    function sendPacket(APacket: TRipUSBPacket): cardinal;
    function sendPacketAndWaitFor(APacket: TRipUSBPacket; WaitForClass: TRipUSBPacketClass): cardinal;
    function sendPacketAndWaitForAndFreeIt(APacket: TRipUSBPacket; WaitForClass: TRipUSBPacketClass): cardinal;    
    function sendPacketAndFreeIt(APacket: TRipUSBPacket): cardinal;
  protected
    FLastReceivedPacket: TObject;  
    procedure doOnPacketReceived(APacket: TRipUSBPacket); virtual;
    procedure createSpecificPacketObject(APacket: TRipUSBPacket; var newSpecificPacket: TRipUSBPacket); virtual;{Will take a packet, and if recognised will return a new instance of a specific subclass. The new object will be freed by this instance of TRipSonyUSBLink}
  published
    property OnPacketReceived: TPacketReceivedEvent read FOnPacketReceived write FOnPacketReceived;
end; {type}

implementation
uses SetupAPI, Windows;

{ TRipSonyUSBPCLink }

function GetRegistryProperty(PnPHandle: HDEVINFO; var DevData: TSPDevInfoData; ClassID: DWORD): string;
var
  BytesReturned: DWORD;
  RegData:       DWORD;
  Buffer:        array [0..256] of Char;
begin
  BytesReturned := 0;
  RegData       := REG_SZ;
  Buffer[0]     := #0;
  SetupDiGetDeviceRegistryProperty(PnPHandle, DevData, ClassID,
    @RegData, @Buffer[0], SizeOf(Buffer), @BytesReturned);
  Result := Buffer;
end;

procedure TRipSonyUSBPCLink.closeFiles;
begin
  if FHandleRead <> -1 then
    fileClose(FHandleRead);
  if FHandleWrite <> -1 then
    fileClose(FHandleWrite);

  FHandleRead := -1;
  FHandleWrite := -1;
end;

constructor TRipSonyUSBPCLink.Create(AOwner: TComponent);
begin
  inherited;
  FHandleRead := -1;
  FHandleWrite := -1;
  FReadPacketBuffer := TRipUSBPacket.create;
  FTimer := TTimer.create(self);
  FTimer.onTimer := TimerTimer;
  FTimer.interval := READ_INTERVAL;
  FTimer.enabled := true;
end;


procedure TRipSonyUSBPCLink.createSpecificPacketObject(APacket: TRipUSBPacket; var newSpecificPacket: TRipUSBPacket);
begin
  { Dummy}
end;

destructor TRipSonyUSBPCLink.destroy;
begin
  FTimer.enabled := false;
  closeFiles;
  FReadPacketBuffer.free;  
  inherited;
end;

procedure TRipSonyUSBPCLink.doOnPacketReceived(APacket: TRipUSBPacket);
begin
  if assigned(OnPacketReceived) then
    OnPacketReceived(self, APAcket);
end;

function TRipSonyUSBPCLink.getDeviceFileName: String;
var PnPHandle:               HDEVINFO;
    DevData:                 TSPDevInfoData;
    DeviceInterfaceData:     TSPDeviceInterfaceData;
    FunctionClassDeviceData: PSPDeviceInterfaceDetailData;
    Success:                 LongBool;
    Devn:                    Integer;
    BytesReturned:           DWORD;
    AGUID: TGUID;
begin
  if FDeviceFileName <> '' then
  begin {Already know it...}
    result := FDeviceFileName;
    exit;
  end; {if}
  result := '';
  AGUID := SONY_GUID;
  // Get a handle for the Plug and Play node and request currently active HID devices
  PnPHandle := SetupDiGetClassDevs(@aGUID {@FHidGuid - GUID}, nil {Enumerator}, 0 {handle to top level window?}, DIGCF_DEVICEINTERFACE or DIGCF_PRESENT{ or DIGCF_DEVICEINTERFACE});
  { PnPHandle is   said to be device information set. }
  if PnPHandle = Pointer(INVALID_HANDLE_VALUE) then
    raise exception.create('Call to getClassDevs fails. Could not find device with Sony GUID.');

  Devn := 0;
  repeat
    DeviceInterfaceData.cbSize := SizeOf(TSPDeviceInterfaceData);
    // Is there a Sony PCLink device at this table entry?
    Success := SetupDiEnumDeviceInterfaces(PnPHandle {device info set}                              , nil {optional constraint }, SONY_GUID {Interface class GUID}, Devn, {index}     DeviceInterfaceData {buffer for returned info});
    if Success then
    begin
      DevData.cbSize := SizeOf(DevData);
      BytesReturned  := 0;
      SetupDiGetDeviceInterfaceDetail(PnPHandle, @DeviceInterfaceData, nil, 0, @BytesReturned, @DevData);
      if (BytesReturned <> 0) and (GetLastError = ERROR_INSUFFICIENT_BUFFER) then
      begin
        FunctionClassDeviceData := AllocMem(BytesReturned);
        FunctionClassDeviceData.cbSize := 5;
        if SetupDiGetDeviceInterfaceDetail(PnPHandle, @DeviceInterfaceData, FunctionClassDeviceData, BytesReturned, @BytesReturned, @DevData) then
        begin
          // create HID device object and add it to the device list
//          FLog.log('DevicePath='+(PChar(@FunctionClassDeviceData.DevicePath)));
          result := PChar(@FunctionClassDeviceData.DevicePath);
//          FLog.log('DevInst='+intToStr(DevData.DevInst));
//          FLog.log('Device Desc= '+GetRegistryProperty(PnPHandle, DevData, SPDRP_DEVICEDESC));
//          FLog.log('DEVICE Class= '+GetRegistryProperty(PnPHandle, DevData, SPDRP_CLASS));
//          fLog.log('ok');
          Inc(Devn);
        end;
        FreeMem(FunctionClassDeviceData);
      end;
    end;
  until not Success;
  SetupDiDestroyDeviceInfoList(PnPHandle);
  FDeviceFileName := result; {Store for next time}
end;


procedure TRipSonyUSBPCLink.openFiles;
  function doOpen(strName: String):integer;
  begin
    result := CreateFile(PChar(strName), GENERIC_READ or GENERIC_WRITE, FILE_SHARE_READ or FILE_SHARE_WRITE, nil, OPEN_EXISTING, 0, 0);
  end; {doOpen}
begin
  if FHandleRead = -1 then
  begin {Not already open}
    FHandleRead := doOpen(DeviceFileName+'\if000pipe001');
    if FHandleRead = -1 then
      raise exception.create('Failed to open read pipe.');
  end; {if}
  if FHandleWrite = -1 then
  begin {Not alreayd opyen}
    FHandleWrite := doOpen(DeviceFileName+'\if000pipe000');
    if FHandleWrite = -1 then
      raise exception.create('Failed to open write pipe.');
  end; {if}
end;


procedure TRipSonyUSBPCLink.ReadPacket;
var toRead, bytesRead: integer;
    specificPacket: TRipUSBPacket;
begin
  { Free up any previous read packets in lastReceivedPacket}
  if FLastReceivedPAcket <> nil then
  begin
    FLastReceivedPAcket.free;
    FLastReceivedPAcket := nil;
  end; {if}
  { Attempt to read $40 bytes }
  toRead := $40;
  bytesRead := FileRead(HandleRead, FReadPacketBuffer.buf, toRead);
  if bytesRead < 1 then
    exit; {Read nothing}
  { Fill in size ready to pass to events }
  FReadPacketBuffer.size := bytesRead;
  { Try and let a subclass of self create a specific subclass of TRipUSBPacket}
  specificPacket := nil;
  createSpecificPacketObject(FReadPAcketBuffer, specificPacket);
  if specificPacket = nil then {Send an unknown packet...}
    specificPacket := TRPRecvUnknown.createFromPacket(FReadPAcketBuffer);

  specificPacket.setSpecificFieldsFromBuf; {Fill in fields in subclass}

  doOnPacketReceived(specificPacket);
  
  { Store for blocking commands}
  FLastReceivedPacket := specificPacket;
end;

function TRipSonyUSBPCLink.sendPacket(APacket: TRipUSBPacket): cardinal;
var bytesWrote: cardinal;
begin
  { Open if needed}
  openFiles;
  APacket.setBufFromSpecificFields;
  Windows.WriteFile(HandleWrite, APacket.buf[0], APacket.size, BytesWrote, nil);
    if bytesWrote = 0 then
      RaiseLastWin32Error;
  result := bytesWrote;
end;

function TRipSonyUSBPCLink.sendPacketAndFreeIt(
  APacket: TRipUSBPacket): cardinal;
begin
  result := sendPacket(APAcket);
  APacket.free;
end;

function TRipSonyUSBPCLink.sendPacketAndWaitFor(APacket: TRipUSBPacket;
  WaitForClass: TRipUSBPacketClass): cardinal;
var startTime: cardinal;
begin
  FTimer.enabled := false;
  try
    result := sendPacket(APacket);
    { Wait for a packet of class WaitForClass }
    startTime := getTickCount;
    FLastReceivedPacket := nil;
    repeat
      readPacket;
      if FLastReceivedPacket is WaitForClass then
        exit; {Got it}
      sleep(100); {Wait for 1/10 sec}
    until (getTickCount > startTime+RIP_TIMEOUT);
  finally
    FTimer.enabled := true;
  end; {try}
  raise exception.create('Timeout waiting for '+WaitForClass.className);
end;

function TRipSonyUSBPCLink.sendPacketAndWaitForAndFreeIt(
  APacket: TRipUSBPacket; WaitForClass: TRipUSBPacketClass): cardinal;
begin
  result := sendPacketAndWaitFor(APacket, WaitForClass);
  APacket.free;
end;

procedure TRipSonyUSBPCLink.TimerTimer(Sender: TObject);
begin
  FTimer.enabled := false;
  try
    if handleRead <> -1 then {File is open}
      readPacket;
  finally
    FTimer.enabled := true;
  end; {try}
end;

{ TRipUSBPacket }

constructor TRipUSBPacket.CreateFromPacket(APacket: TRipUSBPacket);
var i:integer;
begin
  inherited;
  Size := APAcket.size;
  for i := 0 to Size-1 do
    buf[i] := APAcket.buf[i];
end;

function TRipUSBPacket.extractString(intStart: integer; intEnd: integer): String;
var i:cardinal;
begin
  result := '';
  for i := intStart to intEnd do
    if i < size then
      if buf[i] in [32..126] then
        result := result + chr(buf[i]);
end;

function TRipUSBPacket.getAsString: String;
var i:integer;
    c: char;
    ascData: String;
begin
  result := '';
  ascData := '';
  for i := 0 to size-1 do
  begin
    if result <> '' then
      result := result + ' ';
    result := result + intToHEx(buf[i],2);
    c := chr(buf[i]);
    if not (c in ['a'..'z', 'A'..'Z', '0'..'9']) then
      c := '.';
    ascData := ascData + c;
  end; {for}
  result := result + ' '+ascData;
end;

function TRipUSBPacket.getDebugInfo: String;
begin
{dummy}
end;

procedure TRipUSBPacket.setBuf(aBuf: array of byte);
var idx: integer;
    i: integer;
begin
  idx := 0;
  for i := low(aBuf) to high(aBuf) do
  begin
    buf[idx] := aBuf[i];
    inc(idx);
  end; {for}
  size := idx;
end;

procedure TRipUSBPacket.setBufFromSpecificFields;
begin
{dummy}
end;

procedure TRipUSBPacket.setSpecificFieldsFromBuf;
begin
{dummy}
end;

end.
