unit URipBoolToStr;

interface

function boolToStr(Value: Boolean):String;
function strToBool(Value: String):Boolean;

implementation

function boolToStr(Value: Boolean):String;
begin
  result := '1';
  if not value then
    result := '0';
end; {boolToStr}

function strToBool(Value: String):Boolean;
begin
  result := value = '1';
end; {strToBool}
end.
