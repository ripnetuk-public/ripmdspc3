unit URipMDSPC3;

interface
uses URipSonyUSBPCLink, SysUtils, classes, controls;


type TTrackRec = record
  Title: String;
  Mins: integer;
  Secs: integer;
end; {TTrackRec}

type TRipMDSPC3 = class(TRipSonyUSBPCLink)
  private
    FTracks: array[0..99] of TTrackRec;
    FTrackCount: integer;
    function getTrack(idx: integer): TTrackRec;
  protected
    procedure createSpecificPacketObject(APacket: TRipUSBPacket; var newSpecificPacket: TRipUSBPacket); override;
    procedure doOnPacketReceived(APacket: TRipUSBPacket); override;
  public
    procedure readTOC;
    property Track[idx: integer]: TTrackRec read getTrack;
    property TrackCount: integer read FTRackCount;
    procedure sendTitle(ATrackNo: integer; ATitle: String);
end; {type}

implementation
uses URipMDSPC3RecvPackets, URipMDSPC3SendPackets, forms;

{ TRipMDSPC3 }

function getPacketInstructionCode(APacket: TRipUSBPacket): integer;
begin
  result := -1;
  if APacket.size < 5 then
    exit; {Too small}
  if APacket.size <> APacket.buf[0]+1 then
    exit; {Invalid size byte}
  with APacket do
    if (buf[1] <> 0) or (buf[2] <> $12) or (buf[3] <> $B8) then
      exit; {Mismatch}
  result := APacket.buf[4];
end; {getPacketInstructionCode}

procedure TRipMDSPC3.createSpecificPacketObject(APacket: TRipUSBPacket;  var newSpecificPacket: TRipUSBPacket);
var instructionCode: integer;
    newClass: TRipUSBPacketClass;
begin
  inherited;
  instructionCode := getPacketInstructionCode(APacket);
  if instructionCode < 0 then
    exit;

  newClass := nil;
  case instructionCode of
    $00: newClass := TRPRecvCmdPlayOrUnpause;
    $02: newClass := TRPRecvCmdStopOrPause;
    $03: newClass := TRPRecvCmdEject;
    $0E: newClass := TRPRecvError;
    $45: newClass := TRPRecvGetTrackLength;
    $48: newClass := TRPRecvGetFullTitle;
    $50: newClass := TRPRecvTrackChange;
    $51: newClass := TRPRecvPlayTime;
    $6A: newClass := TRPRecvIDHardware;
    $70: newClass := TRPRecvCurrentTrackInfo;
    $71: newClass := TRPRecvDiscInfo;
    $98: newClass := TRPRecvTitle;
    $C1: newClass := TRPRecvLCDMessage;
    $D8: newClass := TRPRecvCmdPowerOff;
    $E8: newClass := TRPRecvTrackShortTitle;
    $EF: newClass := TRPRecvCmdPowerOn;
    $F0: newClass := TRPRecvResponseToInitial;
  end; {case}

  if newClass <> nil then
    newSpecificPacket := newClass.CreateFromPacket(APacket);
end;

{ TRPRecvTrackInfo }


procedure TRipMDSPC3.doOnPacketReceived(APacket: TRipUSBPacket);
begin
  inherited;
  { Stash the information if interesting... }
  if APacket is TRPRecvDiscInfo then
    with TRPRecvDiscInfo(APacket) do
      self.FtrackCount := TrackCount;

  if APacket is TRPRecvGetTrackLength then
    with TRPRecvGetTrackLength(APacket) do
    begin
      FTracks[TrackNo].Mins := Mins;
      FTracks[TrackNo].Secs := Secs;
    end; {with}

  if APacket is TRPRecvGetFullTitle then
    with TRPRecvGetFullTitle(APacket) do
    begin
      if ChunkIndex = 1 then
        FTracks[TrackNo].Title := Title      
      else
        FTracks[TrackNo].Title := FTracks[TrackNo].Title + Title;
    end; {with}
end;

function TRipMDSPC3.getTrack(idx: integer): TTrackRec;
begin
//  if (idx<0) or (idx > FTrackCount) then
//    raise exception.create('Track out of range');
  result := FTracks[idx];
end;

procedure TRipMDSPC3.readTOC;
var p: TRipUSBPacket;
    i, iChunk: integer;
begin
  screen.cursor := crHourglass;
  { Send a get disc info packet, and wait for a response... }
  p := TRPSendGetDiscInfo.create;
  sendPacketAndWaitFor(p, TRPRecvDiscInfo);
  p.free;
  for i := 0 to FTrackCount do {do 0 to include disc}
  begin
    if i > 0 then
    begin {Dont want length for disc (ie unavailable)}
      { Get track length }
      p := TRPSendGetTrackLength.create;
      with TRPSendGetTrackLength(p) do
        trackNo := i;
      sendPacketAndWaitFor(p, TRPRecvGetTrackLength);
      p.free;
    end; {if}
    { Get full title }
    p := TRPSendGetFullTitle.create;
    iChunk := 1;
    repeat
      with TRPSendGetFullTitle(p) do
      begin
        trackNo := i;
        chunkIndex := iChunk;
        sendPacketAndWaitFor(p, TRPRecvGetFullTitle);
        inc(iChunk);
      end; {with}
    until length(TRPRecvGetFullTitle(FLastReceivedPacket).Title) = 0;
    p.free;
  end; {for}
  screen.cursor := crDefault;  
end;

{
Known packets to send:

Initial  $00, $60, $00

$01 TRPSendStop
$03 TRPSendPause
$04 TRPSendEject
$08 TRPSendNextTrack
$09 TRPSendPrevTrack
$0B TRPSendFade
$0E TRPSendGetDiscInfo
$0F TRPSendGetCurrentTrackInfo
$12 TRPSendStartFF
$13 TRPSendStartRew
$1f TRPSendStopRewOrFF
$2F TRPSendPowerOff
$45 TRPSendGetTrackLength
$48 TRPSendGetFullTitle
$50 TRPSendPlay
$6A TRPSendIDHardware







Known packets to receive

    $00: newClass := TRPRecvCmdPlayOrUnpause;
    $02: newClass := TRPRecvCmdStopOrPause;
    $03: newClass := TRPRecvCmdEject;
    $0E: newClass := TRPRecvError;
    $45: newClass := TRPRecvGetTrackLength;
    $48: newClass := TRPRecvGetFullTitle;
    $50: newClass := TRPRecvTrackChange;
    $51: newClass := TRPRecvPlayTime;
    $6A: newClass := TRPRecvIDHardware;
    $70: newClass := TRPRecvCurrentTrackInfo;
    $71: newClass := TRPRecvDiscInfo;
    $C1: newClass := TRPRecvLCDMessage;
    $D8: newClass := TRPRecvCmdPowerOff;
    $E8: newClass := TRPRecvTrackShortTitle;
    $EF: newClass := TRPRecvCmdPowerOn;
    $F0: newClass := TRPRecvResponseToInitial;

}

procedure TRipMDSPC3.sendTitle(ATrackNo: integer; ATitle: String);
var p: TRPSendTitle;
    strChunk: String;
begin
  p := TRPSendTitle.create;
  p.TrackNo := ATrackNo;
  p.chunkIndex := 1;
  repeat
    strChunk := copy(ATitle, 1, 16); {Get next 15 chars}
    ATitle := copy(ATitle, 17, length(ATitle));
    p.title := strChunk;
    sendPacketAndWaitFor(p, TRPRecvTitle);
    p.chunkIndex := p.chunkIndex + 1;
  until length(ATitle)=0;
  p.free;
end;

end.

