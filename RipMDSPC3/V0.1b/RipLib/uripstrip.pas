unit URipStrip;

interface

function stripList(var s:String; strSeperator: String): String;
function stripSpaceList(var s:STring):String;
function stripSemiList(var s:STring):String;

implementation

function stripList(var s:String; strSeperator: String): String;
var p:integer;
begin;
  p := pos(strSeperator, s);
  if (p=0) then
  begin; {Not found}
    result := s;
    s := '';
    exit;
  end; {if}
  result := copy(s, 1, p-1);
  s := copy(s, p+length(strSeperator), length(s));
end; {stripList}

function stripSpaceList(var s:STring):String;
begin;
  result := stripList(s, ' ');
end; {stripCommaList}

function stripSemiList(var s:STring):String;
begin;
  result := stripList(s, ';');
end; {stripCommaList}

end.
