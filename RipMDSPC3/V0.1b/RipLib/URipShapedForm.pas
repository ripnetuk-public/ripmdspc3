unit URipShapedForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, menus;

type TRipShapedFormPoint = class(TShape)
  private
    function getX: integer;
    function getY: integer;
    procedure setX(Value: integer);
    procedure setY(Value: integer);
    function xOffset: integer;
    function yOffset: integer;
    function createMenu: TPopupMenu;
  public
    constructor Create(AOwner: TComponent); override;
    procedure position(newX,newY: integer);
    property x: integer read getX write setX;
    property y: integer read getY write setY;
  protected

end; {TRipShapedFormPoint}

type
  TRipShapedForm = class(TCustomControl)
  private
    FPointList: TList;
    function getPoint(idx: integer):TRipShapedFormPoint;
    function getPointCount: integer;
  protected
    function addPoint: TRipShapedFormPoint;
    procedure paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Points[idx: integer]: TRipShapedFormPoint read getPoint;
    property PointCount: integer read getPointCount;
  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('RipNet', [TRipShapedForm]);
end;

{ TRipFormShaper }

function TRipShapedForm.addPoint: TRipShapedFormPoint;
begin
  result := TRipShapedFormPoint.create(self);
//  result.parent := self;
  FPointList.add(result);
end;

constructor TRipShapedForm.Create(AOwner: TComponent);
begin
  inherited;
  FPointList := TList.create;
  align := alClient;
  addPoint.position(5,5);
  addPoint.position(200,5);
  addPoint.position(200,200);
  addPoint.position(250,150);
  addPoint.position(5,200);
end;

destructor TRipShapedForm.Destroy;
begin
  inherited;
  FPointList.free;
end;

{ TRipShapedFormPoint }

constructor TRipShapedFormPoint.Create(AOwner: TComponent);
begin
  inherited;
  shape := stCircle;
  width := 20;
  height := 20;
  brush.color := clRed;
  dragMode := dmAutomatic;
  popupMenu := createMenu;
end;

function TRipShapedFormPoint.createMenu: TPopupMenu;
var mi: TMenuItem;
begin
  result := TPopupMenu.create(Self);
  mi := TMenuItem.create(result);
  mi.caption := 'test';
  result.items.add(mi);
end; {crateMenu}

function TRipShapedFormPoint.getX: integer;
begin
  result := left+xOffset;
end;

function TRipShapedFormPoint.getY: integer;
begin
  result := top+yOffset;
end;

procedure TRipShapedFormPoint.position(newx, newy: integer);
begin
  x := newX;
  y := newY;
end;

procedure TRipShapedFormPoint.setX(Value: integer);
begin
  left := Value - xOffset;
end;

procedure TRipShapedFormPoint.setY(Value: integer);
begin
  top := Value - yOffset;
end;

function TRipShapedFormPoint.xOffset:integer;
begin
  result := width div 2;
end; {xOffset}

function TRipShapedFormPoint.yOffset:integer;
begin
  result := height div 2;
end; {xOffset}

function TRipShapedForm.getPoint(idx: integer): TRipShapedFormPoint;
begin
  result := FPointList[idx];
end;

function TRipShapedForm.getPointCount: integer;
begin
  result := FPointList.count;
end;

procedure TRipShapedForm.paint;
var i:integer;
begin
  inherited;
  for i := 0 to pointCount-1 do
    canvas.lineTo(points[i].x, points[i].y);
end;

end.
