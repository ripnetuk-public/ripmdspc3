unit URipLog;

interface
uses comCtrls, sysUtils, classes, forms, graphics, stdCtrls, extCtrls, controls;

type TRipLog = class(TCustomRichEdit)
  private
    FDebugMode: boolean;
    FInitialLogWritten: boolean;
    function getLastLog: STring;
    function getLastLogStarting(strPattern: String): String;
  public
    constructor Create(AOwner: TComponent); override;
    procedure logHeader(s: String);
    procedure log(s: String; c:TCOlor = clBlack; aStyle: TFontStyles = []; aSize:integer = 10; aFont: String = 'Courier New'); overload;
    procedure log(s: TStrings; c:TCOlor = clBlack; aStyle: TFontStyles = []; aSize:integer = 10; aFont: String = 'Courier New'); overload;
    procedure logInfo(s: String);
    procedure logError(s: String);
    procedure logSuccess(s: String);
    procedure logDebug(s: String);
    procedure logHelp(s: String; aStyle: TFontStyles = []);
    procedure sitOn(APanel: TPanel);
    procedure ClearLog;
    property DebugMode: boolean read FDebugMode write FDebugMode;
    property LastLog: STring read getLastLog;
    property LastLogStarting[strPattern: String]: String read getLastLogStarting;
    procedure CreateWnd; override;
  protected
    procedure writeInitialLog; virtual; {override to provide readme etc}
end;

implementation

{ TRipLog }

procedure TRipLog.ClearLog;
begin
  lines.clear;
end;

constructor TRipLog.Create(AOwner: TComponent);
begin
  inherited;
  wordWrap := false;
  scrollBars := ssBoth;
  font.Name := 'arial';
  font.size := 8;
  readOnly := true;
  DebugMode := true;
end;

procedure TRipLog.log(s: String; c: TCOlor; aStyle: TFontStyles; aSize:integer; aFont: String);
begin
  selAttributes.color := c;
  selAttributes.Size := aSize;
  selAttributes.Style := aStyle;
  selAttributes.Name := aFont;
  lines.add(s);
  application.processMessages;
end;

function TRipLog.getLastLog: STring;
begin
  result := '';
  if lines.count = 0 then
    exit;
  result := lines[lines.count-1];
end;

procedure TRipLog.log(s: TStrings; c: TCOlor; aStyle: TFontStyles; aSize:integer; aFont: String);
var i:integer;
begin
  for i := 0 to s.count-1 do
    log(s[i], c, aStyle, aSize, aFont);
end;

procedure TRipLog.logDebug(s: String);
begin
  if FDebugMode then
    log(s, clBlue, [fsBold]);
end;

procedure TRipLog.logError(s: String);
begin
  log(s, clRed);
end;

procedure TRipLog.logHelp(s: String; aStyle: TFontStyles);
begin
  log(s, clGreen, aStyle);
end;

procedure TRipLog.logInfo(s: String);
begin
  log(s, clBlue);
end;

procedure TRipLog.logSuccess(s: String);
begin
  log(s, clGreen);
end;

procedure TRipLog.sitOn(APanel: TPanel);
begin
  parent := APanel;
  align := alClient;
end;

function TRipLog.getLastLogStarting(strPattern: String): String;
var idx: integer;
begin
  result := '';
  if lines.count = 0 then
    exit;
  for idx := lines.count-1 downto 0 do
  begin
    if pos(uppercase(strPattern), lines[idx]) = 1 then
    begin
      result := lines[idx];
      exit;
    end; {if}
  end; {for}
end;

procedure TRipLog.writeInitialLog;
begin
  {dummy}
end;



procedure TRipLog.CreateWnd;
begin
  inherited;
  if not FInitialLogWritten then
    writeInitialLog;
  FInitialLogWritten := true;
end;

procedure TRipLog.logHeader(s: String);
begin
  log(s , clBlack, [fsBold], 12, 'Ariel');
end;

end.
