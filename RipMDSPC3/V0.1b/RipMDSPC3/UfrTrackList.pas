unit UfrTrackList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, MemTable, Grids, Wwdbigrd, Wwdbgrid, DBCGrids, StdCtrls,
  Mask, DBCtrls, ExtCtrls, RxMemDS;

type
  TfrTrackList = class(TFrame)
    dsTracks: TDataSource;
    tblTracks: TRxMemoryData;
    MemoryTable1TrackName: TStringField;
    MemoryTable1LengthMins: TIntegerField;
    MemoryTable1LengthSecs: TIntegerField;
    tblTracksTrackNo: TIntegerField;
    dbGrid: TDBCtrlGrid;
    Shape1: TShape;
    pnlOnCtrlGrid: TPanel;
    dbeTrackNo: TDBText;
    dbeTrackName: TDBEdit;
    dbeMins: TDBText;
    dbeSecs: TDBText;
    Label2: TLabel;
    tblTracksSourceFilename: TStringField;
    tblTracksMP3Artist: TStringField;
    tblTracksMP3Album: TStringField;
    tblTracksMP3Title: TStringField;
    tblTracksMP3Track: TStringField;
    tblTracksMP3Bitrate: TStringField;
    procedure FrameResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.DFM}

procedure TfrTrackList.FrameResize(Sender: TObject);
begin
  dbGrid.RowCount := clientHeight div 20;
end;

end.
