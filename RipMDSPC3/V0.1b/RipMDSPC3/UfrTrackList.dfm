object frTrackList: TfrTrackList
  Left = 0
  Top = 0
  Width = 689
  Height = 400
  Color = clRed
  ParentColor = False
  TabOrder = 0
  OnResize = FrameResize
  object dbGrid: TDBCtrlGrid
    Left = 0
    Top = 0
    Width = 689
    Height = 400
    Align = alClient
    ColCount = 1
    Color = clBtnFace
    DataSource = dsTracks
    PanelBorder = gbNone
    PanelHeight = 20
    PanelWidth = 673
    ParentColor = False
    TabOrder = 0
    RowCount = 20
    SelectedColor = clYellow
    object Shape1: TShape
      Left = 0
      Top = 19
      Width = 673
      Height = 1
      Align = alBottom
    end
    object pnlOnCtrlGrid: TPanel
      Left = 0
      Top = 0
      Width = 673
      Height = 19
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object dbeTrackNo: TDBText
        Left = 8
        Top = 1
        Width = 33
        Height = 18
        Alignment = taRightJustify
        DataField = 'TrackNo'
        DataSource = dsTracks
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbeMins: TDBText
        Left = 544
        Top = 1
        Width = 26
        Height = 18
        Alignment = taRightJustify
        DataField = 'LengthMins'
        DataSource = dsTracks
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsItalic]
        ParentFont = False
      end
      object dbeSecs: TDBText
        Left = 576
        Top = 1
        Width = 26
        Height = 18
        DataField = 'LengthSecs'
        DataSource = dsTracks
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsItalic]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 572
        Top = 4
        Width = 3
        Height = 13
        Caption = ':'
      end
      object dbeTrackName: TDBEdit
        Left = 48
        Top = 1
        Width = 481
        Height = 18
        AutoSize = False
        BorderStyle = bsNone
        Ctl3D = False
        DataField = 'TrackName'
        DataSource = dsTracks
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = True
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object dsTracks: TDataSource
    DataSet = tblTracks
    Left = 48
    Top = 48
  end
  object tblTracks: TRxMemoryData
    Active = True
    FieldDefs = <
      item
        Name = 'TrackNo'
        DataType = ftInteger
      end
      item
        Name = 'TrackName'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'LengthMins'
        DataType = ftInteger
      end
      item
        Name = 'LengthSecs'
        DataType = ftInteger
      end
      item
        Name = 'SourceFilename'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'MP3Artist'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'MP3Album'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'MP3Title'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'MP3Track'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'MP3Bitrate'
        DataType = ftString
        Size = 255
      end>
    Left = 32
    Top = 40
    object tblTracksTrackNo: TIntegerField
      DisplayWidth = 10
      FieldName = 'TrackNo'
    end
    object MemoryTable1TrackName: TStringField
      DisplayWidth = 50
      FieldName = 'TrackName'
      Size = 255
    end
    object MemoryTable1LengthMins: TIntegerField
      DisplayWidth = 10
      FieldName = 'LengthMins'
    end
    object MemoryTable1LengthSecs: TIntegerField
      DisplayWidth = 10
      FieldName = 'LengthSecs'
    end
    object tblTracksSourceFilename: TStringField
      FieldName = 'SourceFilename'
      Size = 255
    end
    object tblTracksMP3Artist: TStringField
      FieldName = 'MP3Artist'
      Size = 255
    end
    object tblTracksMP3Album: TStringField
      FieldName = 'MP3Album'
      Size = 255
    end
    object tblTracksMP3Title: TStringField
      FieldName = 'MP3Title'
      Size = 255
    end
    object tblTracksMP3Track: TStringField
      FieldName = 'MP3Track'
      Size = 255
    end
    object tblTracksMP3Bitrate: TStringField
      FieldName = 'MP3Bitrate'
      Size = 255
    end
  end
end
