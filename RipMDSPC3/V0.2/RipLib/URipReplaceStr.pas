unit URipReplaceStr;

interface

function RipReplaceStr(s: String; searchFor: String; replaceWith: String):String;
implementation
uses sysutils, classes;

function RipReplaceStr(s: String; searchFor: String; replaceWith: String):String;
var p:integer;
begin
  result := s;
  repeat
    p := pos(uppercase(searchFor), uppercase(result));
    if p > 0 then
      result := copy(result, 1, p-1) + replaceWith + copy(result, p+length(searchFor), length(result)+length(searchFor));
  until p = 0;
end; {RipReplaceStr}

end.
