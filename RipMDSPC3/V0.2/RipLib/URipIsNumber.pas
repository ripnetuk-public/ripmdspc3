unit URipIsNumber;

interface

function RipIsNumber(s: String):boolean;

implementation

function RipIsNumber(s: String):boolean;
var i:integer;
begin
  result := true;
  for i := 1 to length(s) do
    if not (s[i] in ['0'..'9']) then
      result := false;
end; {RipISNumber}
end.
