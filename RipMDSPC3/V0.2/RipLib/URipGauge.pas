unit URipGauge;

interface
uses extctrls, sysutils, classes, dialogs;

const FrameDelay = 5;
type TSillyRec = record
  X: integer;
  Y: integer;
  XV: integer;
  YV: integer;
end; {type}

type TRipGauge = class(TPanel)
  public
    procedure animate;
    constructor Create(AOwner: TCOmponent); override;
  protected
    procedure paint; override;
  private
    FCountdown: integer;
    FSilly1: TSillyRec;
    FSilly2: TSillyRec;
    FSilly3: TSillyRec;
    procedure randomizeSillyRec(var ASillyRec: TSillyRec);
    procedure moveSillyRec(var ASillyRec: TSillyRec);
end; {TRipGauge}

implementation

{ TRipGauge }

procedure TRipGauge.animate;
begin
  dec(FCountdown);
  if (FCountDown > 0) then
    exit;
  FCountDown := FrameDelay;
  moveSillyRec(FSilly1);
  moveSillyRec(FSilly2);
  moveSillyRec(FSilly3);  
  repaint;
end;

procedure TRipGauge.randomizeSillyRec(var ASillyRec: TSillyRec);
begin
  ASillyRec.x := random(width-20);
  ASillyRec.y := random(height-20);
  ASillyRec.XV := random(3)-1;
  ASillyRec.YV := random(3)-1;
end; {randomizeSillyRec}

procedure TRipGauge.moveSillyRec(var ASillyRec: TSillyRec);
begin
  with ASIllyRec do
  begin
    x := x + xv;
    y := y + yv;
    if (x < 0) or (x > width) then
      xv := -xv;
    if (y < 0) or (y > height) then
      yv := -yv;
  end; {with}
end; {moveSillyRec}

constructor TRipGauge.Create(AOwner: TCOmponent);
begin
  inherited;
  randomizeSillyRec(FSilly1);
  randomizeSillyRec(FSilly2);
  randomizeSillyRec(FSilly3);
  color := 0;
//  showmessage('fake `am he hee');
end;

procedure TRipGauge.paint;
begin
  inherited;
  canvas.pen.Width := 4;
  canvas.pen.color := random($FFFFFF);
  canvas.moveTo(FSilly1.x,FSilly1.y);
  canvas.lineTo(FSilly2.x,FSilly2.y);
  canvas.lineTo(FSilly3.x,FSilly3.y);
  canvas.lineTo(FSilly1.x,FSilly1.y);
end;

end.
