unit URipFormSitOn;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, ExtCtrls, URipLog, UfrmRemoteControl, ComCtrls;


procedure RipFormSitOn(APanel: TPanel; AForm: TForm);

implementation

procedure RipFormSitOn(APanel: TPanel; AForm: TForm);
begin
  with AForm do
  begin
    parent := APAnel;
    align := alClient;
    borderIcons := [];
    borderStyle := bsNone;
    visible := true;
  end; {with}
end; {RipFormSitOn}

end.
