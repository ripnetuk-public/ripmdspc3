unit URipMDSPC3RecvPackets;

interface
uses URipSonyUSBPCLink, SysUtils, classes;

type TRPRecvPlayTime = class(TRipUSBPacket)
  public
    Mins: cardinal;
    Secs: cardinal;
  protected
    procedure setSpecificFieldsFromBuf; override;
    function getDebugInfo: String; override;
end; {type}

type TRPRecvTitle = class(TRipUSBPacket)
  public
    TrackNo: integer;
    ChunkIndex: integer;
  protected
    procedure setSpecificFieldsFromBuf; override;
    function getDebugInfo: String; override;
end; {type}



type TRPRecvCurrentTrackInfo = class(TRipUSBPacket)
  public
    TrackNo: Integer;
  protected
    procedure setSpecificFieldsFromBuf; override;
    function getDebugInfo: String; override;
end; {type}

type TRPRecvGetTrackLength = class(TRipUSBPacket)
  public
    TrackNo: Integer;
    Mins: cardinal;
    Secs: cardinal;
  protected
    procedure setSpecificFieldsFromBuf; override;
    function getDebugInfo: String; override;
end; {type}

type TRPRecvGetFullTitle = class(TRipUSBPacket)
  public
    Title: String;
    TrackNo: integer;
    ChunkIndex: integer;
  protected
    procedure setSpecificFieldsFromBuf; override;
    function getDebugInfo: String; override;
end; {type}

type TRPRecvIDHardware = class(TRipUSBPacket)
  public
    HardwareID: String;
  protected
    procedure setSpecificFieldsFromBuf; override;
    function getDebugInfo: String; override;
end; {type}

type TRPRecvTrackShortTitle = class(TRipUSBPacket)
  public
    TrackNo: integer;
    ShortTitle: String;
  protected
    procedure setSpecificFieldsFromBuf; override;
    function getDebugInfo: String; override;
end; {type}

type TRPRecvDiscInfo = class(TRipUSBPacket)
  public
    TrackCount: integer;
  protected
    procedure setSpecificFieldsFromBuf; override;
    function getDebugInfo: String; override;
end; {type}


type TRPRecvTrackChange = class(TRipUSBPacket)
  public
    TrackNo: integer;
    LengthMins: integer;
    LengthSecs: integer;
  protected
    procedure setSpecificFieldsFromBuf; override;
    function getDebugInfo: String; override;
end; {type}

type TRPRecvLCDMessage = class(TRipUSBPacket)
  public
    LCDMessage: String;
  protected
    procedure setSpecificFieldsFromBuf; override;
    function getDebugInfo: String; override;
end; {type}

type TRPRecvResponseToInitial = class(TRipUSBPacket);
type TRPRecvError = class(TRipUSBPacket);
type TRPRecvCmdPowerOff = class(TRipUSBPacket);
type TRPRecvCmdEject = class(TRipUSBPacket);
type TRPRecvCmdPowerOn  = class(TRipUSBPacket);
type TRPRecvCmdStopOrPause  = class(TRipUSBPacket);
type TRPRecvCmdPlayOrUnpause  = class(TRipUSBPacket);

type TRPRecvCmdRecordOrUnpause  = class(TRipUSBPacket);
type TRPRecvPauseRecording  = class(TRipUSBPacket);

type TRPRecv70  = class(TRipUSBPacket);

implementation

{ TRipMDSPC3 }

function TRPRecvTrackShortTitle.getDebugInfo: String;
begin
  result := 'Track Info: '+intToStr(TrackNo)+':'+ShortTitle;
end;

procedure TRPRecvTrackShortTitle.setSpecificFieldsFromBuf;
begin
  inherited;
  TrackNo := buf[7];
  ShortTitle := extractString(10, size-1);
end;

{ TRPRecvLCDMessage }

function TRPRecvLCDMessage.getDebugInfo: String;
begin
  result := 'LCD: '+LCDMessage;
end;

procedure TRPRecvLCDMessage.setSpecificFieldsFromBuf;
begin
  inherited;
  LCDMessage := '';
  LCDMessage := extractString(9, size-1);
end;

{ TRPRecvPlayTime }

function TRPRecvPlayTime.getDebugInfo: String;
begin
  result := 'Play Time: '+intToStr(Mins)+':'+intToStr(Secs);
end;

procedure TRPRecvPlayTime.setSpecificFieldsFromBuf;
begin
  inherited;
  Mins := buf[7];
  secs := buf[8];
end;

{ TRPSendPlay }

{ TRPRecvTrackChange }

function TRPRecvTrackChange.getDebugInfo: String;
begin
  result := 'Track Change: '+intToStr(TrackNo)+' ('+intToStr(LEngthMins)+':'+intToStr(LengthSecs)+')';
end;

procedure TRPRecvTrackChange.setSpecificFieldsFromBuf;
begin
  inherited;
  TrackNo := buf[7];
  LengthMins := buf[9];
  LengthSecs := buf[10];
end;

function TRPRecvIDHardware.getDebugInfo: String;
begin
  result := 'ID Hardware - '+HardwareID;
end;

procedure TRPRecvIDHardware.setSpecificFieldsFromBuf;
begin
  inherited;
  HardwareID := extractString(5, size-1);
end;

{ TRPRecvGetFullTitle }

function TRPRecvGetFullTitle.getDebugInfo: String;
begin
  result := 'GetFullTitle: Track '+intToStr(TrackNo)+' Chunk '+intToStr(ChunkIndex)+' Title:'+Title;
end;

procedure TRPRecvGetFullTitle.setSpecificFieldsFromBuf;
begin
  inherited;
  TrackNo := buf[7];
  ChunkIndex := buf[9];
  Title := extractString(10, size-1);
end;

{ TRPRecvGetTrackLength }

function TRPRecvGetTrackLength.getDebugInfo: String;
begin
  result := 'Get TRack Length: Track '+intToStr(TrackNo)+' '+intToStr(Mins)+':'+intToStr(Secs);
end;

procedure TRPRecvGetTrackLength.setSpecificFieldsFromBuf;
begin
  inherited;
  TrackNo := buf[7];
  Mins := buf[9];
  Secs := Buf[10];
end;

{ TRPRecvDiscInfo }

function TRPRecvDiscInfo.getDebugInfo: String;
begin
  result := 'Disc Info '+intToStr(TrackCount)+' Tracks';
end;

procedure TRPRecvDiscInfo.setSpecificFieldsFromBuf;
begin
  inherited;
  TrackCount := buf[9];
end;

{ TRPRecvCurrentTrackInfo }

function TRPRecvCurrentTrackInfo.getDebugInfo: String;
begin
  result := 'Current TRack Info: Trackno '+inttostr(TrackNo);
end;

procedure TRPRecvCurrentTrackInfo.setSpecificFieldsFromBuf;
begin
  inherited;
  TrackNo := buf[7];
end;

{ TRPRecvTitle }

function TRPRecvTitle.getDebugInfo: String;
begin
  result := 'Title: Track '+intToStr(TRackNo)+' chunk '+intToStr(ChunkIndex);
end;

procedure TRPRecvTitle.setSpecificFieldsFromBuf;
begin
  inherited;
  TrackNo := buf[7];
  ChunkIndex := buf[9];
end;

end.

