object frmPlayTrack: TfrmPlayTrack
  Left = 171
  Top = 354
  Width = 600
  Height = 59
  Caption = 'Play'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lblTime: TLabel
    Left = 8
    Top = 4
    Width = 86
    Height = 25
    Alignment = taCenter
    AutoSize = False
    Caption = '--:--'
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clLime
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object lblFilename: TLabel
    Left = 104
    Top = 4
    Width = 480
    Height = 25
    AutoSize = False
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clAqua
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object mpPlay: TMediaPlayer
    Left = 0
    Top = 32
    Width = 253
    Height = 30
    Visible = False
    TabOrder = 0
    OnNotify = mpPlayNotify
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 1
    OnTimer = Timer1Timer
    Left = 296
    Top = 16
  end
  object Timer2: TTimer
    Interval = 250
    OnTimer = Timer2Timer
    Left = 352
    Top = 16
  end
end
