program RipMDSPC3;

{$R 'icons.res' 'icons.rc'}

uses
  Forms,
  UfrmMain in 'UfrmMain.pas' {frmMain},
  UfrTrackList in 'UfrTrackList.pas' {frTrackList: TFrame},
  UReadMeLog in 'UReadMeLog.pas',
  UfrmPlayTrack in 'UfrmPlayTrack.pas' {frmPlayTrack},
  UfrmRipCountdown in '..\RipLib\UfrmRipCountdown.pas' {frmRipCountdown};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Rip MDS-PC3';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
