unit UfrmMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  URipLog, ExtCtrls, StdCtrls, URipHexToInt,
  URipStrip, URipMDSPC3, Grids, URipSonyUSBPCLink, Buttons, ComCtrls,
  UfrTrackList, ActnList, ToolWin, ImgList, Menus, AppEvnts, Mask, DBCtrls,
  UfrmRipAbout, UReadMeLog, db;

type
    TfrmMain = class(TForm)
    PageControl1: TPageControl;
    tsExperiment: TTabSheet;
    tsDeck: TTabSheet;
    pnlTop: TPanel;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    memAllReceived: TMemo;
    TabSheet4: TTabSheet;
    memUnknownReceived: TMemo;
    TabSheet5: TTabSheet;
    memKnownOnly: TMemo;
    pnlDash: TPanel;
    pnlTrackList: TPanel;
    frTrackListPlay: TfrTrackList;
    tsRecord: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    frTrackListRecord: TfrTrackList;
    ToolBar1: TToolBar;
    btnInitial: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    actActions: TActionList;
    actDeckRefreshTrackList: TAction;
    actDeckPlay: TAction;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Deck1: TMenuItem;
    Initialise1: TMenuItem;
    actDeckPlay1: TMenuItem;
    actDeckPowerOff: TAction;
    PowerOff1: TMenuItem;
    actDeckEject: TAction;
    actDeckPause: TAction;
    actDeckStop: TAction;
    actDeckNextTrack: TAction;
    actDeckPrevTrack: TAction;
    ToolButton1: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    actDeckStartFF: TAction;
    actDeckStartRew: TAction;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    actDeck1x: TAction;
    ToolButton10: TToolButton;
    pnlDisplay: TPanel;
    Label1: TLabel;
    lblDashPlayTime: TLabel;
    Label2: TLabel;
    lblDashTrack: TLabel;
    Label4: TLabel;
    lblDashDisc: TLabel;
    lblTrackNo: TLabel;
    ToolBar2: TToolBar;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    Label3: TLabel;
    edtAlbumName: TEdit;
    actRecordEraseDisc: TAction;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    actRecordRecord: TAction;
    ToolButton14: TToolButton;
    Edit1: TEdit;
    Edit2: TEdit;
    tsReadme: TTabSheet;
    actHelpAbout: TAction;
    actFileExit: TAction;
    Help1: TMenuItem;
    About1: TMenuItem;
    actFileExit1: TMenuItem;
    pnlReadMe: TPanel;
    actRecordTitle: TAction;
    ToolButton15: TToolButton;
    actRecordTransfer: TAction;
    ToolButton16: TToolButton;
    tsLogAllPackets: TTabSheet;
    pnlLogAllPackets: TPanel;
    Panel3: TPanel;
    Button2: TButton;
    btnClear: TButton;
    btnClose: TButton;
    lbSequences: TListBox;
    edtBytes: TEdit;
    btnWriteChunk: TButton;
    chkLog: TCheckBox;
    tsGeneralLog: TTabSheet;
    pnlLog: TPanel;
    Record1: TMenuItem;
    EraseDisc1: TMenuItem;
    Record2: TMenuItem;
    Title1: TMenuItem;
    Transfer1: TMenuItem;
    imgActions: TImageList;
    Eject1: TMenuItem;
    N1x1: TMenuItem;
    Next1: TMenuItem;
    Pause1: TMenuItem;
    Previous1: TMenuItem;
    FF1: TMenuItem;
    Rew1: TMenuItem;
    Stop1: TMenuItem;
    Pause2: TMenuItem;
    Stop2: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnWriteChunkClick(Sender: TObject);
    procedure lbSequencesClick(Sender: TObject);
    procedure lbSequencesDblClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure actDeckRefreshTrackListExecute(Sender: TObject);
    procedure actDeckPlayExecute(Sender: TObject);
    procedure actDeckPowerOffExecute(Sender: TObject);
    procedure actDeckEjectExecute(Sender: TObject);
    procedure actDeckPauseExecute(Sender: TObject);
    procedure actDeckStopExecute(Sender: TObject);
    procedure actDeckNextTrackExecute(Sender: TObject);
    procedure actDeckPrevTrackExecute(Sender: TObject);
    procedure actDeckStartFFExecute(Sender: TObject);
    procedure actDeckStartRewExecute(Sender: TObject);
    procedure actDeck1xExecute(Sender: TObject);
    procedure frTrackListPlaydbGridDblClick(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG;
      var Handled: Boolean);
    procedure actRecordEraseDiscExecute(Sender: TObject);
    procedure actRecordRecordExecute(Sender: TObject);
    procedure actHelpAboutExecute(Sender: TObject);
    procedure actFileExitExecute(Sender: TObject);
    procedure actRecordTitleExecute(Sender: TObject);
    procedure actRecordTransferExecute(Sender: TObject);
    procedure actActionsExecute(Action: TBasicAction;
      var Handled: Boolean);
  private
    FRipMDSPC3: TRipMDSPC3;
    FLog: TRipLog;
    FLogAllPackets: TRipLog;
    FReadMeLog: TReadMeLog;
    FDeckTrackListUpToDate: boolean;
    FLastRecordTrackNumberUsed: integer;
    procedure setLogText(const Value: String);
    property logText: String write setLogText;
    procedure setBytesToSelectedSequence;
    { Events for the FRipMDSPC3 }
    procedure OnPacketReceived(Sender: TRipSonyUSBPCLink; APacket: TRipUSBPacket);
    procedure OnPacketSent(Sender: TRipSonyUSBPCLink; APacket: TRipUSBPacket);    
    procedure refreshDeckTrackList;
    procedure setDeckTrackListUpToDate(const Value: boolean);
    property DeckTrackListUpToDate: boolean read FDeckTrackListUpToDate write setDeckTrackListUpToDate;
    procedure FindDeckTrack(ATrack: integer);
    procedure addRecordFile(fname: String);
    procedure clearRecordFileList;
    procedure transferTrack(AFilename: String; ATitle: String);
    procedure loadIconResources;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation
uses URipMDSPC3RecvPackets, URipMDSPC3SendPackets, shellapi, mpgtools,
  UfrmPlayTrack, UfrmRipCountdown;
{$R *.DFM}

procedure TfrmMain.setLogText(const Value: String);
begin
  FLog.log(Value);
end;


procedure TfrmMain.FormCreate(Sender: TObject);
begin
  try
    loadIconResources;
    { Hide tsExperiment if not with /experiment param }
    if (paramCount <> 1) or (paramStr(1) <> '/experiment') then
      tsExperiment.tabVisible := false;
    caption := getCaption;
    DragAcceptFiles(frTrackListRecord.Handle, True);
    DragAcceptFiles(Application.Handle, True);

    FLog := TRipLog.create(self);
    FLog.sitOn(pnlLog);

    FLogAllPackets := TRipLog.create(self);
    FLogAllPackets.logTime := true;
    FLogAllPackets.sitOn(pnlLogAllPackets);

    FReadMeLog := TReadMeLog.create(self);
    FREadMELog.sitOn(pnlReadME);

    FRipMDSPC3 := TRipMDSPC3.create(self);
    with FRipMDSPC3 do
    begin
      OnPacketReceived := self.OnPacketReceived;
      OnPacketSent := self.OnPacketSent;
    end; {with}
    DeckTrackListUpToDate := false;
  except
    on e:exception do showMessage('Error Starting Up: '+e.message);
  end; {try}
end;

procedure TfrmMain.Button2Click(Sender: TObject);
begin
  FRipMDSPC3.openFiles;
  FLog.log('Open handle read = '+intToStr(FRipMDSPC3.HandleRead), clBlue);
  FLog.log('Open handle write = '+intToStr(FRipMDSPC3.HandleWrite), clBlue);
  lbSequences.enabled := true;
end;

procedure TfrmMain.btnCloseClick(Sender: TObject);
begin
  FRipMDSPC3.closeFiles;
  FLog.logSuccess('File closed');
end;

procedure TfrmMain.btnWriteChunkClick(Sender: TObject);
var bytesWrote: DWORD;
    i: integer;
    toWrite: cardinal;
    packet: TRipUSBPacket;
    s: String;
begin
  packet := TRipUSBPacket.create;
  s := edtBytes.text;
  i := 0;
  while s <> '' do
  begin
    packet.buf[i] := HexToInt(stripSpaceList(s));
    packet.size := i+1;
    inc(i);    
  end; {while}
  toWrite := packet.size;
  LogText := 'Writing '+intToStr(toWrite)+'bytes';
  bytesWrote := FRipMDSPC3.sendPacket(packet);
  LogText := intToStr(BytesWrote)+' Bytes wrote';
  packet.free;
end;

procedure TfrmMain.setBytesToSelectedSequence;
var s:String;
begin
  with lbSequences do
    s := items[itemIndex];
  s := stripList(s, '('); {Remove caption}
  edtBytes.Text := trim(s);
end;

procedure TfrmMain.lbSequencesClick(Sender: TObject);
begin
  setBytesToSelectedSequence;
end;

procedure TfrmMain.lbSequencesDblClick(Sender: TObject);
begin
  btnWriteChunk.click;
end;

procedure TfrmMain.OnPacketReceived(Sender: TRipSonyUSBPCLink; APacket: TRipUSBPacket);
var s:STring;
begin
  if chkLog.checked then
    FLogAllPackets.log('Recv: '+APacket.asString+' ('+APacket.className+' - '+APacket.debugInfo+')', clBlue);

  if aPAcket is TRPRecvCurrentTrackInfo then
    with TRPRecvCurrentTrackInfo(aPacket) do
    begin
      if TrackNo <> 0 then
      begin
        lblDashTrack.Caption := FRipMDSPC3.Track[TrackNo].title;
        lblTrackNo.caption := intToStr(TrackNo);
        findDeckTrack(TrackNo);
      end; {if}
    end;

  if aPAcket is TRPRecvTrackShortTitle then
    with TRPRecvTrackShortTitle(aPacket) do
    begin
      if TrackNo <> 0 then
      begin
        lblDashTrack.Caption := FRipMDSPC3.Track[TrackNo].title;
        lblTrackNo.caption := intToStr(TrackNo);
        findDeckTrack(TrackNo)
      end; {if}
    end;

  if aPAcket is TRPRecvPlayTime then
    with TRPRecvPlayTime(aPacket) do
    begin
      if mins = 255 then
        lblDashPlayTime.caption := '--:--'
      else
        lblDashPlayTime.caption := intToStr(mins)+':'+intToStr(secs);
    end; {with}

  if aPAcket is TRPRecvDiscInfo then
    with TRPRecvDiscInfo(aPacket) do
    begin
      if trackCount = 0 then
        DeckTrackListUpToDate := false;
    end; {with}

  if not chkLog.checked then
    exit;
  { Log packet if not play time! }
  if not (aPacket is TRPRecvPlayTime) then
  begin
    s := APacket.className+':'+APacket.asString+' ('+APacket.debugInfo+')';
    if APacket is TRPRecvUnknown then
    begin
      s := '*'+s;
    end; {if unknown}

    if (APAcket is TRPRecvUnknown) then
      memUnknownReceived.Lines.add(s)
    else
      memKnownOnly.Lines.add(s);

    memAllReceived.lines.add(s);
  end; {if}
end;

//  lblDashPlayTime.caption := intToStr(mins)+':'+intToStr(secs);

procedure TfrmMain.btnClearClick(Sender: TObject);
begin
  memAllReceived.lines.clear;
  memUnknownReceived.Lines.clear;
  memKnownOnly.lines.clear;
  FLogAllPackets.lines.clear; 
end;

procedure TfrmMain.refreshDeckTrackList;
var i:integer;
    track: TTrackRec;
begin
  { Send initial packjet}
  FRipMDSPC3.SendPAcketAndWaitForAndFreeIt(TRPSendInitial.create, TRPRecvResponseToInitial);
  FRipMDSPC3.readTOC;
  LogText := FRipMDSPC3.Track[0].title;
  frTrackListPlay.emptyTable;
  for i := 1 to FRipMDSPC3.TrackCount do
  begin
    track := FRipMDSPC3.Track[i];
    with frTrackListPlay.tblTracks  do
    begin
      Append;
      fieldByName('TrackNo').asInteger := i;
      fieldByName('TrackName').asString := track.title;
      fieldByName('LengthSecs').asInteger := track.Secs;
      fieldByName('LengthMins').asInteger := track.Mins;
      post;
      with track do
        LogText := intToStr(i)+' '+track.Title+' '+intToStr(Mins)+':'+intToStr(Secs);
    end; {with}
  end; {for}
  with frTrackListPlay.tblTracks do
    first;
  { Get current track info }
  FRipMDSPC3.SendPAcketAndWaitForAndFreeIt(TRPSendGetCurrentTrackInfo.create, TRPRecvCurrentTrackInfo);
  lblDashDisc.Caption := FRipMDSPC3.Track[0].title;
  DeckTrackListUpToDate := true;
end;

procedure TfrmMain.actDeckRefreshTrackListExecute(Sender: TObject);
begin
  refreshDeckTrackList;
end;

procedure TfrmMain.actDeckPlayExecute(Sender: TObject);
var playPacket: TRPSendPlay;
begin
  playPacket := TRPSendPlay.create;
  playPacket.trackNo := 1;
  FRipMDSPC3.sendPacketAndFreeIt(playPacket);
end;

procedure TfrmMain.actDeckPowerOffExecute(Sender: TObject);
begin
 FRipMDSPC3.SendPAcketAndFreeIt(TRPSendPowerOff.create);
end;

procedure TfrmMain.actDeckEjectExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendEject.create);
end;

procedure TfrmMain.actDeckPauseExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendPause.create);
end;

procedure TfrmMain.actDeckStopExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendStop.create);
end;

procedure TfrmMain.actDeckNextTrackExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendNextTrack.create);
end;

procedure TfrmMain.actDeckPrevTrackExecute(Sender: TObject);
begin
 FRipMDSPC3.SendPAcketAndFreeIt(TRPSendPrevTrack.create);
end;

procedure TfrmMain.actDeckStartFFExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendStartFF.create);
end;

procedure TfrmMain.actDeckStartRewExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendStartRew.create);
end;

procedure TfrmMain.actDeck1xExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendStopRewOrFF.create);
end;

procedure TfrmMain.setDeckTrackListUpToDate(const Value: boolean);
//var i:integer;
begin
  FDeckTrackListUpToDate := Value;
  if value = false then
  begin
    frTrackListPlay.emptyTable;
    lblTrackNo.caption := '';
    lblDashTrack.caption := '';
    lblDashDisc.caption := '';
    lblDashPlayTime.caption := '';
  end; {if}
(*  with actActions do
    for i := 0 to actionCount-1 do
      if actions[i].category = 'Deck' then
        TAction(actions[i]).enabled := Value;*)
  actDeckRefreshTrackList.enabled := true;
end;

procedure TfrmMain.frTrackListPlaydbGridDblClick(Sender: TObject);
var playPacket: TRPSendPlay;
begin   { Play track from current record }
  playPacket := TRPSendPlay.create;
  playPacket.trackNo := frTrackListPlay.tblTracks.fieldByName('TrackNo').asInteger;
  FRipMDSPC3.sendPacketAndFreeIt(playPacket);
end;

procedure TfrmMain.FindDeckTrack(ATrack: integer);
begin
  frTrackListPlay.tblTracks.Locate('TrackNo', ATrack, []);
end;
procedure TfrmMain.ApplicationEvents1Message(var Msg: tagMSG;
  var Handled: Boolean);
var Buff : Array[0..MAX_PATH] of Char;
    Count : Word;
begin
  if (Msg.message = WM_DropFiles) then
  begin
    frTrackListRecord.tblTracks.disableControls;
    clearRecordFileList;
    for Count := 0 to DragQueryFile(Msg.wParam, $FFFFFFFF, NIL, 0) - 1 do
    begin
      DragQueryFile(Msg.wParam,Count,@Buff,SizeOf(Buff) - 1);
      try
        addRecordFile(buff);
      except
        on e:exception do
          showMEssage('Error adding file "'+buff+'" - '+e.message);
      end; {try..except}
    end; {for}
    frTrackListRecord.tblTracks.sortOnFields('TrackNo');
    frTrackListRecord.tblTracks.first;
    frTrackListRecord.tblTracks.enableControls;
    DragFinish(Msg.wParam);
    Handled := True;
 end; {if}
end;

procedure TfrmMain.clearRecordFileList;
begin
  frTrackListRecord.emptyTable;
end; {clearRecordFileList}

procedure TfrmMain.addRecordFile(fname: String);
var MPEGFile : TMPEGAudio;
begin
  if not fileExists(fname) then
    exit;
  MPEGFile := TMPEGAudio.Create;
  MPEGFile.FileName := FName;

  with frTrackListRecord.tblTracks do
  begin
    inc(FLastRecordTrackNumberUsed);
    append;
    fieldByNAme('TrackNo').asINteger := FLastRecordTrackNumberUsed;
    if MPegFile.track <> 0 then
      fieldByNAme('TrackNo').asINteger := MPegFile.track;
    fieldByName('SourceFilename').asString := fname;
    fieldByNAme('LengthMins').asInteger := MPegFile.Duration div 60;
    fieldByNAme('LengthSecs').asInteger := MPegFile.Duration mod 60;

    fieldByName('MP3Artist').asString := MPegFile.Artist;
    fieldByName('MP3Album').asString := MPegFile.album;
    fieldByName('MP3Title').asString := MPegFile.title;
    fieldByName('MP3Track').asInteger := MPegFile.Track;
    fieldByName('MP3Bitrate').asInteger := MpegFile.bitRate;
    edtAlbumName.text := MPegFile.Artist + ' - '+MPegFile.album;

    { Guess track / disc title }
    if trim(MPegFile.title) <> '' then
      fieldByName('TrackName').asString := MPegFile.title
    else
      fieldByNAme('TrackName').asString := changeFileExt(extractFileName(fname), '');
    post;
  end; {With}
  MPEGFile.Free;
end; {clearRecordFileList}

procedure TfrmMain.actRecordEraseDiscExecute(Sender: TObject);
begin
  { Erase disc }
  if messageDlg('Erase Disc - Are you sure?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
    exit;
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendEraseDisc.create);
end;

procedure TfrmMain.actRecordRecordExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendRecord.create);
end;

procedure TfrmMain.actHelpAboutExecute(Sender: TObject);
begin
  executeAbout;
end;

procedure TfrmMain.actFileExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfrmMain.actRecordTitleExecute(Sender: TObject);
begin
  FRipMDSPC3.sendTitle(strToINt(edit1.text), edit2.text);
end;

procedure TfrmMain.actRecordTransferExecute(Sender: TObject);begin
  { Send a record... }
  if frTrackListRecord.tblTracks.recordCount = 0 then
   raise exception.create('No tracks selected.');
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendRecord.create);
  RipCountdown(5, 'Waiting For Record to be ready...');
  FRipMDSPC3.sendTitle(0, edtAlbumName.text);
  RipCountdown(5, 'Waiting for Album Name to finish');
  with frTrackListRecord.tblTracks do
  begin
    first;
    while not eof do
    begin
      transferTrack(fieldByName('SourceFilename').asString, fieldByName('TrackName').asString);
      next;
    end; {while}
    first;
  end; {with}
  RipCountdown(5, 'Waiting For Recording To Finish');
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendStop.create);
  RipCountdown(5, 'Waiting For Stop To Finish');
  { RefreshTrackList }
  actDeckRefreshTrackList.Execute;
end;

procedure TfrmMain.transferTrack(AFilename: String; ATitle: String);
var f:TfrmPlayTrack;
begin
  f := TfrmPlayTrack.create(self);
  f.filename := AFilename;
  f.prepare;
  RipCountdown(5, 'Waiting For file to open');  
  FRipMDSPC3.SendPAcketAndWaitForAndFreeIt(TRPSendPause.create, TRPRecvCmdRecordOrUnpause);
  f.play;
  FRipMDSPC3.SendPAcketAndWaitForAndFreeIt(TRPSendPause.create, TRPRecvPauseRecording);
  FRipMDSPC3.sendTitle(FRipMDSPC3.TrackNo, ATitle);
  f.free;
end; {transferTrack}

procedure TfrmMain.OnPacketSent(Sender: TRipSonyUSBPCLink; APacket: TRipUSBPacket);
begin
  if chkLog.checked then
    FLogAllPackets.log('Sent: '+APacket.asString+' ('+APacket.className+' - '+APacket.debugInfo+')', clRed, [fsUnderline]);
end;

procedure TfrmMain.actActionsExecute(Action: TBasicAction; var Handled: Boolean);
begin
  if chkLog.checked then
    FLogAllPackets.log('Action: '+Action.name, clGreen, [fsBold]);
end;

{$R ripicons.res}

procedure TfrmMain.loadIconResources;
var i, idx: integer;
    strActionName: string;
    act: TAction;
begin
  idx := 0;
  imgACtions.clear;
  for i := 0 to actActions.actioncount-1 do
  begin
    act := TACtion(actActions.actions[i]);
    strActionName := act.name;
    act.imageIndex := -1; {Default}
    if imgActions.GetResource(rtBitmap, strActionName, 16, [],clWhite) then
    begin
      act.imageIndex := idx;
      inc(idx);
    end; {if}
  end; {for}
end;

end.
