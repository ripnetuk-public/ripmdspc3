program RipMDSPC3;

{$R 'icons.res' 'icons.rc'}

uses
  Forms,
  UfrmMain in 'UfrmMain.pas' {frmMain},
  UfrTrackList in 'UfrTrackList.pas' {frTrackList: TFrame},
  UfrmPlayTrack in 'UfrmPlayTrack.pas' {frmPlayTrack},
  UfrmRipCountdown in '..\RipLib\UfrmRipCountdown.pas' {frmRipCountdown},
  URipTimeString in '..\RipLib\URipTimeString.pas',
  UfrmRipTreeOptionsBase in '..\RipLib\UfrmRipTreeOptionsBase.pas' {frmRipTreeOptionsBase},
  UfrmOptions in 'UfrmOptions.pas' {frmOptions},
  UfrmRipTaskManager in '..\RipLib\RipTasks\UfrmRipTaskManager.pas' {frmRipTaskManager},
  URipBaseTask in '..\RipLib\RipTasks\URipBaseTask.pas',
  URTDelay in '..\RipLib\RipTasks\URTDelay.pas',
  UMDSPC3Tasks in 'UMDSPC3Tasks.pas',
  URTTransferTrack in 'URTTransferTrack.pas',
  UfrmRipFreeDB in '..\RipLib\UfrmRipFreeDB.pas' {frmRipFreeDB},
  URipEncodeURL in '..\RipLib\URipEncodeURL.pas',
  URipDebugMode in '..\RipLib\URipDebugMode.pas',
  UfrColourSelector in '..\RipLib\UfrColourSelector.pas' {frColourSelector: TFrame};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Rip MDS-PC3';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmRipFreeDB, frmRipFreeDB);
  Application.Run;
end.
