unit UReadMeLog;
NOT USED
interface
uses URipLog;

type TReadMeLog = class(TRipLog)
  protected
    procedure writeInitialLog; override;
end; {type}

implementation

{ TReadMeLog }

procedure TReadMeLog.writeInitialLog;
begin
  inherited;
  WordWrap := true;
  logHeader('Readme File For RipMDSPC3 V0.2');
  log('');
  logHeader('Forward');
  log('Ever since I first saw Minidisc I wanted a way to automate the '+
      ' recording of MP3s with track marks and titles in tact. I have '+
      ' attacked this from many angles, including building my own hardware '+
      ' device to plug into the parallel port for a portable device, ('+
      ' which nearly worked...)');
  log('');
  log('Finally I saw the MDS-PC3 in What Hi-Fi, and it looked like the '+
      'answer - it plugs into USB, and has a built in soundcard with '+
      'digital out.');
  log('');
  log('So I rushed down to my nearest Sony Centre and picked one up. '+
      'Everything was great apart from the software which was supplied '+
      'with the unit (MCrew). It looks like it does a lot more than just '+
      'control MD decks, so is "bloated".');
  log('');
  log('The final straw came when I discovered a couple of bugs/limitation '+
      'in the software, the most annoying one being the "long filename" bug '+
      'which causes files with long names (like most mp3s for example!) to be '+
      'reported as not found.');
  log('');
  log('Other annoyances included the fact that it failed to retrieve the ID3 tags '+
      'from some files (seemingly at random), and that it left the .mp3 or .wav '+
      'on the end of the filename.');
  log('');
  log('So I decided to try and write a better piece of software - something simple '+
      'which lets you drag a load of MP3s onto it, and records them without hassle.');
  log('');
  logHeader('Usage');
  log('The Deck tab is very straightforward. The only thing to say really about recording is drag a load of MP3s into the window, and click transfer to xfer them to minidisc...');
  logHeader('Whats Done');
  log('* Basic two way Communication with the device');
  log('* Most protocol codes understood');
  log('* Retrieve track/disc names from disc');
  log('* Record to disc');
  log('* Send title to disc/track');
  log('* Erase Disc');
  log('* Drag MP3s onto record listing, and take a good guess at album/track names');
  log('NEW * Recording of mp3s automatically, taking track names with it');
  log('NEW * Correct and sensible icons for rest of menus / buttons');
  log('');
  logHeader('Whats not done');
  log('* Checking that track will fit onto a MD');
  log('* Selecting Analog or Digital input');  
  log('* Recoding from CDs (hopefully with CDDB support)');
  log('');
  logHeader('I need help testing this thing!!');
  log('I would be grateful to anyone testing this thing to tell me '+
      'if it works on their setup. It only works with the MDS-PC3 as '+
      'far as I know.');
  log('Can you mail me and tell me what OS you are running, and a bit '+
      'about your system, and if it works.');
  log('');
  logHeader('How did I do it?');
  log('How did I work out how to talk to the MD deck??? well first I hoped '+
      'that Sony would have provided a DLL to control it. No joy. I then looked at the '+
      'supplied software, and all it was was a .exe, and a .sys driver.');
  log('');
  log('I then used filehandleex from sysinternals.com, to find out what files '+
      'the supplied software held open. I found it opened two files on the USB '+
      'and guessed one for input and one for output.');
  log('');
  log('I then disassembled MCrew, and searched for calles to Kernel32.createFile '+
      'using Microsofts WinDBG i managed to set breakpoints, and script it so it '+
      'provided me with a log of all communications between MCrew and the serial port ');
  log('');
  log('After that, it was a simple matter of doing stuff in MCrew and seting '+
      'what happened.');
  log('');
  log('Oh yes, there was also fun and games getting the virtual filename to open '+
      'to access the USB port. Thanks to the Delphi Jedi project for converting APIs '+
      'and providing HID examples (The sony device is NOT HID, but it is similar - that '+
      'one threw me as the sound card showed up as a consumer control device, so I started off trying to send commands to the soundcard!!');
  log('');
  logHeader('Legal Shit:');
  log('This program is freeware. I do not take responsibility if you muck up any discs playing with this. Use discs you dont really care about the content of to experiment!!!');

  log('');
  logHeader('Contact Me:');
  log('Do help..about and there are hyperlinks to my email and homepage.');
  logHeader('Version History:');
  log('V0.1beta - Initial Release');
  log('V0.2beta - Added in automatic transfer of lists of mp3s');
end;


end.
