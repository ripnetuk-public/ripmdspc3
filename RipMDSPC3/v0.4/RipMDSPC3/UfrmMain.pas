unit UfrmMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  URipLog, ExtCtrls, StdCtrls, URipHexToInt,
  URipStrip, URipMDSPC3, Grids, URipSonyUSBPCLink, Buttons, ComCtrls,
  UfrTrackList, ActnList, ToolWin, ImgList, Menus, AppEvnts, Mask, DBCtrls,
  UfrmRipAbout, db, URipKeyISPressed, UfrmOptions, URipVersion,
  Clipbrd, UfrmRipFreeDB, Psock, NMNNTP;

type
    TfrmMain = class(TForm)
    pcMain: TPageControl;
    tsExperiment: TTabSheet;
    tsDeck: TTabSheet;
    pnlTop: TPanel;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    memAllReceived: TMemo;
    TabSheet4: TTabSheet;
    memUnknownReceived: TMemo;
    TabSheet5: TTabSheet;
    memKnownOnly: TMemo;
    pnlDash: TPanel;
    pnlTrackList: TPanel;
    frTrackListPlay: TfrTrackList;
    tsRecord: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    frTrackListRecord: TfrTrackList;
    ToolBar1: TToolBar;
    btnInitial: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    actActions: TActionList;
    actDeckRefreshTrackList: TAction;
    actDeckPlay: TAction;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Deck1: TMenuItem;
    Initialise1: TMenuItem;
    actDeckPlay1: TMenuItem;
    actDeckPowerOff: TAction;
    PowerOff1: TMenuItem;
    actDeckEject: TAction;
    actDeckPause: TAction;
    actDeckStop: TAction;
    actDeckNextTrack: TAction;
    actDeckPrevTrack: TAction;
    ToolButton1: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    actDeckStartFF: TAction;
    actDeckStartRew: TAction;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    actDeck1x: TAction;
    ToolButton10: TToolButton;
    pnlDisplay: TPanel;
    Label1: TLabel;
    lblDashPlayTime: TLabel;
    Label2: TLabel;
    lblDashTrack: TLabel;
    Label4: TLabel;
    lblDashLCD: TLabel;
    lblTrackNo: TLabel;
    ToolBar2: TToolBar;
    ToolButton11: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    actRecordEraseDisc: TAction;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    actRecordRecord: TAction;
    ToolButton14: TToolButton;
    actHelpAbout: TAction;
    actFileExit: TAction;
    Help1: TMenuItem;
    About1: TMenuItem;
    actFileExit1: TMenuItem;
    actRecordTransfer: TAction;
    ToolButton16: TToolButton;
    tsLogAllPackets: TTabSheet;
    pnlLogAllPackets: TPanel;
    Panel3: TPanel;
    btnOpen: TButton;
    btnClear: TButton;
    btnClose: TButton;
    lbSequences: TListBox;
    edtBytes: TEdit;
    btnWriteChunk: TButton;
    chkLog: TCheckBox;
    tsGeneralLog: TTabSheet;
    pnlLog: TPanel;
    Record1: TMenuItem;
    EraseDisc1: TMenuItem;
    Record2: TMenuItem;
    Transfer1: TMenuItem;
    imgActions: TImageList;
    Eject1: TMenuItem;
    N1x1: TMenuItem;
    Next1: TMenuItem;
    Pause1: TMenuItem;
    Previous1: TMenuItem;
    FF1: TMenuItem;
    Rew1: TMenuItem;
    Stop1: TMenuItem;
    Pause2: TMenuItem;
    Stop2: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    actDeckUpdateTitles: TAction;
    ToolButton15: TToolButton;
    actToolsOptions: TAction;
    Debug1: TMenuItem;
    actRecordPreview: TAction;
    N5: TMenuItem;
    Preview1: TMenuItem;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    actDeckFreeDB: TAction;
    ToolButton19: TToolButton;
    tsDebug: TTabSheet;
    memDebug: TMemo;
    NMNNTP1: TNMNNTP;
    N7: TMenuItem;
    FreeDB1: TMenuItem;
    Tools1: TMenuItem;
    Options1: TMenuItem;
    actDebugTestTaskManager: TAction;
    TestTaskManager1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnWriteChunkClick(Sender: TObject);
    procedure lbSequencesClick(Sender: TObject);
    procedure lbSequencesDblClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure actDeckRefreshTrackListExecute(Sender: TObject);
    procedure actDeckPlayExecute(Sender: TObject);
    procedure actDeckPowerOffExecute(Sender: TObject);
    procedure actDeckEjectExecute(Sender: TObject);
    procedure actDeckPauseExecute(Sender: TObject);
    procedure actDeckStopExecute(Sender: TObject);
    procedure actDeckNextTrackExecute(Sender: TObject);
    procedure actDeckPrevTrackExecute(Sender: TObject);
    procedure actDeckStartFFExecute(Sender: TObject);
    procedure actDeckStartRewExecute(Sender: TObject);
    procedure actDeck1xExecute(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG;
      var Handled: Boolean);
    procedure actRecordEraseDiscExecute(Sender: TObject);
    procedure actRecordRecordExecute(Sender: TObject);
    procedure actHelpAboutExecute(Sender: TObject);
    procedure actFileExitExecute(Sender: TObject);
    procedure actRecordTransferExecute(Sender: TObject);
    procedure actActionsExecute(Action: TBasicAction;
      var Handled: Boolean);
    procedure actDeckUpdateTitlesExecute(Sender: TObject);
    procedure frTrackListPlaydbeTrackNoClick(Sender: TObject);
    procedure actToolsOptionsExecute(Sender: TObject);
    procedure actRecordPreviewExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure frTrackListPlayRemoveJunkFromTitle1Click(Sender: TObject);
    procedure actDeckFreeDBExecute(Sender: TObject);
    procedure actDebugTestTaskManagerExecute(Sender: TObject);
  private
    FFreeDB: TfrmRipFreeDB;
    FOptions: TfrmOptions;
    FRipMDSPC3: TRipMDSPC3;
    FLog: TRipLog;
    FLogAllPackets: TRipLog;
    FDeckTrackListUpToDate: boolean;
    FLastRecordTrackNumberUsed: integer;
    procedure setLogText(const Value: String);
    property logText: String write setLogText;
    procedure setBytesToSelectedSequence;
    { Events for the FRipMDSPC3 }
    procedure OnPacketReceived(Sender: TRipSonyUSBPCLink; APacket: TRipUSBPacket);
    procedure OnPacketSent(Sender: TRipSonyUSBPCLink; APacket: TRipUSBPacket);    
    procedure refreshDeckTrackList;
    procedure setDeckTrackListUpToDate(const Value: boolean);
    property DeckTrackListUpToDate: boolean read FDeckTrackListUpToDate write setDeckTrackListUpToDate;
    procedure FindDeckTrack(ATrack: integer);
    procedure addRecordFile(fname: String);
    procedure clearRecordFileList;
//    procedure transferTrack(AFilename: String; ATitle: String);
    procedure loadIconResources;
    procedure grabMpegDetails(AFilename: STring);
    procedure buildFreeDBTOC;
    procedure openMDSPC3;
    procedure rereadOptions;
    procedure OptionsFormApply(Sender: TObject);
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation
uses URipMDSPC3RecvPackets, URipMDSPC3SendPackets, shellapi, mpgtools,
  UfrmPlayTrack, UfrmRipCountdown, URipTimeString, UfrmRipTaskManager,
  URipBaseTask, URTDelay, UMDSPC3Tasks, URTTransferTrack, URipDebugMode;
{$R *.DFM}

procedure TfrmMain.setLogText(const Value: String);
begin
  FLog.log(Value);
end;


procedure TfrmMain.FormCreate(Sender: TObject);
begin
  FOptions := TfrmOptions.create(self);
  FOptions.onApply := OptionsFormApply;
  try
    loadIconResources;
    { Hide tsExperiment if not with /experiment param }
    if not RipDebugMode then
    begin
      tsExperiment.tabVisible := false;
      tsDebug.tabVisible := false;
      Debug1.Visible := false;
    end; {if}

    pcMain.activePage := tsDeck;

    caption := getCaption;
    DragAcceptFiles(frTrackListRecord.Handle, True);
    DragAcceptFiles(frTrackListPlay.Handle, True);
    DragAcceptFiles(Application.Handle, True);

    FLog := TRipLog.create(self);
    FLog.sitOn(pnlLog);

    FFreeDB := TfrmRipFreeDB.create(self);

    FLogAllPackets := TRipLog.create(self);
    FLogAllPackets.logTime := true;
    FLogAllPackets.sitOn(pnlLogAllPackets);

    FRipMDSPC3 := TRipMDSPC3.create(self);
    with FRipMDSPC3 do
    begin
      OnPacketReceived := self.OnPacketReceived;
      OnPacketSent := self.OnPacketSent;
    end; {with}
    DeckTrackListUpToDate := false;
  except
    on e:exception do showMessage('Error Starting Up: '+e.message);
  end; {try}
end;

procedure TfrmMain.btnOpenClick(Sender: TObject);
begin
  openMDSPC3;
end; {btnOpenClick}

procedure TfrmMain.openMDSPC3;
begin
  FRipMDSPC3.openFiles;
  FripMDSPC3.sendPacketAndFreeIt(TRPSendTurnOnTimeInfo.create);
  FLog.log('Open handle read = '+intToStr(FRipMDSPC3.HandleRead), clBlue);
  FLog.log('Open handle write = '+intToStr(FRipMDSPC3.HandleWrite), clBlue);
  lbSequences.enabled := true;
end;

procedure TfrmMain.btnCloseClick(Sender: TObject);
begin
  FRipMDSPC3.closeFiles;
  FLog.logSuccess('File closed');
end;

procedure TfrmMain.btnWriteChunkClick(Sender: TObject);
var bytesWrote: DWORD;
    i: integer;
    toWrite: cardinal;
    packet: TRipUSBPacket;
    s: String;
begin
  packet := TRipUSBPacket.create;
  s := edtBytes.text;
  i := 0;
  while s <> '' do
  begin
    packet.buf[i] := HexToInt(stripSpaceList(s));
    packet.size := i+1;
    inc(i);    
  end; {while}
  toWrite := packet.size;
  LogText := 'Writing '+intToStr(toWrite)+'bytes';
  bytesWrote := FRipMDSPC3.sendPacket(packet);
  LogText := intToStr(BytesWrote)+' Bytes wrote';
  packet.free;
end;

procedure TfrmMain.setBytesToSelectedSequence;
var s:String;
begin
  with lbSequences do
    s := items[itemIndex];
  s := stripList(s, '('); {Remove caption}
  edtBytes.Text := trim(s);
end;

procedure TfrmMain.lbSequencesClick(Sender: TObject);
begin
  setBytesToSelectedSequence;
end;

procedure TfrmMain.lbSequencesDblClick(Sender: TObject);
begin
  btnWriteChunk.click;
end;

procedure TfrmMain.OnPacketReceived(Sender: TRipSonyUSBPCLink; APacket: TRipUSBPacket);
var s:STring;
begin
  if chkLog.checked then
    FLogAllPackets.log('Recv: '+APacket.asString+' ('+APacket.className+' - '+APacket.debugInfo+')', clBlue);

  if aPAcket is TRPRecvCurrentTrackInfo then
    with TRPRecvCurrentTrackInfo(aPacket) do
    begin
      if TrackNo <> 0 then
      begin
        lblDashTrack.Caption := FRipMDSPC3.Track[TrackNo].title;
        lblTrackNo.caption := intToStr(TrackNo);
        findDeckTrack(TrackNo);
      end; {if}
    end;

  if aPAcket is TRPRecvTrackShortTitle then
    with TRPRecvTrackShortTitle(aPacket) do
    begin
      if TrackNo <> 0 then
      begin
        lblDashTrack.Caption := FRipMDSPC3.Track[TrackNo].title;
        lblTrackNo.caption := intToStr(TrackNo);
        findDeckTrack(TrackNo)
      end; {if}
    end;

  if aPAcket is TRPRecvPlayTime then
    with TRPRecvPlayTime(aPacket) do
    begin
      if mins = 255 then
        lblDashPlayTime.caption := '--:--:--'
      else
        lblDashPlayTime.caption := RipTimeString(Hours, Mins, Secs);
      if trim(lblDashLCD.caption) = '' then
        lblDashLCD.caption := frTrackListPlay.edtAlbumName.text;
    end; {with}

  if aPAcket is TRPRecvDiscInfo then
    with TRPRecvDiscInfo(aPacket) do
    begin
      if trackCount = 0 then
        DeckTrackListUpToDate := false;
    end; {with}

  if aPacket is TRPRecvLCDMessage then
    with TRPRecvLCDMessage(aPAcket) do
      lblDashLCD.caption := LCDMessage;

  if not chkLog.checked then
    exit;
  { Log packet if not play time! }
  if not (aPacket is TRPRecvPlayTime) then
  begin
    s := APacket.className+':'+APacket.asString+' ('+APacket.debugInfo+')';
    if APacket is TRPRecvUnknown then
    begin
      s := '*'+s;
    end; {if unknown}

    if (APAcket is TRPRecvUnknown) then
      memUnknownReceived.Lines.add(s)
    else
      memKnownOnly.Lines.add(s);

    memAllReceived.lines.add(s);
  end; {if}
end;

//  lblDashPlayTime.caption := intToStr(mins)+':'+intToStr(secs);

procedure TfrmMain.btnClearClick(Sender: TObject);
begin
  memAllReceived.lines.clear;
  memUnknownReceived.Lines.clear;
  memKnownOnly.lines.clear;
  FLogAllPackets.lines.clear; 
end;

procedure TfrmMain.refreshDeckTrackList;
var i:integer;
    track: TTrackRec;
begin
  { Send initial packjet}
  FRipMDSPC3.SendPAcketAndWaitForAndFreeIt(TRPSendInitial.create, TRPRecvResponseToInitial);
  FRipMDSPC3.readTOC;
  LogText := FRipMDSPC3.Track[0].title;
  frTrackListPlay.emptyTable;
  for i := 1 to FRipMDSPC3.TrackCount do
  begin
    track := FRipMDSPC3.Track[i];
    with frTrackListPlay.tblTracks  do
    begin
      Append;
      fieldByName('TrackNo').asInteger := i;
      fieldByName('TrackName').asString := track.title;
      fieldByName('LengthSecs').asInteger := track.Secs;
      fieldByName('LengthMins').asInteger := track.Mins;
      post;
      with track do
        LogText := intToStr(i)+' '+track.Title+' '+intToStr(Mins)+':'+intToStr(Secs);
    end; {with}
  end; {for}
  with frTrackListPlay.tblTracks do
    first;
  { Get current track info }
  FRipMDSPC3.SendPAcketAndWaitForAndFreeIt(TRPSendGetCurrentTrackInfo.create, TRPRecvCurrentTrackInfo);
  frTrackListPlay.edtAlbumName.text := FRipMDSPC3.Track[0].title;
  DeckTrackListUpToDate := true;
end;

procedure TfrmMain.actDeckRefreshTrackListExecute(Sender: TObject);
begin
  refreshDeckTrackList;
end;

procedure TfrmMain.actDeckPlayExecute(Sender: TObject);
var playPacket: TRPSendPlay;
begin   { Play track from current record }
  playPacket := TRPSendPlay.create;
  playPacket.trackNo := frTrackListPlay.tblTracks.fieldByName('TrackNo').asInteger;
  FRipMDSPC3.sendPacketAndFreeIt(playPacket);
end;

procedure TfrmMain.actDeckPowerOffExecute(Sender: TObject);
begin
 FRipMDSPC3.SendPAcketAndFreeIt(TRPSendPowerOff.create);
end;

procedure TfrmMain.actDeckEjectExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendEject.create);
end;

procedure TfrmMain.actDeckPauseExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendPause.create);
end;

procedure TfrmMain.actDeckStopExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendStop.create);
end;

procedure TfrmMain.actDeckNextTrackExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendNextTrack.create);
end;

procedure TfrmMain.actDeckPrevTrackExecute(Sender: TObject);
begin
 FRipMDSPC3.SendPAcketAndFreeIt(TRPSendPrevTrack.create);
end;

procedure TfrmMain.actDeckStartFFExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendStartFF.create);
end;

procedure TfrmMain.actDeckStartRewExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendStartRew.create);
end;

procedure TfrmMain.actDeck1xExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendStopRewOrFF.create);
end;

procedure TfrmMain.setDeckTrackListUpToDate(const Value: boolean);
//var i:integer;
begin
  FDeckTrackListUpToDate := Value;
  if value = false then
  begin
    frTrackListPlay.emptyTable;
    lblTrackNo.caption := '';
    lblDashTrack.caption := '';
    frTrackListPlay.edtAlbumName.text := '';
    lblDashPlayTime.caption := '';
  end; {if}
(*  with actActions do
    for i := 0 to actionCount-1 do
      if actions[i].category = 'Deck' then
        TAction(actions[i]).enabled := Value;*)
  actDeckRefreshTrackList.enabled := true;
end;

procedure TfrmMain.FindDeckTrack(ATrack: integer);
begin
  frTrackListPlay.tblTracks.Locate('TrackNo', ATrack, []);
end;

procedure TfrmMain.ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
var Buff : Array[0..MAX_PATH] of Char;
    Count : Word;
    slFiles: TStringList;
    i: integer;
begin
  slFiles := TSTringList.create;
  if (Msg.message = WM_DropFiles) then
  begin
    pcMain.activePage := tsRecord;
    frTrackListRecord.tblTracks.disableControls;
    if not keyIsPressed(VK_CONTROL) then
      clearRecordFileList;
    for Count := 0 to DragQueryFile(Msg.wParam, $FFFFFFFF, NIL, 0) - 1 do
    begin
      DragQueryFile(Msg.wParam,Count,@Buff,SizeOf(Buff) - 1);
      try
        slFiles.add(buff);
      except
        on e:exception do
          showMEssage('Error adding file "'+buff+'" - '+e.message);
      end; {try..except}
    end; {for}
    screen.cursor := crHourglass;
    slFiles.sort;
    for i := 0 to slFiles.count-1 do
      addRecordFile(slFiles[i]);
    frTrackListRecord.tblTracks.sortOnFields('TrackNo');
    frTrackListRecord.tblTracks.first;
    frTrackListRecord.tblTracks.enableControls;
    screen.cursor := crDefault;    
    DragFinish(Msg.wParam);
    Handled := True;
 end; {if}
 slFiles.free;
end;

procedure TfrmMain.clearRecordFileList;
begin
  frTrackListRecord.emptyTable;
  FLastRecordTrackNumberUsed := 0;
end; {clearRecordFileList}

procedure TfrmMain.grabMpegDetails(AFilename: STring);
var MPEGFile : TMPEGAudio;
begin
  MPEGFile := TMPEGAudio.Create;
  MPEGFile.FileName := AFilename;
  with frTrackListRecord.tblTracks do
  begin
    if MPegFile.track <> 0 then
      fieldByNAme('TrackNo').asINteger := MPegFile.track;
    fieldByNAme('LengthMins').asInteger := MPegFile.Duration div 60;
    fieldByNAme('LengthSecs').asInteger := MPegFile.Duration mod 60;

    fieldByName('MP3Artist').asString := MPegFile.Artist;
    fieldByName('MP3Album').asString := MPegFile.album;
    fieldByName('MP3Title').asString := MPegFile.title;
    fieldByName('MP3Track').asInteger := MPegFile.Track;
    fieldByName('MP3Bitrate').asInteger := MpegFile.bitRate;
    frTrackListRecord.edtAlbumName.text := MPegFile.Artist + ' - '+MPegFile.album;
  end; {with}
  MPEGFile.Free;
end; {grabMpegDetails}

procedure TfrmMain.addRecordFile(fname: String);
begin
  if not fileExists(fname) then
    exit;
  with frTrackListRecord.tblTracks do
  begin
    inc(FLastRecordTrackNumberUsed);
    append;
    if pos('.mp3', lowerCase(fname)) > 0 then
      grabMpegDetails(fname);
    fieldByNAme('TrackNo').asINteger := FLastRecordTrackNumberUsed;
    fieldByName('SourceFilename').asString := fname;

    { Guess track / disc title }
    if fieldByName('MP3Title').asString <> '' then
      fieldByName('TrackName').asString := fieldByName('MP3Title').asString
    else
      fieldByNAme('TrackName').asString := changeFileExt(extractFileName(fname), '');
    post;
  end; {With}
end; {clearRecordFileList}

procedure TfrmMain.actRecordEraseDiscExecute(Sender: TObject);
begin
  { Erase disc }
  if messageDlg('Erase Disc - Are you sure?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
    exit;
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendEraseDisc.create);
end;

procedure TfrmMain.actRecordRecordExecute(Sender: TObject);
begin
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendRecord.create);
end;

procedure TfrmMain.actHelpAboutExecute(Sender: TObject);
begin
  executeAbout;
end;

procedure TfrmMain.actFileExitExecute(Sender: TObject);
begin
  close;
end;

procedure TfrmMain.actRecordTransferExecute(Sender: TObject);
var tStartRecord: TRTStartRecording;
    tTitle: TRTSendTitle;
    tTransfer: TRTTransferTrack;
    tStop: TRTStop;
    tErase: TRTEraseDisc;
    aManager: TfrmRipTaskMAnager;
    idx: integer;
begin
  { Check we have something to record... }
  if frTrackListRecord.tblTracks.recordCount = 0 then
   raise exception.create('No tracks selected.');

  aManager := TfrmRipTaskManager.create(self);
  aManager.caption := 'Transfer Tracks';
  aManager.coverControl(pcMain);

  tStop := TRTStop.createRipMDSPC3(FRipMDSPC3);
  aManager.addTask(tStop);

  aManager.addDelay(2000, 'Wait 2s for stop to finish');

  tErase := TRTEraseDisc.createRipMDSPC3(FRipMDSPC3);
  aManager.addTask(tErase);

  aManager.addDelay(2000, 'Wait 2s for erase to finish');

  tStartRecord := TRTStartRecording.CreateRipMDSPC3(FRipMDSPC3);
  aManager.addTask(tStartRecord);

  aManager.addDelay(2000, 'Wait 2s for recording to start');

  frTrackListRecord.beginWork;
  with frTrackListRecord.tblTracks do
  begin
    first;
    while not eof do
    begin
      tTransfer := TRTTransferTrack.createRipMDSPC3(FRipMDSPC3);
      tTransfer.parentWindow := self;
      tTransfer.filename := fieldByName('SourceFilename').asString;
      tTransfer.title := fieldByName('TrackName').asString;
      aManager.addTask(tTransfer);

      aManager.addDelay(4000, 'Wait 4s for recording to finish');

      next;
    end; {while}
  end; {with}

  tStop := TRTStop.createRipMDSPC3(FRipMDSPC3);
  aManager.addTask(tStop);

  aManager.addDelay(2000, 'Wait 2s for stop to finish');

  { Title disc }
  tTitle := TRTSendTitle.createRipMDSPC3(FRipMDSPC3);
  tTitle.TrackNo := 0; {Album name}
  tTitle.title := frTrackListRecord.edtAlbumName.text;
  aManager.addTask(tTitle);
  { Now add tasks to title each track }
  idx := 1;
  with frTrackListRecord.tblTracks do
  begin
    first;
    while not eof do
    begin
      tTitle := TRTSendTitle.createRipMDSPC3(FRipMDSPC3);
      tTitle.trackNo := idx;
      tTitle.title := fieldByName('TrackName').asString;
      aManager.addTask(tTitle);
      inc(idx);
      next;
    end; {while}
    first;
  end; {with}
  frTrackListRecord.endWork;
  aManager.showModal;
  aManager.free;
  { RefreshTrackList }
  actDeckRefreshTrackList.Execute;
end;

(* keep old version :)
procedure TfrmMain.actRecordTransferExecute(Sender: TObject);
begin
  { Send a record... }
  if frTrackListRecord.tblTracks.recordCount = 0 then
   raise exception.create('No tracks selected.');
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendRecord.create);
  RipCountdown(3, 'Waiting For Record to be ready...');
  FRipMDSPC3.sendTitle(0, frTrackListRecord.edtAlbumName.text);
  RipCountdown(3, 'Waiting for Album Name to finish');
  with frTrackListRecord.tblTracks do
  begin
    first;
    while not eof do
    begin
      transferTrack(fieldByName('SourceFilename').asString, fieldByName('TrackName').asString);
      next;
    end; {while}
    first;
  end; {with}
  RipCountdown(5, 'Waiting For Recording To Finish');
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendStop.create);
  RipCountdown(5, 'Waiting For Stop To Finish');
  { RefreshTrackList }
  actDeckRefreshTrackList.Execute;
end;
*)

(*procedure TfrmMain.transferTrack(AFilename: String; ATitle: String);
var f:TfrmPlayTrack;
begin
  f := TfrmPlayTrack.create(self);
  f.filename := AFilename;
  f.prepare;
  RipCountdown(2, 'Waiting For file to open');
  FRipMDSPC3.SendPAcketAndWaitForAndFreeIt(TRPSendPause.create, TRPRecvCmdRecordOrUnpause);
  f.play;
  FRipMDSPC3.SendPAcketAndWaitForAndFreeIt(TRPSendPause.create, TRPRecvPauseRecording);
  FRipMDSPC3.sendTitle(FRipMDSPC3.TrackNo, ATitle);
  f.free;
end; {transferTrack}*)

procedure TfrmMain.OnPacketSent(Sender: TRipSonyUSBPCLink; APacket: TRipUSBPacket);
begin
  if chkLog.checked then
    FLogAllPackets.log('Sent: '+APacket.asString+' ('+APacket.className+' - '+APacket.debugInfo+')', clRed, [fsUnderline]);
end;

procedure TfrmMain.actActionsExecute(Action: TBasicAction; var Handled: Boolean);
begin
  if chkLog.checked then
    FLogAllPackets.log('Action: '+Action.name, clGreen, [fsBold]);
end;

{$R ripicons.res}

procedure TfrmMain.loadIconResources;
var i, idx: integer;
    strActionName: string;
    act: TAction;
begin
  idx := 0;
  imgACtions.clear;
  for i := 0 to actActions.actioncount-1 do
  begin
    act := TACtion(actActions.actions[i]);
    strActionName := act.name;
    act.imageIndex := -1; {Default}
    if imgActions.GetResource(rtBitmap, strActionName, 16, [],clWhite) then
    begin
      act.imageIndex := idx;
      inc(idx);
    end; {if}
  end; {for}
end;

procedure TfrmMain.actDeckUpdateTitlesExecute(Sender: TObject);
begin
  { Need to save updated titles }
  FRipMDSPC3.sendTitle(0, frTrackListPlay.edtAlbumName.text);
  frTrackListPlay.beginWork;
  try
    with frTrackListPlay.tblTracks do
    begin
      first;
      while not eof do
      begin
        FRipMDSPC3.sendTitle(fieldByName('TrackNo').asINteger, fieldByName('TrackName').asString);
        next;
      end; {while}
    end; {with}
  finally
    frTrackListPlay.endWork;
  end; {try..finally}
end;

procedure TfrmMain.frTrackListPlaydbeTrackNoClick(Sender: TObject);
begin
  actDeckPlay.execute;
end;

procedure TfrmMain.actToolsOptionsExecute(Sender: TObject);
begin
  FOptions.execute;
  rereadOptions;
end;

procedure TfrmMain.actRecordPreviewExecute(Sender: TObject);
var tPreview: TRTPreviewTrack;
    aManager: TfrmRipTaskMAnager;
begin
  { Check we have something to record... }
  if frTrackListRecord.tblTracks.recordCount = 0 then
   raise exception.create('No tracks selected.');

  aManager := TfrmRipTaskManager.create(self);
  aManager.autoStart := true;
  aManager.caption := 'Preview '+frTrackListRecord.tblTracks.fieldByNAme('TrackName').asString;
  aManager.coverControl(pcMain);

  tPreview := TRTPreviewTrack.createRipMDSPC3(FRipMDSPC3);
  tPreview.parentWindow := self;
  tPreview.filename := frTrackListRecord.tblTracks.fieldByName('SourceFilename').asString;
  tPreview.title := frTrackListRecord.tblTracks.fieldByName('TrackName').asString;
  aManager.addTask(tPreview);

  { Run it}
  aManager.showModal;
  aManager.free;
end;


procedure TfrmMain.FormShow(Sender: TObject);
begin
  try
    openMDSPC3;
    if FOptions.chkStartupAutoRefreshTrackListing.checked then
      refreshDeckTrackList;
  except
  end; {ignore errors at this stage}
  try
    rereadOptions;
  except
    on e:exception do
      showMessage(e.message);
  end; {try..except}
end;

procedure TfrmMain.frTrackListPlayRemoveJunkFromTitle1Click(
  Sender: TObject);
begin
  frTrackListPlay.RemoveJunkFromTitle1Click(Sender);
end;

procedure TfrmMain.buildFreeDBTOC;
var sTRack, sTotal: integer;
begin
  FFreeDB.clearTOC;
  sTotal := 2; {Start at 2 s seems good for now}

  FFreeDB.addTOCSecs(sTotal);
  frTrackListPlay.beginWork;
  try
    with frTrackListPlay.tblTracks do
    begin
      first;
      while not eof do
      begin
        sTrack := fieldByName('LengthHours').asInteger*60*60 + fieldByName('LengthMins').asInteger*60 +  fieldByName('LengthSecs').asINteger;
        sTotal := sTotal + sTrack;
        FFreeDB.addTOCSecs(sTotal);
        next;
      end; {while}
    end; {with}
  finally
    frTrackListPlay.endWork;
  end; {try..finally}
end;

procedure TfrmMain.actDeckFreeDBExecute(Sender: TObject);
var mr: TModalResult;
    i: integer;
    sTitle: String;
begin
  { Fill CDDB object's TOC }
  buildFreeDBToc;
  FFreeDB.autoStart := true;
  FFreeDB.debugDumpTOC(memDebug.lines);
  mr := FFreeDB.showModal;
  if mr <> mrOK then
    exit; {Canceled}
  { Retrieve CDDB info from response of RipFreeDB }
  frTrackListPLay.edtAlbumName.Text := FFreeDB.response.values['DTITLE'];
  i := 0;
  frTrackListPlay.beginWork;
  try
    with frTrackListPlay.tblTracks do
    begin
      first;
      while not eof do
      begin
        sTitle := FFreeDB.response.values['TTITLE'+intToStr(i)];
        edit;
        fieldByName('TrackName').asString := sTitle;
        post;
        inc(i);
        next;
      end; {while}
    end; {with}
  finally
    frTrackListPlay.endWork;
  end; {try..finally}

end;

procedure TfrmMain.actDebugTestTaskManagerExecute(Sender: TObject);
var f:TfrmRipTaskManager;
    td: TRTDelay;
    tr: TRTTransferTrack;
    var i:integer;
begin
  f := TfrmRipTaskManager.create(self);
  f.coverControl(pcMain);

  tr := TRTTransferTrack.CreateRipMDSPC3(FRipMDSPC3);
  tr.parentWindow := self;
  tr.Filename := 'z:\testmp3\1.mp3';
  tr.Title := 'Test MP3';
  f.addTask(tr);

  for i := 1 to 10 do
  begin
    td := TRTDelay.create;
    td.PeriodMS := 4000; {4s}
    f.addTask(td);

    td := TRTDelay.create;
    td.PeriodMS := 6000; {4s}
    f.addTask(td);
  end; {foe}
  f.show;
end;

procedure TfrmMain.rereadOptions;
begin
  lblTrackNo.font.color := FOptions.csTrackNumberLCDColor.getColor;
  lblDashTrack.font.color := FOptions.csTrackNameLCDColor.getColor;
  lblDashLCD.font.color := FOptions.csMainLCDColor.getColor;
  lblDashPlayTime.font.color := FOptions.csTimeLCDColor.getColor;

  frTrackListPlay.dbGrid.SelectedColor := FOptions.csPlaySelectedColor.getColor;
  frTrackListRecord.dbGrid.SelectedColor := FOptions.csRecordSelectedColor.getColor;

  FFreeDB.Host := FOptions.edtFreeDBServer.Text;
end; {rereadOptions}

procedure TfrmMain.OptionsFormApply(Sender: TObject);
begin
  rereadOptions;
end; {optionsFormApply}

end.
