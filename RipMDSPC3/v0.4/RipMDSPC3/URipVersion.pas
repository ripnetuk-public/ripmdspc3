unit URipVersion;

interface
const RIP_PROGRAM = 'Rip MDS-PC3';
const RIP_VERSION = 'V0.4';
const RIP_COPYRIGHT = '(C) August 2001 George Styles';
implementation

{
DONE in 0.4

1. Time in playback displays H:M:S
2. Proper task pane for transfer process
3. Copy / Paste All
4. FreeDB support
5. Cancel feature
6. Preview track
7. Rename existing tracks
8. Time overhaul

TODO:

1. Track scroller to position on track

DONE:

 }

end.
