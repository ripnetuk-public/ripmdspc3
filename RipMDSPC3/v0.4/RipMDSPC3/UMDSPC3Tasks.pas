unit UMDSPC3Tasks;

interface
uses URipBaseTask, SysUtils, Classes, URipMDSPC3;

type TRipBaseMDSPC3Task = class(TRipBaseTask)
  protected
    FRipMDSPC3: TRipMDSPC3;
  public
    constructor CreateRipMDSPC3(ARipMDSPC3: TRipMDSPC3);
end;

type TRTStartRecording = class(TRipBaseMDSPC3Task)
  public
  protected
    procedure doRun; override;
    function getTaskDescription: String; override;
end; {tyoe}

type TRTEraseDisc = class(TRipBaseMDSPC3Task)
  public
  protected
    procedure doRun; override;
    function getTaskDescription: String; override;
end; {tyoe}

type TRTStop = class(TRipBaseMDSPC3Task)
  public
  protected
    procedure doRun; override;
    function getTaskDescription: String; override;
end; {tyoe}


type TRTSendTitle = class(TRipBaseMDSPC3Task)
  public
    TrackNo: integer;
    Title: String;
  protected
    procedure doRun; override;
    function getTaskDescription: String; override;
end; {tyoe}

implementation

uses URipMDSPC3RecvPackets, URipMDSPC3SendPackets, URipSleep;
{ TRTStartRecording }

procedure TRTStartRecording.doRun;
begin
  doProgress(0,1);
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendRecord.create);
  doProgress(1,1);
end;

function TRTStartRecording.getTaskDescription: String;
begin
  result := 'Start Recording';
end;

{ TRipBaseMDSPC3Task }

constructor TRipBaseMDSPC3Task.CreateRipMDSPC3(ARipMDSPC3: TRipMDSPC3);
begin
  inherited;
  FRipMDSPC3 := ARipMDSPC3;
end;

{ TRTSendTitle }

procedure TRTSendTitle.doRun;
begin
  FRipMDSPC3.sendTitle(TrackNo, title);
end;

function TRTSendTitle.getTaskDescription: String;
begin
  if trackNo = 0 then
    result := 'Send Disc Title "'+Title+'"'
  else
    result := 'Send Track '+intToStr(TrackNo)+' Title "'+Title+'"';
end;

{ TRTStop }

procedure TRTStop.doRun;
begin
  doProgress(0,1);
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendStop.create);
  doProgress(1,1);
end;

function TRTStop.getTaskDescription: String;
begin
  result := 'Press Stop';
end;

{ TRTEraseDisc }

procedure TRTEraseDisc.doRun;
begin
  inherited;
  doProgress(0,1);
  FRipMDSPC3.SendPAcketAndFreeIt(TRPSendEraseDisc.create);
  doProgress(1,1);
end;

function TRTEraseDisc.getTaskDescription: String;
begin
  result := 'Erase Disc';
end;

end.
