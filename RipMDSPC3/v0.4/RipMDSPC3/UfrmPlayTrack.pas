unit UfrmPlayTrack;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MPlayer, ExtCtrls, StdCtrls, UfrmRipCountdown, URipTimeString;

type
  TfrmPlayTrack = class(TForm)
    mpPlay: TMediaPlayer;
    timerClose: TTimer;
    timerUpdateDisplay: TTimer;
    lblTime: TLabel;
    lblFilename: TLabel;
    procedure timerCloseTimer(Sender: TObject);
    procedure mpPlayNotify(Sender: TObject);
    procedure timerUpdateDisplayTimer(Sender: TObject);
  private
    { Private declarations }
  public
    Filename: String;
    procedure play;
    procedure prepare;
  end;

var
  frmPlayTrack: TfrmPlayTrack;

implementation

{$R *.DFM}

{ TfrmPlayTrack }

procedure TfrmPlayTrack.timerCloseTimer(Sender: TObject);
begin
  close;
end;

procedure TfrmPlayTrack.mpPlayNotify(Sender: TObject);
begin
  if mpPlay.Mode = mpPlaying then
    timerClose.enabled := true;
end;

procedure TfrmPlayTrack.timerUpdateDisplayTimer(Sender: TObject);
var timeSecs: longint;
begin
  timeSecs  := mpPlay.position div 1000;
  lblTime.caption := RipTimeString(TimeSecs);
  if mpPlay.position >= mpPlay.length then
    timerClose.enabled := true;
end;

var PreparedBefore: boolean;

procedure TfrmPlayTrack.prepare;
begin
  mpPlay.FileName := Filename;
  lblFilename.caption := extractFilename(Filename);
  mpPlay.Wait := true;
  mpPlay.open;
  if not PreparedBefore then
  begin
    mpPlay.play;
    ripCountdown(3, 'Practice Run...');
    mpPlay.stop;
    mpPlay.Rewind;
    PreparedBefore := true;
  end; {if}
  mpPlay.Wait := false;
end;

procedure TfrmPlayTrack.play;
begin
  mpPlay.play;
  showModal;
end;

initialization
  PReparedBefore := false;
end.
