unit URTTransferTrack;

{ In own unit as complex... }
interface

uses URipBaseTask, SysUtils, Classes, URipMDSPC3, UMDSPC3Tasks,
     MPlayer, windows, forms, URipTimeString;

const MEDIAPLAYER_TIMEOUT = 4000; {4s}

type TRTBasePlayTrack = class(TRipBaseMDSPC3Task)
  private
    FMediaPlayer: TMediaPlayer;
    FPlaying: boolean;
  public
    Filename: String;
    Title: String;
    ParentWindow: TForm;
  protected
    procedure startPlayback;
    procedure waitUntilFinished;
    procedure endPlayback;
    function getStatusBarText: String; override;
end; {tyoe}

type TRTTransferTrack = class(TRTBasePlayTrack)
  protected
    procedure doRun; override;
    function getTaskDescription: String; override;
end; {type}

type TRTPreviewTrack = class(TRTBasePlayTrack)
  protected
    procedure doRun; override;
    function getTaskDescription: String; override;
end; {type}

implementation

uses URipMDSPC3RecvPackets, URipMDSPC3SendPackets, URipSleep;

{ TRTBasePlayTrack }

procedure TRTTransferTrack.doRun;
begin
  startPlayback;
  try
    { Unpause }
    FRipMDSPC3.SendPAcketAndFreeIt(TRPSendPause.create);
    waitUntilFinished;
    { Pause }
    FRipMDSPC3.SendPAcketAndFreeIt(TRPSendPause.create);
  finally
    endPlayback;
  end; {try..finally}
end;

function TRTTransferTrack.getTaskDescription: String;
begin
  result := 'Record Track "'+Title+'"';
end;

{ TRTBasePlayTrack }

procedure TRTBasePlayTrack.endPlayback;
begin
  FPlaying := false;
  FMediaPlayer.free;
end;

function TRTBasePlayTrack.getStatusBarText: String;
begin
  { Report current time situation if playing }
  result := '';
  if not FPlaying then
    exit;
  result := RipTimeString(FMediaPlayer.position div 1000) + ';' +
            RipTimeString(FMediaPlayer.length div 1000);
end;

procedure TRTBasePlayTrack.startPlayback;
var bolTimeOut: boolean;
    carStartTick: cardinal;
begin
  inherited;
  if not fileExists(Filename) then
    raise exception.create('File '+filename+' does not exist.');

  FMediaPlayer := TMediaPlayer.create(nil);
  with FMediaPlayer do
  begin
    parent := self.PArentWindow;
    visible := false;
    shareable := true;
    filename := self.fileName;
    wait := true;
    try
      open;
    except
    end;
    wait := false;
    play;
    carStartTick := getTickCount;
    repeat {wait until playing}
      bolTimeOut := getTickCount > (carStartTick + MEDIAPLAYER_TIMEOUT);
    until (mode = mpPlaying) or (bolTimeOut) or (Stopped);
    if bolTimeOut then
      raise exception.create('Timeout waiting for media player to start playing.');
  end; {with}
  FPlaying := true;
end; {start playback}

procedure TRTBasePlayTrack.waitUntilFinished;
begin
  with FMediaPLayer do
    repeat {wait until not playing}
      doProgress(position, length);
      sleep(50);
    until (not (mode = mpPlaying)) or (Stopped);
  FPlaying := false;
end;

{ TRTPreviewTrack }

procedure TRTPreviewTrack.doRun;
begin
  inherited;
  startPlayback;
  try
    waitUntilFinished;
  finally
    endPlayback;
  end; {try..finally}
end;

function TRTPreviewTrack.getTaskDescription: String;
begin
    result := 'Preview Track "'+Title+'"';
end;

end.
