object frmMain: TfrmMain
  Left = 229
  Top = 171
  Width = 700
  Height = 558
  Caption = 'Rip MDS-PC3 www.ripnet.co.uk'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pcMain: TPageControl
    Left = 0
    Top = 68
    Width = 692
    Height = 436
    ActivePage = tsDeck
    Align = alClient
    TabOrder = 0
    object tsDeck: TTabSheet
      Caption = 'Deck'
      ImageIndex = 1
      object pnlDash: TPanel
        Left = 0
        Top = 0
        Width = 684
        Height = 42
        Align = alTop
        BevelInner = bvSpace
        BevelOuter = bvLowered
        TabOrder = 0
        object ToolBar1: TToolBar
          Left = 2
          Top = 2
          Width = 680
          Height = 47
          ButtonHeight = 36
          ButtonWidth = 46
          Caption = 'ToolBar1'
          HotImages = imgActions
          Images = imgActions
          ParentShowHint = False
          ShowCaptions = True
          ShowHint = True
          TabOrder = 0
          object btnInitial: TToolButton
            Left = 0
            Top = 2
            Action = actDeckRefreshTrackList
          end
          object ToolButton3: TToolButton
            Left = 46
            Top = 2
            Action = actDeckPowerOff
          end
          object ToolButton2: TToolButton
            Left = 92
            Top = 2
            Action = actDeckPlay
          end
          object ToolButton6: TToolButton
            Left = 138
            Top = 2
            Action = actDeckPause
          end
          object ToolButton5: TToolButton
            Left = 184
            Top = 2
            Action = actDeckPrevTrack
          end
          object ToolButton4: TToolButton
            Left = 230
            Top = 2
            Action = actDeckNextTrack
          end
          object ToolButton7: TToolButton
            Left = 276
            Top = 2
            Action = actDeckStop
          end
          object ToolButton9: TToolButton
            Left = 322
            Top = 2
            Action = actDeckStartRew
          end
          object ToolButton10: TToolButton
            Left = 368
            Top = 2
            Action = actDeck1x
          end
          object ToolButton8: TToolButton
            Left = 414
            Top = 2
            Action = actDeckStartFF
            Caption = 'FF2'
          end
          object ToolButton1: TToolButton
            Left = 460
            Top = 2
            Action = actDeckEject
          end
          object ToolButton15: TToolButton
            Left = 506
            Top = 2
            Action = actDeckUpdateTitles
          end
          object ToolButton19: TToolButton
            Left = 552
            Top = 2
            Action = actDeckFreeDB
            Caption = 'Free DB'
          end
        end
      end
      object pnlTrackList: TPanel
        Left = 0
        Top = 42
        Width = 684
        Height = 366
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 1
        inline frTrackListPlay: TfrTrackList
          Left = 2
          Top = 2
          Width = 680
          Height = 362
          Align = alClient
          inherited dbGrid: TDBCtrlGrid
            Width = 680
            Height = 309
            PanelHeight = 15
            PanelWidth = 663
            inherited Shape1: TShape
              Top = 14
              Width = 663
            end
            inherited pnlOnCtrlGrid: TPanel
              Width = 663
              Height = 14
              inherited dbeTrackNo: TDBText
                OnClick = frTrackListPlaydbeTrackNoClick
              end
            end
          end
          inherited edtHint: TEdit
            Text = 'Click Refresh To Begin...'
          end
          inherited Panel1: TPanel
            Top = 338
            Width = 680
          end
          inherited Panel2: TPanel
            Width = 680
          end
        end
      end
    end
    object tsRecord: TTabSheet
      Caption = 'Record'
      ImageIndex = 2
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 684
        Height = 43
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object ToolBar2: TToolBar
          Left = 2
          Top = 2
          Width = 680
          Height = 41
          ButtonHeight = 36
          ButtonWidth = 58
          Caption = 'ToolBar1'
          Images = imgActions
          ParentShowHint = False
          ShowCaptions = True
          ShowHint = True
          TabOrder = 0
          object ToolButton11: TToolButton
            Left = 0
            Top = 2
            Action = actRecordEraseDisc
          end
          object ToolButton14: TToolButton
            Left = 58
            Top = 2
            Action = actRecordRecord
          end
          object ToolButton12: TToolButton
            Left = 116
            Top = 2
            Action = actDeckPause
          end
          object ToolButton13: TToolButton
            Left = 174
            Top = 2
            Action = actDeckStop
          end
          object ToolButton16: TToolButton
            Left = 232
            Top = 2
            Action = actRecordTransfer
          end
          object ToolButton17: TToolButton
            Left = 290
            Top = 2
            Action = actRecordPreview
          end
          object ToolButton18: TToolButton
            Left = 348
            Top = 2
            Action = actDeckEject
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 43
        Width = 684
        Height = 365
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 1
        inline frTrackListRecord: TfrTrackList
          Left = 2
          Top = 2
          Width = 680
          Height = 361
          Align = alClient
          inherited dbGrid: TDBCtrlGrid
            Width = 680
            Height = 308
            PanelHeight = 15
            PanelWidth = 663
            inherited Shape1: TShape
              Top = 14
              Width = 663
            end
            inherited pnlOnCtrlGrid: TPanel
              Width = 663
              Height = 14
            end
          end
          inherited edtHint: TEdit
            Text = 'Drag MP3 or WAV Files Here To Record...'
          end
          inherited Panel1: TPanel
            Top = 337
            Width = 680
          end
          inherited Panel2: TPanel
            Width = 680
          end
        end
      end
    end
    object tsExperiment: TTabSheet
      Caption = 'Experiment'
      object pnlTop: TPanel
        Left = 0
        Top = 0
        Width = 684
        Height = 408
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object PageControl2: TPageControl
          Left = 209
          Top = 2
          Width = 473
          Height = 404
          ActivePage = TabSheet3
          Align = alClient
          TabOrder = 0
          object TabSheet3: TTabSheet
            Caption = 'All Received'
            object memAllReceived: TMemo
              Left = 0
              Top = 0
              Width = 465
              Height = 376
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 0
              WordWrap = False
            end
          end
          object TabSheet4: TTabSheet
            Caption = 'Unknown Only'
            ImageIndex = 1
            object memUnknownReceived: TMemo
              Left = 0
              Top = 0
              Width = 585
              Height = 341
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 0
              WordWrap = False
            end
          end
          object TabSheet5: TTabSheet
            Caption = 'Known Only'
            ImageIndex = 2
            object memKnownOnly: TMemo
              Left = 0
              Top = 0
              Width = 425
              Height = 379
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 0
              WordWrap = False
            end
          end
          object tsLogAllPackets: TTabSheet
            Caption = 'All Packets'
            ImageIndex = 3
            object pnlLogAllPackets: TPanel
              Left = 0
              Top = 0
              Width = 465
              Height = 379
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
            end
          end
          object tsGeneralLog: TTabSheet
            Caption = 'General Log'
            ImageIndex = 4
            object pnlLog: TPanel
              Left = 0
              Top = 0
              Width = 465
              Height = 379
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
            end
          end
        end
        object Panel3: TPanel
          Left = 2
          Top = 2
          Width = 207
          Height = 404
          Align = alLeft
          TabOrder = 1
          object btnOpen: TButton
            Left = 8
            Top = 8
            Width = 75
            Height = 25
            Caption = 'Open:'
            TabOrder = 0
            OnClick = btnOpenClick
          end
          object btnClear: TButton
            Left = 112
            Top = 8
            Width = 75
            Height = 25
            Caption = 'Clear'
            TabOrder = 1
            OnClick = btnClearClick
          end
          object btnClose: TButton
            Left = 8
            Top = 40
            Width = 75
            Height = 25
            Caption = 'Close'
            TabOrder = 2
            OnClick = btnCloseClick
          end
          object lbSequences: TListBox
            Left = 8
            Top = 72
            Width = 185
            Height = 217
            ItemHeight = 13
            Items.Strings = (
              '04 00 60 b0 26 (Bye1)'
              '00 60 02 (Bye2)'
              '04 00 60 b0 6a (Identify Hardware)'
              ' ')
            TabOrder = 3
            OnClick = lbSequencesClick
            OnDblClick = lbSequencesDblClick
          end
          object edtBytes: TEdit
            Left = 8
            Top = 288
            Width = 185
            Height = 21
            TabOrder = 4
          end
          object btnWriteChunk: TButton
            Left = 8
            Top = 312
            Width = 75
            Height = 25
            Caption = 'Write Chunk'
            TabOrder = 5
            OnClick = btnWriteChunkClick
          end
          object chkLog: TCheckBox
            Left = 117
            Top = 48
            Width = 58
            Height = 17
            Caption = 'Log'
            TabOrder = 6
          end
        end
      end
    end
    object tsDebug: TTabSheet
      Caption = 'Debug'
      ImageIndex = 3
      object memDebug: TMemo
        Left = 0
        Top = 0
        Width = 684
        Height = 403
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object pnlDisplay: TPanel
    Left = 0
    Top = 0
    Width = 692
    Height = 68
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 520
      Top = 43
      Width = 26
      Height = 13
      Caption = 'Time:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblDashPlayTime: TLabel
      Left = 552
      Top = 37
      Width = 86
      Height = 23
      Alignment = taCenter
      AutoSize = False
      Caption = '--:--:--'
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 14
      Width = 28
      Height = 13
      Caption = 'Track'
    end
    object lblDashTrack: TLabel
      Left = 88
      Top = 8
      Width = 551
      Height = 23
      AutoSize = False
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clAqua
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 43
      Width = 24
      Height = 13
      Caption = 'LCD:'
    end
    object lblDashLCD: TLabel
      Left = 48
      Top = 37
      Width = 457
      Height = 23
      AutoSize = False
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clLime
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object lblTrackNo: TLabel
      Left = 48
      Top = 8
      Width = 33
      Height = 23
      AutoSize = False
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clAqua
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
  end
  object actActions: TActionList
    OnExecute = actActionsExecute
    Left = 206
    Top = 5
    object actDeckRefreshTrackList: TAction
      Category = 'Deck'
      Caption = 'Refresh'
      Hint = 'Refresh Track Listing'
      OnExecute = actDeckRefreshTrackListExecute
    end
    object actDeckPlay: TAction
      Category = 'Deck'
      Caption = 'Play'
      Hint = 'Play'
      ImageIndex = 1
      OnExecute = actDeckPlayExecute
    end
    object actDeckPowerOff: TAction
      Category = 'Deck'
      Caption = 'Pwr Off'
      Hint = 'Turns Off The Minidisc Deck'
      OnExecute = actDeckPowerOffExecute
    end
    object actDeckEject: TAction
      Category = 'Deck'
      Caption = 'Eject'
      Hint = 'Eject the current disc'
      ImageIndex = 3
      OnExecute = actDeckEjectExecute
    end
    object actDeckPause: TAction
      Category = 'Deck'
      Caption = 'Pause'
      Hint = 'Pause / Unpause Playback'
      ImageIndex = 5
      OnExecute = actDeckPauseExecute
    end
    object actDeckStop: TAction
      Category = 'Deck'
      Caption = 'Stop'
      Hint = 'Stop Playback or Record'
      ImageIndex = 2
      OnExecute = actDeckStopExecute
    end
    object actDeckNextTrack: TAction
      Category = 'Deck'
      Caption = 'Next'
      Hint = 'Next Track'
      ImageIndex = 7
      OnExecute = actDeckNextTrackExecute
    end
    object actDeckPrevTrack: TAction
      Category = 'Deck'
      Caption = 'Prev.'
      Hint = 'Previous Track'
      ImageIndex = 6
      OnExecute = actDeckPrevTrackExecute
    end
    object actDeckStartFF: TAction
      Category = 'Deck'
      Caption = 'FF'
      Hint = 'Fast Forward'
      ImageIndex = 3
      OnExecute = actDeckStartFFExecute
    end
    object actDeckStartRew: TAction
      Category = 'Deck'
      Caption = 'Rew'
      Hint = 'Rewind'
      ImageIndex = 4
      OnExecute = actDeckStartRewExecute
    end
    object actDeck1x: TAction
      Category = 'Deck'
      Caption = '1x'
      Hint = 'Play at 1x speed again'
      ImageIndex = 10
      OnExecute = actDeck1xExecute
    end
    object actRecordEraseDisc: TAction
      Category = 'Record'
      Caption = 'Erase Disc'
      Hint = 'Erases The Whole Minidisc'
      ImageIndex = 11
      OnExecute = actRecordEraseDiscExecute
    end
    object actRecordRecord: TAction
      Category = 'Record'
      Caption = 'Record'
      Hint = 'Start Recording'
      ImageIndex = 0
      OnExecute = actRecordRecordExecute
    end
    object actHelpAbout: TAction
      Category = 'Help'
      Caption = 'About...'
      ImageIndex = 13
      OnExecute = actHelpAboutExecute
    end
    object actFileExit: TAction
      Category = 'File'
      Caption = 'Exit'
      ImageIndex = 14
      OnExecute = actFileExitExecute
    end
    object actRecordTransfer: TAction
      Category = 'Record'
      Caption = 'Transfer'
      Hint = 'Transfers record list to Minidisc'
      ImageIndex = 19
      OnExecute = actRecordTransferExecute
    end
    object actDeckUpdateTitles: TAction
      Category = 'Deck'
      Caption = 'Titles'
      Hint = 'Saves Changes To Titles'
      OnExecute = actDeckUpdateTitlesExecute
    end
    object actToolsOptions: TAction
      Category = 'Tools'
      Caption = 'Options...'
      OnExecute = actToolsOptionsExecute
    end
    object actRecordPreview: TAction
      Category = 'Record'
      Caption = 'Preview...'
      Hint = 'Listen to the track currently selected in record tab.'
      OnExecute = actRecordPreviewExecute
    end
    object actDeckFreeDB: TAction
      Category = 'Deck'
      Caption = 'Free DB...'
      Hint = 'Lookup Disc On FreeDB (Like an open version of CDDB)'
      OnExecute = actDeckFreeDBExecute
    end
    object actDebugTestTaskManager: TAction
      Category = 'Debug'
      Caption = 'Test Task Manager...'
      OnExecute = actDebugTestTaskManagerExecute
    end
  end
  object MainMenu1: TMainMenu
    Images = imgActions
    Left = 276
    Top = 1
    object File1: TMenuItem
      Caption = '&File'
      object actFileExit1: TMenuItem
        Action = actFileExit
      end
    end
    object Deck1: TMenuItem
      Caption = '&Deck'
      object Initialise1: TMenuItem
        Action = actDeckRefreshTrackList
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object actDeckPlay1: TMenuItem
        Action = actDeckPlay
      end
      object Stop1: TMenuItem
        Action = actDeckStop
      end
      object Pause1: TMenuItem
        Action = actDeckPause
      end
      object Next1: TMenuItem
        Action = actDeckNextTrack
      end
      object Previous1: TMenuItem
        Action = actDeckPrevTrack
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Rew1: TMenuItem
        Action = actDeckStartRew
      end
      object N1x1: TMenuItem
        Action = actDeck1x
      end
      object FF1: TMenuItem
        Action = actDeckStartFF
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object FreeDB1: TMenuItem
        Action = actDeckFreeDB
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object Eject1: TMenuItem
        Action = actDeckEject
      end
      object PowerOff1: TMenuItem
        Action = actDeckPowerOff
      end
    end
    object Record1: TMenuItem
      Caption = '&Record'
      object EraseDisc1: TMenuItem
        Action = actRecordEraseDisc
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Record2: TMenuItem
        Action = actRecordRecord
      end
      object Transfer1: TMenuItem
        Action = actRecordTransfer
      end
      object Pause2: TMenuItem
        Action = actDeckPause
      end
      object Stop2: TMenuItem
        Action = actDeckStop
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object Preview1: TMenuItem
        Action = actRecordPreview
      end
    end
    object Tools1: TMenuItem
      Caption = '&Tools'
      object Options1: TMenuItem
        Action = actToolsOptions
      end
    end
    object Help1: TMenuItem
      Caption = '&Help'
      object About1: TMenuItem
        Action = actHelpAbout
      end
    end
    object Debug1: TMenuItem
      Caption = '&Debug'
      object TestTaskManager1: TMenuItem
        Action = actDebugTestTaskManager
      end
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnMessage = ApplicationEvents1Message
    Left = 204
    Top = 229
  end
  object imgActions: TImageList
    Left = 254
    Top = 5
    Bitmap = {
      494C010108000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001001000000000000018
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000001000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001F001F00000000000000
      0000000000000000100000000000000000000000000000000000100000000000
      000000000000000000001F001F00000000000000000000000000000000000000
      1000000000000000000010001F00000000000000000000000000000000000000
      000000000000000000000000000000000000000000001F001F00000000000000
      00000000000010001F00000000000000000000000000000000001F0010000000
      000000000000000000001F001F00000000000000000000000000000000001000
      1F0000000000000010001F001F00000000000000000000000000000000000000
      000000000000000000000000000000000000000000001F001F00000000000000
      0000000010001F001F00000000000000000000000000000000001F001F001000
      000000000000000000001F001F00000000000000000000000000000010001F00
      1F000000000010001F001F001F00000000000000000000000000000000000000
      000000000000000000000000000000000000000000001F001F00000000000000
      000010001F001F001F00000000000000000000000000000000001F001F001F00
      100000000000000000001F001F0000000000000000000000000010001F001F00
      1F001F001F001F001F001F001F00000000000000000000000000000000000000
      000000000000000000000000000000000000000000001F001F00000000000000
      10001F001F001F001F00000000000000000000000000000000001F001F001F00
      1F0010000000000000001F001F000000000000000000000010001F001F001F00
      1F001F001F001F001F001F001F00000000000000000000000000000000000000
      000000000000000000000000000000000000000000001F001F00000000001F00
      1F001F001F001F001F00000000000000000000000000000000001F001F001F00
      1F001F001F00000000001F001F000000000000000000000010001F001F001F00
      1F001F001F001F001F0010001F000000000000000000000010001F0010000000
      0000000010001F0010000000000000000000000000001F001F00000000001F00
      1F001F001F0010001F00000000000000000000000000000000001F0010001F00
      1F001F001F00000000001F001F0000000000000000000000000010001F001F00
      1F001F001F001F00100010001F00000000000000000000001F001F001F000000
      000000001F001F001F000000000000000000000000001F001F00000000000000
      10001F00100010001F00000000000000000000000000000000001F0010001000
      1F0010000000000000001F001F00000000000000000000000000000010001F00
      1F000000000010001F001F001F00000000000000000000001F001F001F000000
      000000001F001F001F000000000000000000000000001F001F00000000000000
      000010001F001F001F00000000000000000000000000000000001F001F001F00
      100000000000000000001F001F00000000000000000000000000000000001000
      1F0000000000000010001F001F00000000000000000000001F001F001F000000
      000000001F001F001F000000000000000000000000001F001F00000000000000
      0000000010001F001F00000000000000000000000000000000001F001F001000
      000000000000000000001F001F00000000000000000000000000000000000000
      1000000000000000000010001F000000000000000000000010001F0010000000
      0000000010001F0010000000000000000000000000001F001F00000000000000
      00000000000010001F00000000000000000000000000000000001F0010000000
      000000000000000000001F001F00000000000000000000000000000000000000
      0000000000000000000000001000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001F001F00000000000000
      0000000000000000100000000000000000000000000000000000100000000000
      000000000000000000001F001F00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000010000000000000000000
      0000000000000000000000000000000000000000000010001F001F001F001F00
      1F001F001F001F001F001F001000000000000000000010000000000000000000
      0000000000000000000000000000000000000000000000000000000000000040
      007C007C0040000000000000000000000000000000001F001F00100000000000
      000000000000000000000000000000000000000000001F001F001F001F001F00
      1F001F001F001F001F001F001F0000000000000000001F001000000000000000
      00001000000000000000000000000000000000000000000000000040007C007C
      007C007C007C007C00400000000000000000000000001F001F001F001F001000
      000000000000000000000000000000000000000000001F001F001F001F001F00
      1F001F001F001F001F001F001F0000000000000000001F001F00100000000000
      00001F0010000000000000000000000000000000000000000000007C007C007C
      007C007C007C007C007C0000000000000000000000001F001F001F001F001F00
      1F0010000000000000000000000000000000000000001F001F001F001F001F00
      1F001F001F001F001F001F001F0000000000000000001F001F001F0010000000
      00001F001F001000000000000000000000000000000000000040007C007C007C
      007C007C007C007C007C0040000000000000000000001F001F001F001F001F00
      1F001F001F00100000000000000000000000000000001F001F001F001F001F00
      1F001F001F001F001F001F001F0000000000000000001F001F001F001F001F00
      1F001F001F001F0010000000000000000000000000000000007C007C0040007C
      007C007C007C007C007C007C000000000000000000001F001F001F001F001F00
      1F001F001F001F001F001000000000000000000000001F0010001F001F001F00
      1F001F001F001F001F001F001F0000000000000000001F001F001F001F001F00
      1F001F001F001F001F001000000000000000000000000000007C007C0040007C
      007C007C007C007C007C007C000000000000000000001F0010001F001F001F00
      1F001F001F001F001F001000000000000000000000001F0010001F001F001F00
      1F001F001F001F001F001F001F0000000000000000001F0010001F001F001F00
      1F001F001F001F001F0010000000000000000000000000000040007C007C0040
      007C007C007C007C007C0040000000000000000000001F0010001F001F001F00
      1F001F001F00100000000000000000000000000000001F0010001F001F001F00
      1F001F001F001F001F001F001F0000000000000000001F00100010001F001F00
      1F001F001F001F00100000000000000000000000000000000000007C007C007C
      00400040007C007C007C0000000000000000000000001F001F0010001F001F00
      1F0010000000000000000000000000000000000000001F001F0010001F001F00
      1F001F001F001F001F001F001F0000000000000000001F001F001F0010000000
      00001F001F0010000000000000000000000000000000000000000040007C007C
      007C007C007C007C00400000000000000000000000001F001F001F001F001000
      000000000000000000000000000000000000000000001F001F001F0010001000
      10001F001F001F001F001F001F0000000000000000001F001F00100000000000
      00001F0010000000000000000000000000000000000000000000000000000040
      007C007C0040000000000000000000000000000000001F001F00100000000000
      000000000000000000000000000000000000000000001F001F001F001F001F00
      1F001F001F001F001F001F001F0000000000000000001F001000000000000000
      0000100000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000010000000000000000000
      0000000000000000000000000000000000000000000010001F001F001F001F00
      1F001F001F001F001F001F001000000000000000000010000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFBFFFF87E7E7E1
      FE71FFFF87C7E3E1FC61FFFF8787E1E1F841FFFF8707E0E1F001FFFF8607E061
      E001FFFF8407E021C001E38F8007E001C001C1078007E001E001C1078407E021
      F001C1078607E061F841C1078707E0E1FC61C1078787E1E1FEF1E38F87C7E3E1
      FFFBFFFF87E7F7E1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FFF80019EFF
      FC3F87FF80018E7FF00F81FF8001863FE007807F8001821FE007801F8001800F
      C003800780018007C003800380018003C003800380018003C003800780018007
      E007801F8001800FE007807F8001821FF00F81FF8001863FFC3F87FF80018E7F
      FFFF9FFF80019EFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object NMNNTP1: TNMNNTP
    Port = 119
    ReportLevel = 0
    CacheMode = cmMixed
    ParseAttachments = False
    Left = 332
    Top = 289
  end
end
