unit UfrmOptions;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UfrmRipTreeOptionsBase, ExtCtrls, ComCtrls, StdCtrls, UfrColourSelector;

type
  TfrmOptions = class(TfrmRipTreeOptionsBase)
    TabSheet2: TTabSheet;
    chkStartupAutoRefreshTrackListing: TCheckBox;
    TabSheet3: TTabSheet;
    Label1: TLabel;
    edtFreeDBServer: TEdit;
    Memo1: TMemo;
    Memo2: TMemo;
    TabSheet4: TTabSheet;
    csTrackNumberLCDColor: TfrColourSelector;
    csTrackNameLCDColor: TfrColourSelector;
    csMainLCDColor: TfrColourSelector;
    csTimeLCDColor: TfrColourSelector;
    csPlaySelectedColor: TfrColourSelector;
    csRecordSelectedColor: TfrColourSelector;
  private
  public
  protected
  end;

{var
  frmOptions: TfrmOptions;}

implementation

{$R *.DFM}

{ TfrmOptions }

{ TfrmOptions }

end.
