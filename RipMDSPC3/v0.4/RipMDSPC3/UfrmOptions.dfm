inherited frmOptions: TfrmOptions
  Left = 272
  Top = 241
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlPAgeView: TPanel
    inherited pcPages: TPageControl
      ActivePage = TabSheet3
      object TabSheet3: TTabSheet
        Caption = 'Settings.FreeDB (CDDB)'
        ImageIndex = 2
        object Label1: TLabel
          Left = 12
          Top = 24
          Width = 70
          Height = 13
          Caption = 'FreeDB Server'
        end
        object edtFreeDBServer: TEdit
          Left = 88
          Top = 20
          Width = 209
          Height = 21
          TabOrder = 0
          Text = 'fr.freedb.org'
        end
        object Memo2: TMemo
          Left = 12
          Top = 48
          Width = 287
          Height = 133
          Color = clBtnFace
          Lines.Strings = (
            'Enter the name of a FreeDB server here. FreeDB is similar'
            'to CDDB, except it doesnt make you agree to unfair'
            'licenceing terms, and is far more '#39'open'#39'. See'
            'www.freedb.org for a list of servers.'
            ''
            'FreeDB will allow you to automatically look up track listings '
            'for a MiniDisc on the internet, and title the disc. This works '
            'great when you use a HI-FI to dub from CD to MD and '
            'need to title the disc.'
            ' ')
          TabOrder = 1
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Settings.Startup'
        ImageIndex = 1
        object chkStartupAutoRefreshTrackListing: TCheckBox
          Left = 12
          Top = 16
          Width = 169
          Height = 17
          Caption = 'Auto-Refresh Track Listing'
          TabOrder = 0
        end
        object Memo1: TMemo
          Left = 12
          Top = 48
          Width = 287
          Height = 51
          Color = clBtnFace
          Lines.Strings = (
            'Check this box to make the program automatically read the '
            'track listing from the Minidisc on loading. This causes a '
            'delay opening the program.')
          TabOrder = 1
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Settings.Colours'
        ImageIndex = 3
        inline csTrackNumberLCDColor: TfrColourSelector
          Width = 305
          Align = alTop
          inherited Label1: TLabel
            Width = 95
            Caption = 'Track Number LCD:'
          end
          inherited shapeColor: TShape
            Brush.Color = clAqua
          end
        end
        inline csTrackNameLCDColor: TfrColourSelector
          Top = 25
          Width = 305
          Align = alTop
          TabOrder = 1
          inherited Label1: TLabel
            Width = 86
            Caption = 'Track Name LCD:'
          end
          inherited shapeColor: TShape
            Brush.Color = clAqua
          end
        end
        inline csMainLCDColor: TfrColourSelector
          Top = 50
          Width = 305
          Align = alTop
          TabOrder = 2
          inherited Label1: TLabel
            Width = 50
            Caption = 'Main LCD:'
          end
          inherited shapeColor: TShape
            Brush.Color = clLime
          end
        end
        inline csTimeLCDColor: TfrColourSelector
          Top = 75
          Width = 305
          Align = alTop
          TabOrder = 3
          inherited Label1: TLabel
            Width = 50
            Caption = 'Time LCD:'
          end
          inherited shapeColor: TShape
            Brush.Color = clLime
          end
        end
        inline csPlaySelectedColor: TfrColourSelector
          Top = 100
          Width = 305
          Align = alTop
          TabOrder = 4
          inherited Label1: TLabel
            Width = 118
            Caption = 'Play List Selected Track:'
          end
          inherited shapeColor: TShape
            Brush.Color = clYellow
          end
        end
        inline csRecordSelectedColor: TfrColourSelector
          Top = 125
          Width = 305
          Align = alTop
          TabOrder = 5
          inherited Label1: TLabel
            Caption = 'Record List Selected Track:'
          end
          inherited shapeColor: TShape
            Brush.Color = clYellow
          end
        end
      end
    end
  end
end
