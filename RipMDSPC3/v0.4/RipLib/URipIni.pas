unit URipIni;

interface
uses IniFiles, sysUtils, forms, Classes;

type TRipIniFile = class(TIniFile)
  public
    function ReadText(const Section, Ident, Default: string): string;
    procedure WriteText(const Section, Ident, Value: string);
end; {type}

var RipIni: TRipIniFile;

implementation

{ TRipIniFile }

function TRipIniFile.ReadText(const Section, Ident, Default: string): string;
var intCount: integer;
    slTemp: TStringList;
    i: integer;
begin
  slTemp := TStringList.create;
  intCount := readInteger(Section, Ident+'.count', 01);
  for i := 0 to intCount-1 do
    slTemp.add(readString(Section, Ident+'['+intToStr(i)+']', ''));
  result := slTemp.text;
  if trim(result) = '' then
    result := Default;
  slTemp.free;
end;

procedure TRipIniFile.WriteText(const Section, Ident, Value: string);
var i:integer;
    slTemp: TStringList;
begin
  slTemp := TStringList.create;
  slTemp.text := Value;
  writeInteger(Section, Ident+'.count', slTemp.count);
  for i := 0 to slTemp.count-1 do
    writeString(Section, Ident+'['+intToStr(i)+']', slTemp[i]);
  slTemp.free;
end;

initialization
  RipIni := TRipIniFile.create(changeFileExt(application.exename, '.ini'));
finalization
  RipIni.free;
  RipIni := nil;
end.
