unit URipAddBackslash;

interface

function RipAddBackslash(strPath: String):String;

implementation

function RipAddBackslash(strPath: String):String;
begin
  result := strPath;
  if result = '' then
  begin
    result := '\';
    exit;
  end; {if}
  if result[length(result)] <> '\' then
    result := result + '\';
end; {RipAddBackslash}
end.
