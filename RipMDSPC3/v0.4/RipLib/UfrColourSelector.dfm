object frColourSelector: TfrColourSelector
  Left = 0
  Top = 0
  Width = 250
  Height = 25
  TabOrder = 0
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 133
    Height = 13
    Caption = 'Replace Me With Whatever'
  end
  object shapeColor: TShape
    Left = 152
    Top = 6
    Width = 73
    Height = 17
    OnMouseUp = shapeColorMouseUp
  end
  object btnRandom: TButton
    Left = 228
    Top = 6
    Width = 14
    Height = 16
    Hint = 'Random Colour!!!'
    Caption = 'R'
    TabOrder = 0
    OnClick = btnRandomClick
  end
  object ColorDialog1: TColorDialog
    Ctl3D = True
    Left = 248
    Top = 8
  end
end
