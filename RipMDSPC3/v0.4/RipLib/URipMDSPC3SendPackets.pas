unit URipMDSPC3SendPackets;

interface
uses URipSonyUSBPCLink, SysUtils, classes;

type TRPSendTurnOnTimeInfo = class(TRipUSBPacket)
  public
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPSendPlay = class(TRipUSBPacket)
  public
    TrackNo: integer;
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPSendTitle = class(TRipUSBPacket)
  public
    TrackNo: integer;
    ChunkIndex: integer;
    Title: String;
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPSendEraseDisc = class(TRipUSBPacket)
  public
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPSendRecord = class(TRipUSBPacket)
  public
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPBaseSendByteCommand = class(TRipUSBPacket)
  public
  protected
    procedure setBufFromSpecificFields; override;
    function getByteCommand: byte; virtual; abstract;
end; {type}

type TRPSendPause = class(TRPBaseSendByteCommand)
  public
  protected
    function getByteCommand: byte; override;
end; {type}

type TRPSendNextTrack = class(TRPBaseSendByteCommand)
  public
  protected
    function getByteCommand: byte; override;
end; {type}

type TRPSendStartFF = class(TRPBaseSendByteCommand)
  public
  protected
    function getByteCommand: byte; override;
end; {type}

type TRPSendStartRew = class(TRPBaseSendByteCommand)
  public
  protected
    function getByteCommand: byte; override;
end; {type}

type TRPSendStopRewOrFF = class(TRPBaseSendByteCommand)
  public
  protected
    function getByteCommand: byte; override;
end; {type}

type TRPSendPrevTrack = class(TRPBaseSendByteCommand)
  public
  protected
    function getByteCommand: byte; override;
end; {type}

type TRPSendStop = class(TRPBaseSendByteCommand)
  public
  protected
    function getByteCommand: byte; override;
end; {type}

type TRPSendGetDiscInfo = class(TRipUSBPacket)
  public
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPSendGetCurrentTrackInfo = class(TRipUSBPacket)
  public
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPSendEject = class(TRipUSBPacket)
  public
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPSendPowerOff = class(TRipUSBPacket)
  public
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPSendGetFullTitle = class(TRipUSBPacket)
  public
    TrackNo: integer;
    ChunkIndex: integer;
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPSendGetTrackLength = class(TRipUSBPacket)
  public
    TrackNo: integer;
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPSendFade = class(TRipUSBPacket)
  public
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPSendIDHardware = class(TRipUSBPacket)
  public
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

type TRPSendInitial = class(TRipUSBPacket)
  public
  protected
    procedure setBufFromSpecificFields; override;
end; {type}

implementation

procedure TRPSendPlay.setBufFromSpecificFields;
begin
  setBuf([$08, $00, $60, $B0, $50, $00, $01, TrackNo, $00]);
end;

{ TRPRecvTrackChange }



procedure TRPSendFade.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$04, $00, $60, $B0, $0B]);
end;

{ TRPSendInitial }

procedure TRPSendInitial.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$00, $60, $00]);
end;

{ TRPSendIDHardware }

procedure TRPSendIDHardware.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$04, $00, $60, $B0, $6A]);
end;

{ TRPRecvIDHardware }

{ TRPSendGetFullTitle }

procedure TRPSendGetFullTitle.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$09, $00, $60, $B0, $48, $00, $01, TrackNo, $00, ChunkIndex]);
end;

{ TRPSendGetTrackLength }

procedure TRPSendGetTrackLength.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$07, $00, $60, $B0, $45, $00, $01, TrackNo]);
end;

{ TRPSendGetDiscInfo }

procedure TRPSendGetDiscInfo.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$06, $00, $60, $B0, $0e, $00, $01]);
end;

{ TRPSendPowerOff }

procedure TRPSendPowerOff.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$04, $00, $60, $B0, $2F]);
end;



{ TRPSendEject }

procedure TRPSendEject.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$07, $00, $60, $B0, $04, $00, $00, $00]);
end;



{ TRPSendGetCurrentTrackInfo }

procedure TRPSendGetCurrentTrackInfo.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$04, $00, $60, $b0, $0f]);
end;

{ TRPBaseSendByteCommand }

procedure TRPBaseSendByteCommand.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$04, $00, $60, $b0, getbyteCommand]);
end;

{ TRPSendPause }

function TRPSendPause.getByteCommand: byte;
begin
  result := $03; {Pause}
end;

{ TRPSendStop }

function TRPSendStop.getByteCommand: byte;
begin
  result := $01;
end;

{ TRPSendPrevTrack }

function TRPSendPrevTrack.getByteCommand: byte;
begin
  result := $9;
end;

{ TRPSendNextTrack }

function TRPSendNextTrack.getByteCommand: byte;
begin
  result := $8;
end;

{ TRPSendStartRew }

function TRPSendStartRew.getByteCommand: byte;
begin
  result := $13;
end;

{ TRPSendStartFF }

function TRPSendStartFF.getByteCommand: byte;
begin
  result := $12;
end;

{ TRPSendStopRewOrFF }

function TRPSendStopRewOrFF.getByteCommand: byte;
begin
  result := $1f;
end;

{ TRPSendEraseDisc }

procedure TRPSendEraseDisc.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$07, $00, $60, $b0, $33, $01, $00, $00]);
end;

{ TRPSendRecord }

procedure TRPSendRecord.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$04, $00, $60, $b0, $07]);
end;

{ TRPSendTitle }

procedure TRPSendTitle.setBufFromSpecificFields;
var i:integer;
begin
  inherited;
  {         19  00   60   B0   98   00   01   00       00      01  4D 79 20 41  6C 62 75 6D  00 00 00 00  00 00 00 00}
  setBuf([$19, $00, $60, $b0, $98, $00, $01, TrackNo, $00, ChunkIndex, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0]);
  for i := 1 to length(Title) do
    if i < 17 then
    begin
      buf[i+9] := ord(title[i]);
    end; {if}
end;

{ TRPSendTurnOnTimeInfo }

procedure TRPSendTurnOnTimeInfo.setBufFromSpecificFields;
begin
  inherited;
  setBuf([$04, $00, $60, $B0, $25]);
end;

end.

