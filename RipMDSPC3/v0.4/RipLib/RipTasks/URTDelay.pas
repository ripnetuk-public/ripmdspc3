unit URTDelay;

interface
uses URipBaseTask, SysUtils, Classes;

type TRTDelay = class(TRipBaseTask)
  public
    PeriodMS: cardinal; {Period to wait for}
    Caption: String; {Optoional captioon override}
  protected
    procedure doRun; override;
    function getTaskDescription: String; override;    
end; {tyoe}


implementation
uses URipDElay;
{ TRTDelay }

procedure TRTDelay.doRun;
var qsCount: integer; {count of quarter seconds}
    i: integer;
begin
  { Sleep for periodMS ms }
  qsCount := periodMS div 250; { Number of quarter secs}
  doProgress(0,qsCount);
  for i := 1 to qsCount do
  begin
    ripDelay(250);
    doProgress(i,qsCount);
    if Stopped then
      exit;
  end; {for}
end;

function TRTDelay.getTaskDescription: String;
begin
  result := 'Wait for '+intToStr(periodms div 1000)+'s';
  if caption <> '' then
    result := caption;
end;

end.
