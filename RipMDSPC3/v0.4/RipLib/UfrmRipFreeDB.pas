unit UfrmRipFreeDB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,URipTimeString,
  URipLog, ExtCtrls, Psock, StdCtrls, ScktComp, NMHttp, URipStrip, ComCtrls;

type TToc = class
  public
    min: dword;
    sec: dword;
    frame: dword;
    constructor Create(Amin, Asec, Aframe: dword);
    function toString: String;
    function toFrame: integer;
    function toSec: integer;
end; {type}

type
  TfrmRipFreeDB = class(TForm)
    http: TNMHTTP;
    Panel1: TPanel;
    btnGo: TButton;
    pcMain: TPageControl;
    tsMatches: TTabSheet;
    tsDebug: TTabSheet;
    memResponse: TMemo;
    pnlLog: TPanel;
    lvMatches: TListView;
    Panel2: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    tsTracks: TTabSheet;
    memTracks: TMemo;
    timerGo: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure socketConnect(Sender: TObject);
    procedure socketConnectionFailed(Sender: TObject);
    procedure socketDisconnect(Sender: TObject);
    procedure socketHostResolved(Sender: TComponent);
    procedure socketInvalidHost(var Handled: Boolean);
    procedure socketStatus(Sender: TComponent; Status: String);
    procedure btnGoClick(Sender: TObject);
    procedure lvMatchesDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure timerGoTimer(Sender: TObject);
  private
    FLog: TRipLog;
    FToc: TList;
    function cddbSum(n: dword): dword;
    function getDiscID: dword;
    function getToc(idx: integer): TToc;
    function getTocCount: integer;
    procedure issueHTTPCommand(strCommand: STring);
    function getResponseCode: integer;
    function getResponse: TStrings;
    procedure buildMatchList;
    procedure processSelectedDisc;
  public
    Host: String;
    AutoStart: boolean;
    function makeCommandURL(strCommand: String):String;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure addTOC(Amin, Asec, Aframe: integer);
    procedure addTOCSecs(Asec: integer);
    procedure ClearTOC;
    procedure debugDumpTOC(sl: TSTrings);
    function getCDDBQueryCommand: String;

    property Toc[idx: integer]: TToc read getToc;
    property TocCount: integer read getTocCount;
    property DiscID: dword read getDiscID;
    property ResponseCode: integer read getResponseCode;
    property Response: TStrings read getResponse;
  end;

var
  frmRipFreeDB: TfrmRipFreeDB;

implementation

uses URipEncodeURL, URipDebugMode;

{$R *.DFM}

procedure TfrmRipFreeDB.addTOC(Amin, Asec, Aframe: integer);
begin
  FToc.add(TToc.create(Amin, Asec, Aframe));
end;

constructor TfrmRipFreeDB.Create(AOwner: TComponent);
begin
  inherited;
  if not RipDebugMode then
    tsDebug.tabVisible := false;
end;

destructor TfrmRipFreeDB.Destroy;
begin
  clearTOC;
  FToc.free;
  inherited;
end;

function TfrmRipFreeDB.cddbSum(n: dword): dword;
begin
  // For backward compatibility this algorithm must not change
  result := 0;

  while (n > 0) do
  begin
    result := result + (n mod 10);
    n := n div 10;
  end;
end; {cddbSum}

function TfrmRipFreeDB.getDiscID: dword;
var i: integer;
    t, n: DWord;
    r1, r2, r3: dword;
    tocX: TToc;
begin
  n := 0;
  // For backward compatibility this algorithm must not change */

  for i := 0 to TocCount-2 do
    n := n + cddbSum((Toc[i].min * 60) + Toc[i].sec);

  tocX := Toc[TocCount-1];

  t := ((tocX.min * 60) + tocX.sec) -
	    ((toc[0].min * 60) + toc[0].sec);

  r1 := (n mod $ff); {needs shl 24}
  r1 := r1 shl 24;

  r2 := t;
  r2 := r2 shl 8;

  r3 := TocCount-1;

  result := r1 or r2 or r3;
end; {discid}

function TfrmRipFreeDB.getToc(idx: integer): TToc;
begin
  result := TToc(FToc[idx]);
end;

function TfrmRipFreeDB.getTocCount: integer;
begin
  result := FToc.count;
end;

{ TToc }

constructor TToc.Create(Amin, Asec, Aframe: dword);
begin
  min := AMin;
  sec := ASEc;
  frame := AFrame;
end;

procedure TfrmRipFreeDB.ClearTOC;
begin
  while FToc.count > 0 do
  begin
    TObject(FToc[0]).free;
    FToc.delete(0);
  end; {while}
end;

procedure TfrmRipFreeDB.addTOCSecs(Asec: integer);
var m,s,f: integer;
begin
  m := Asec div 60;
  s := Asec mod 60;
  f := 0; {assume zero frame...}
  addTOC(m,s,f);
end;

procedure TfrmRipFreeDB.debugDumpTOC(sl: TSTrings);
var i:integer;
    s: String;
begin
  sl.clear;
  for i := 0 to TOCCount-2 do
  begin
    with toc[i] do
      s := 'Track '+intToStr(i+1)+': '+toString;
    sl.add(s);
  end; {for}
  s := 'Lead-Out: '+toc[tocCount-1].toString;
  sl.add(s);

  sl.add('CDDBID: '+intToHex(getDiscID,8));

  sl.add(getCDDBQueryCommand);

  sl.add(makeCommandURL(getCDDBQueryCommand));
end;

function TfrmRipFreeDB.getCDDBQueryCommand: String;
var strTrackFrameList: String;
    i: integer;
begin
  strTrackFRameList := '';
  for i := 0 to TOCCount-2 do
  begin
    if strTrackFrameList <> '' then
     strTrackFrameList := strTrackFrameList + ' ';
    strTrackFrameList := strTrackFrameList + intToStr(toc[i].toFrame);
  end; {for}
  result := 'cddb query '+intTOHex(getDiscID,8)+' '+intToStr(tocCount-1)+' '+strTrackFrameList+' '+intToStr(toc[TocCount-1].toSec);
//cddb query eb0cc50f 15 150 14637 29117 46315 62760 80090 100410 115440 134980 148167 164165 180817 199140 217575 231462 3271
end; {getCDDBQUERYCOMMAND}

function TToc.toFrame: integer;
begin
  result := (min*60+sec)*75+frame;
end;

function TToc.toSec: integer;
begin
  result := toFrame div 75;
end;

function TToc.toString: String;
begin
  result := ripTimeString(min, sec, frame);
end;

procedure TfrmRipFreeDB.FormCreate(Sender: TObject);
begin
  FToc := TList.create;
  FLog := TRipLog.create(Self);
  FLog.sitOn(pnlLog);
  Host := 'fr.freedb.org';
end;

procedure TfrmRipFreeDB.socketConnect(Sender: TObject);
begin
  FLog.logSuccess('Connect');
end;

procedure TfrmRipFreeDB.socketConnectionFailed(Sender: TObject);
begin
  FLog.logSuccess('Connect fail');
end;

procedure TfrmRipFreeDB.socketDisconnect(Sender: TObject);
begin
  FLog.logSuccess('disConnect');
end;

procedure TfrmRipFreeDB.socketHostResolved(Sender: TComponent);
begin
  FLog.logSuccess('host resolved');
end;

procedure TfrmRipFreeDB.socketInvalidHost(var Handled: Boolean);
begin
  FLog.logSuccess('invalid host');
end;

procedure TfrmRipFreeDB.socketStatus(Sender: TComponent; Status: String);
begin
  FLog.logSuccess('Stauts: '+Status);
end;

// http://fr.freedb.org/~cddb/cddb.cgi?cmd=cddb+query+910B1E0B+11+150+14700+32850+48900+68175+90900+107550+126750+145650+166800+185850+2848&hello=george+ripnet.co.uk+george+f&proto=5

function TfrmRipFreeDB.makeCommandURL(strCommand: String): String;
begin
  result := 'http://'+host+'/~cddb/cddb.cgi?cmd='+RipEncodeURL(strCommand)+'&hello=freedb+ripnet.co.uk+ripmdspc3+f&proto=5';
end;

procedure TfrmRipFReeDB.issueHTTPCommand(strCommand: STring);
begin
  http.get(makeCommandURL(strCommand));
  response.text := http.body;
end; {issueHTTPCommand}

procedure TfrmRipFreeDB.btnGoClick(Sender: TObject);
begin
  screen.cursor := crHourglass;
  try
    issueHTTPCommand(getCDDBQueryCommand);
    case responseCode of
      200:
      begin
        FLog.logSuccess('Found Exact Match(es).');
        buildMatchList;
      end; {200}
      211:
      begin
        FLog.logSuccess('Found inexact Match(es).');
        buildMatchList;
      end; {211}
      202: raise exception.create('No match found.');
      403: raise exception.create('Database Entry (on server) is corrupt.');
      409: raise exception.create('No Handshake.');
    end;
  finally
    screen.cursor := crDefault;
  end; {try..finally}
end;

function TfrmRipFreeDB.getResponseCode: integer;
var s:String;
begin
  if response.Count = 0 then
    raise exception.create('No response code');
  s := response[0];
  s := stripSpaceList(s);
  result := strToInt(s);
end;

function TfrmRipFreeDB.getResponse: TStrings;
begin
  result := memResponse.lines;
end;

procedure TfrmRipFreeDB.buildMatchList;
var i:integer;
    li: TListItem;
    s, strCategory, strDiscID, strTitle: String;
begin
  i := 1;
  while (i < response.count) and (copy(response[i],1,1) <> '.') do
  begin
    s := response[i];
    strCategory := stripSpaceList(s);
    strDiscID := stripSpaceList(s);
    strTitle := s;
    li := lvMatches.items.add;
    li.caption := strCategory;
    li.subitems.add(strDiscID);
    li.subItems.add(strTitle);
    inc(i);
  end; {while}
end; {buildMatchList}
procedure TfrmRipFreeDB.lvMatchesDblClick(Sender: TObject);
var s, strCategory, strDiscID: String;
begin
  strCategory := lvMatches.Selected.Caption;
  strDiscID := lvMatches.Selected.subitems[0];
  { Get the selected discs data }
  s := 'cddb read '+strCategory+' '+strDiscID;
  issueHTTPCommand(s);
  case responseCode of
    210:
    begin
      FLog.logSuccess('Receive data...');
      processSelectedDisc;
    end; {200}
    401: raise exception.create('Disc not found.');
    402: raise exception.create('Server Error.');
    403: raise exception.create('Database Entry (on server) is corrupt.');
    409: raise exception.create('No Handshake.');
  end;
end;

procedure TfrmRipFreeDB.processSelectedDisc;
var s: String;
    i: integer;
begin
  { Build Tracks memo }
  with memTracks.lines do
  begin
    add('Disc Title: '+response.values['DTITLE']);
    i := 0;
    repeat
      s := response.values['TTITLE'+intTOStr(i)];
      if s <> '' then
        add('Track '+intTOStr(i+1)+': '+s);
      inc(i);
    until s = '';
  end; {with}
  btnOK.enabled := true;
  pcMain.activePAge := tsTracks;
end;

{
210 rock 980b230b CD database entry follows (until terminating `.')
# xmcd CD Database Entry
#
# Track frame offsets:
# 182
# 14752
# 32952
# 49062
# 68355
# 91090
# 107762
# 127017
# 145962
# 167132
# 186227
#
# Disc length: 2853 seconds
#
# Revision: 1
# Processed by: cddbd v1.4b42PL0 Copyright (c) Steve Scherf et al.
# Submitted via: tcd 2.0b
#
DISCID=980b230b
DTITLE=Megadeth / Countdown to Extinction
DYEAR=
DGENRE=
TTITLE0=Skin O' My Teeth
TTITLE1=Symphony of Destruction
TTITLE2=Architecture of Aggression
TTITLE3=Foreclosure of a Dream
TTITLE4=Sweating Bullets
TTITLE5=This was My Life
TTITLE6=Countdown to Extinction
TTITLE7=High Speed Dirt
TTITLE8=Psychotron
TTITLE9=Captive Honour
TTITLE10=Ashes in Your Mouth
EXTD=
EXTT0=
EXTT1=
EXTT2=
EXTT3=
EXTT4=
EXTT5=
EXTT6=
EXTT7=
EXTT8=
EXTT9=
EXTT10=
PLAYORDER=
.
}

procedure TfrmRipFreeDB.FormShow(Sender: TObject);
begin
  lvMatches.items.clear;
  pcMain.activePAge := tsMAtches;
  memTracks.Lines.clear;
  if AutoStart then
  begin
    btnGo.visible := false;
    timerGo.enabled := true;
  end; {if}
end;

procedure TfrmRipFreeDB.timerGoTimer(Sender: TObject);
begin
  timerGo.enabled := false;
  btnGo.click;
end;

end.
