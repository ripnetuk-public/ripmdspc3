unit URipParams;

interface
uses SysUtils, Classes;

var RipParams: TStringList;
function RipParamPresent(AParam: String):boolean;

implementation

function RipParamPresent(AParam: String):boolean;
var i:integer;
begin
  result := false;
  for i := 0 to RipParams.count-1 do
    if pos(uppercase(AParam), uppercase(RipParams[i])) = 1 then
      result := true;
end; {RipPAramPResent}

var i:integer;
initialization
  RipParams := TStringList.create;
  for i := 0 to paramCount do
    RipParams.add(paramStr(i));
finalization
  RipParams.free;
  RipParams := nil;
end.
