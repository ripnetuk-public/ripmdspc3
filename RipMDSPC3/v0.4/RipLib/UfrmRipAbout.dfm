object frmRipAbout: TfrmRipAbout
  Left = 377
  Top = 299
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'About'
  ClientHeight = 149
  ClientWidth = 221
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 118
    Width = 221
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Button1: TButton
      Left = 149
      Top = 4
      Width = 67
      Height = 24
      Anchors = [akTop, akRight]
      Caption = '&OK'
      Default = True
      TabOrder = 0
      OnClick = Button1Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 221
    Height = 118
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 1
    object Panel4: TPanel
      Left = 0
      Top = 61
      Width = 221
      Height = 57
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      OnMouseMove = Panel4MouseMove
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 55
        Height = 13
        Caption = 'Homepage:'
      end
      object Label2: TLabel
        Left = 8
        Top = 32
        Width = 32
        Height = 13
        Caption = 'E-Mail:'
      end
      object lblHomepage: TLabel
        Left = 88
        Top = 8
        Width = 83
        Height = 13
        Caption = 'www.ripnet.co.uk'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = lblHomepageClick
        OnMouseMove = lblEMailMouseMove
      end
      object lblEMail: TLabel
        Left = 88
        Top = 32
        Width = 100
        Height = 13
        Caption = 'george@ripnet.co.uk'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsUnderline]
        ParentFont = False
        OnClick = lblHomepageClick
        OnMouseMove = lblEMailMouseMove
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 221
      Height = 61
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object lblProgram: TLabel
        Left = 8
        Top = 8
        Width = 201
        Height = 25
        AutoSize = False
        Caption = 'lblProgram'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblCopyright: TLabel
        Left = 8
        Top = 40
        Width = 209
        Height = 25
        AutoSize = False
        Caption = 'lblCopyright'
      end
    end
  end
end
