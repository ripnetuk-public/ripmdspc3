unit UfrColourSelector;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls;

type
  TfrColourSelector = class(TFrame)
    Label1: TLabel;
    shapeColor: TShape;
    ColorDialog1: TColorDialog;
    btnRandom: TButton;
    procedure shapeColorMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnRandomClick(Sender: TObject);
  private
    { Private declarations }
  public
    function getColor: TColor;
  end;

implementation

{$R *.DFM}

{ TfrColourSelector }

function TfrColourSelector.getColor: TColor;
begin
  result := shapeColor.brush.color;
end;

procedure TfrColourSelector.shapeColorMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if not colorDialog1.execute then
    exit;
  shapeColor.brush.color := colorDialog1.color;
end;

procedure TfrColourSelector.btnRandomClick(Sender: TObject);
begin
  shapeColor.brush.color := random($FFFFFF); 
end;

end.
