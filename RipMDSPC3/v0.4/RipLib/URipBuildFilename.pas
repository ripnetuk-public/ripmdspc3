unit URipBuildFilename;

interface

function RipBuildFilename(strDir: String; strFilename: String):String; {Will add \ as needed}

implementation

function RipBuildFilename(strDir: String; strFilename: String):String; {Will add \ as needed}
begin
  result := strDir;
  if not (copy(result, length(result), 1) = '\') then
    result := result+'\';
  result := result + strFilename;
end; {ripBuildFIlename}

end.
