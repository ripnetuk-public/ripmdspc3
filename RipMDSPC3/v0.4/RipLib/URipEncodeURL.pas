unit URipEncodeURL;

interface

function RipEncodeURL(s: String): String;

implementation
uses sysutils, classes;

function encodeable(c: char):boolean;
begin
  result := not (c in ['a'..'z', 'A'..'Z', '0'..'9']);
end;

function RipEncodeURL(s: String): String;
var i:integer;
    c: Char;
begin
  result := '';
  for i := 1 to length(s) do
  begin
    c := s[i];
    if c = ' ' then
      result := result + '+'
    else if encodeAble(c) then
      result := result+'%'+intToHex(ord(c),2)
    else
      result := result + c;
  end; {for}
end; {RipEncodeURL}

end.
