unit UfrmRipTreeOptionsBase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, URipStrip, StdCtrls, URipIni;

type
  TfrmRipTreeOptionsBase = class(TForm)
    pnlTreeView: TPanel;
    tvOptions: TTreeView;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    btnOK: TButton;
    btnCancel: TButton;
    pnlPAgeView: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    pcPages: TPageControl;
    TabSheet1: TTabSheet;
    Label2: TLabel;
    btnApply: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure tvOptionsChange(Sender: TObject; Node: TTreeNode);
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FRootNodes: TStringList;
    FOnApply: TNotifyEvent;
    procedure buildTreeView;
    function getTreeNode(s: String): TTreeNode;
    procedure loadSettings;
    procedure saveSettings;
    procedure autoSaveControls;
    procedure autoLoadControls;
    procedure autoLoadSingleControl(c: TControl);
    procedure autoSaveSingleControl(c: TControl);
    function getFullControlName(c: TControl): STring;
  public
    procedure execute;
  protected
    procedure doSaveSettings; virtual;
    procedure doLoadSettings; virtual;
  published
    property OnApply: TNotifyEvent read FOnApply write FOnApply;
  end;

{var
  frmRipTreeOptionsBase: TfrmRipTreeOptionsBase;}

implementation

{$R *.DFM}

{ TfrmRipTreeOptionsBase }

procedure TfrmRipTreeOptionsBase.execute;
begin
  showModal;
end;

procedure TfrmRipTreeOptionsBase.buildTreeView;
var i:integer;
    ap, ts: TTabSheet;
    an, tn: TTreeNode;
begin
  { Get active page, so we can re-select it in treeview }
  ap := pcPAges.activePage;
  an := nil; {Active node}
  for i := 0 to pcPages.pageCount-1 do
  begin
    ts := pcPages.pages[i];
    ts.visible := false;
    tn := getTreeNode(ts.caption);
    tn.data := ts;
    if ap = ts then
      tn.selected := true;
  end; {for}

  for i := 0 to pcPages.pageCount-1 do
    pcPages.pages[i].tabVisible := false;
  tvOptions.fullExpand;
  
end;

function TfrmRipTreeOptionsBase.getTreeNode(s: String):TTreeNode;
  function findChildNode(AParentNode: TTreeNode; ACaption: String):integer;
  var i:integer;
  begin
    result := -1;
    for i := 0 to AParentNode.Count-1 do
      if uppercase(AParentNode.item[i].text) = uppercase(ACaption) then
      begin
        result := i;
        exit;
      end; {if}
  end; {findChildNode}
var strRootNode, strSubNode: String;
    newNode: TTreeNode;
    idx: integer;
begin
  { Create root node if it doesnt exist }
  strRootNode := stripList(s, '.');
  idx := FRootNodes.IndexOf(uppercase(strRootNode));
  if idx = -1 then
  begin
    newNode := tvOptions.items.addChild(nil, strRootNode);
    idx := FRootNodes.AddObject(uppercase(strRootNode), newNode);
  end; {if}
  { set result to this new node, in case no more }
  result := TTreeNode(FRootNodes.objects[idx]);

  { Now check each sub node }
  while s <> '' do
  begin
    strSubNode := stripList(s, '.');
    { See if newNode has any subnodes with caption strSubNode }
    idx := findChildNode(result, strSubNode);
    if idx = -1 then {Need to create it}
      result := tvOptions.items.addChild(result, strSubNode)
    else {Already there}
      result := result.Item[idx];
  end; {while}
end; {getTreeNode}

procedure TfrmRipTreeOptionsBase.FormCreate(Sender: TObject);
begin
  FRootNodes := TStringList.create;
  buildTreeView;
  loadSettings;
end;

procedure TfrmRipTreeOptionsBase.FormDestroy(Sender: TObject);
begin
  FRootNodes.free;
end;

procedure TfrmRipTreeOptionsBase.tvOptionsChange(Sender: TObject;
  Node: TTreeNode);
begin
  if Node = nil then
    exit;
  pcPages.activePage := TTabSheet(Node.data);
end;

procedure TfrmRipTreeOptionsBase.btnOKClick(Sender: TObject);
begin
  { Save new settings }
  saveSettings;
  close;
end;

procedure TfrmRipTreeOptionsBase.btnCancelClick(Sender: TObject);
begin
  { Reload old settings }
  loadSettings;
  close;
end;

procedure TfrmRipTreeOptionsBase.doLoadSettings;
begin
  autoLoadControls;
end;

procedure TfrmRipTreeOptionsBase.doSaveSettings;
begin
  autoSaveControls;
end;

procedure TfrmRipTreeOptionsBase.loadSettings;
begin
  doLoadSettings;
end;

procedure TfrmRipTreeOptionsBase.saveSettings;
begin
  doSaveSettings;
end;

procedure TfrmRipTreeOptionsBase.autoLoadControls;
  procedure loadControlAndChildren(c: TControl);
  var i:integer;
  begin
    autoLoadSingleControl(c);
    if c is TWinControl then
      with TWinControl(c) do
        for i := 0 to controlCount-1 do
          loadControlAndChildren(controls[i]);
    end; {loadControlAndChildren}
begin
  loadControlAndChildren(pcPages);
end; {autoLoadControls}

procedure TfrmRipTreeOptionsBase.autoSaveControls;
  procedure saveControlAndChildren(c: TControl);
  var i:integer;
  begin
    autoSaveSingleControl(c);
    if c is TWinControl then
      with TWinControl(c) do
        for i := 0 to controlCount-1 do
          saveControlAndChildren(controls[i]);
    end; {saveControlAndChildren}
begin
  saveControlAndChildren(pcPages);
end; {AutoSaveControls}


function TfrmRipTreeOptionsBase.getFullControlName(c: TControl):STring;
begin
  result := '';
  if c.Parent <> nil then
    if not (c.parent is TForm) then
      result := getFullControlName(c.parent);
  result := result + c.name;
end; {getFullControlName}

procedure TfrmRipTreeOptionsBase.autoLoadSingleControl(c: TControl);
var strName: String;
begin
  strName := getFullControlName(c);
  if c is TEdit then
    with TEdit(c) do
      text := RipIni.ReadString('Settings', strName, text);
  if c is TCheckbox then
    with TCheckBox(c) do
      Checked := RipIni.ReadBool('Settings', strName, checked);
  if c is TShape then
    with TShape(c) do
      Brush.color := RipIni.ReadInteger('Settings', strName, Brush.color);
end; {autoLoadSingleControl}

procedure TfrmRipTreeOptionsBase.autoSaveSingleControl(c: TControl);
var strName: String;
begin
  strName := getFullControlName(c);
  if c is TEdit then
    with TEdit(c) do
      RipIni.WriteString('Settings', strName, text);
  if c is TCheckBox then
    with TCheckBox(c) do
      RipIni.WriteBool('Settings', strName, checked);
  if c is TShape then
    with TShape(c) do
      RipIni.WriteInteger('Settings', strName, brush.color);

end; {autoSaveSingleControl}

procedure TfrmRipTreeOptionsBase.btnApplyClick(Sender: TObject);
begin
  if assigned(FOnApply) then
    FOnApply(self);
end;

procedure TfrmRipTreeOptionsBase.FormShow(Sender: TObject);
begin
  pcPages.Style := tsFlatButtons;
end;

end.
