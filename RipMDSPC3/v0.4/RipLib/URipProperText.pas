unit URipProperText;

interface
uses sysutils, classes;

function RipProperText(S: String):String;
function RipNiceText(s: String):String;
function RipPercentJunk(s: String):integer;

implementation
function RipProperText(S: String):String;
var bolUpper: boolean;
    c: String;
    i: integer;
begin;
  result := '';
  bolUpper := true;
  for i := 1 to length(s) do
  begin;
    c := copy(s, i, 1);
    if bolUpper then
      c := uppercase(c)
    else
      c := lowercase(c);
    if c = ' ' then
      bolUpper := true
    else
      bolUpper := false;
    result := result + c;
  end; {for}
end; {RipProperText}

function RipNiceText(S: String):String;
var c: String;
    i: integer;
begin;
  result := '';
  for i := 1 to length(s) do
  begin;
    c := copy(s, i, 1);
    if (ord(c[1]) < 32) or (ord(c[1]) > 126) then
      c := '';
    result := result + c;
  end; {for}
  result := trim(result);
end; {RipNiceText}

function RipPercentJunk(s: String):integer;
var c: char;
    i: integer;
    intJunk: integer;
begin;
  intJunk := 0;
  for i := 1 to length(s) do
  begin;
    c := s[i];
    if not (c in ['0'..'9', 'a'..'z', 'A'..'Z']) then
      intJunk := intJunk+1;
  end; {for}
  if length(s) = 0 then
  begin;
    result := 100;
    exit;
  end; {of}
  if length(s) = 1 then
  begin;
    result := 50;
    exit;
  end; {of}
  if length(s) = 2 then
  begin;
    result := 25;
    exit;
  end; {of}
  result := (100*intJunk) div length(s);
end; {ripPercentJunk}

end.
