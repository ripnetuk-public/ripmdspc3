unit URipDebugMode;

interface

function RipDebugMode: boolean;

implementation

function RipDebugMode: boolean;
begin
  { Mickey Mouse for now }
  result := true;
  if (paramCount <> 1) or (paramStr(1) <> '/experiment') then
    result := false;
end; {ripDebugMode}

end.
