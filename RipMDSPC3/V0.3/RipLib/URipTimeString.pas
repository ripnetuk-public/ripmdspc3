unit URipTimeString;

interface

function RipTimeString(h,m,s: integer):String; overload;
function RipTimeString(s: integer):String; overload;

implementation
uses sysUtils, classes;

function RipTimeString(h,m,s: integer):String; overload;
  function padIt(i: integer):String;
  begin
    result := intTOStr(i);
    while length(result)<2 do
      result := '0'+result;
  end; {padIt}
begin
  result := '';
  if h <> 0 then
    result := padIt(h)+':';
  result := result + padIt(m)+':'+padIt(s);
end; {buildTimeString}

function RipTimeString(s: integer):String; overload;
var intH,intM,intS: integer;
begin
  intH := s div (60*60);
  s := s mod (60*60);
  intM := s div 60;
  intS := s mod 60;
  result := RipTimeString(inth,intm,ints);
end; {buildTimeString}


end.
