unit URipUniqueFilename;

interface

function RipUniqueFilename(AExtension: String):String;

implementation
uses sysUtils, classes;

function RipUniqueFilename(AExtension: String):String;
var i:integer;
begin
  i := 0;
  repeat
    result := 'c:\temp\tmp'+intToStr(i)+'.'+AExtension;
    inc(i);
  until not fileExists(result);
end; {RipUniqueFilename}

end.
