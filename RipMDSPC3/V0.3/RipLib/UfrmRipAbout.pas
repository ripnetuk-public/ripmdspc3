unit UfrmRipAbout;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, URipVersion;

type
  TfrmRipAbout = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Panel2: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    lblHomepage: TLabel;
    lblEMail: TLabel;
    Panel3: TPanel;
    lblProgram: TLabel;
    lblCopyright: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lblEMailMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Panel4MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lblHomepageClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure executeAbout;
function getCaption: String;

var
  frmRipAbout: TfrmRipAbout;

implementation

uses UShellExec;

{$R *.DFM}

procedure TfrmRipAbout.Button1Click(Sender: TObject);
begin
  close;
end;

procedure TfrmRipAbout.FormShow(Sender: TObject);
begin
  lblProgram.caption := RIP_PROGRAM + ' '+RIP_VERSION;
  lblCopyright.caption := RIP_COPYRIGHT;
  caption := 'About '+RIP_PROGRAM
end;

procedure executeAbout;
var f:TfrmRipAbout;
begin;
  f := TfrmRipAbout.create(nil);
  f.showModal;
  f.free;
end; {executeAbout}

procedure TfrmRipAbout.lblEMailMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  with TLabel(sender) do
    font.color := clBlue;
end;

procedure TfrmRipAbout.Panel4MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
  lblHomepage.font.color := clHighlight;
  lblEMail.font.color := lblHomepage.font.color;
end;

procedure TfrmRipAbout.lblHomepageClick(Sender: TObject);
var s:String;
begin
  if Sender = lblHomepage then
    s := 'http://www.ripnet.co.uk';
  if Sender = lblEMail then
    s := 'mailto:george@ripnet.co.uk';
  runProgram(s, '', '.');
end;

function getCaption: String;
begin;
  result := RIP_PROGRAM + ' '+RIP_VERSION+' '+RIP_COPYRIGHT;
end;
end.
