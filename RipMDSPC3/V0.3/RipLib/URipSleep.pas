unit URipSleep;

interface

procedure RipSleep(timeInMs: cardinal);

implementation
uses sysutils, windows, classes;

procedure RipSleep(timeInMs: cardinal);
var tickStart: cardinal;
begin
  tickStart := getTickCount;
  repeat
    sleep(250);
  until getTickCount > tickStart+timeInMs;
end; {RipSleep}

end.
