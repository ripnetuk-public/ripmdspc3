unit URipRichEditor;

interface
uses comCtrls, sysutils, classes, wwRichEd;

function RipRichEditor(ARichEdit: TwwDBRichEdit): boolean;
procedure RipCopyRichText(Source: TwwDBRichEdit; Dest: TwwDBRichEdit);

implementation

function RipRichEditor(ARichEdit: TwwDBRichEdit): boolean;
var r: TWWDBRichEdit;
begin
  r := TWWDBRichEdit.create(ARichEdit.parent);
  r.parent := ARichEdit.parent;
  RipCopyRichText(ARichEdit, r);
  r.execute;
  RipCopyRichText(r, ARichEdit);
  r.free;
  result := true;
end; {RipRichEditor}

procedure RipCopyRichText(Source: TwwDBRichEdit; Dest: TwwDBRichEdit);
begin
  source.CopyRichEditTo(Dest);
end; {RipCopyRichText}

end.
