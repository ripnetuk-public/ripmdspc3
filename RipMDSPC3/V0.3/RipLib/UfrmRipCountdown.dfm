object frmRipCountdown: TfrmRipCountdown
  Left = 252
  Top = 240
  Width = 420
  Height = 44
  Caption = 'Countdown'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object pbProgress: TProgressBar
    Left = 0
    Top = 0
    Width = 412
    Height = 17
    Align = alClient
    Min = 0
    Max = 100
    Position = 25
    Step = 1
    TabOrder = 0
  end
end
