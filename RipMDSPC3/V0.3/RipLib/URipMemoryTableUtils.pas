unit URipMemoryTableUtils;

// save and load memory tables to a file
interface
uses memTable, sysUtils, classes, iniFiles, db;

procedure RipSaveMemoryTable(AMemoryTable: TMemoryTable; AFileName: String);
procedure RipLoadMemoryTable(AMemoryTable: TMemoryTable; AFileName: String);

implementation

procedure RipSaveMemoryTable(AMemoryTable: TMemoryTable; AFileName: String);
var ini: TIniFile;
    i: integer;
    count: integer;
    bm: TBookmark;
begin;
  if fileExists(AFileName) then
    deleteFile(AFileName);
  ini := TIniFile.create(AFileName);
  count := 0;
  with AMemoryTable do
  begin;
    bm := getBookmark;
    disableControls;
    first;
    while not eof do
    begin;
      for i := 0 to fieldCount-1 do
        ini.writeString(AMemoryTable.name+'('+intToStr(count)+')', fields[i].fieldName, fields[i].asString);
      next;
      inc(count);
    end; {while}
    ini.WriteInteger(AMemoryTable.name, 'Count', count);
    gotoBookmark(bm);
    freeBookmark(bm);
    enableControls;
  end; {with}
end; {RipSaveMemoryTable}

procedure RipLoadMemoryTable(AMemoryTable: TMemoryTable; AFileName: String);
var ini: TIniFile;
    i,j: integer;
    count: integer;
begin;
  ini := TIniFile.create(AFileName);
  count := ini.readInteger(AMemoryTable.name, 'Count', -1);
  if count=-1 then
    exit;
  with AMemoryTable do
  begin;
    disableControls;
    emptyTable;
    for j := 0 to count-1 do
    begin;
      append;
      for i := 0 to fieldCount-1 do
        fields[i].asString := ini.readString(AMemoryTable.name+'('+intToStr(j)+')', fields[i].fieldName, '');
      post;
    end; {for j}
    enableControls;
  end; {with}
end; {loadMemoryTable}

end.
