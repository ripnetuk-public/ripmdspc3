unit URipKeyIsPressed;
interface

function KeyIsPressed(AKeyCode: integer):boolean;

implementation

uses windows;

function KeyIsPressed(AKeyCode: integer):boolean;
begin;
  result := ((GetKeyState(AKeyCode) and $8000) <> 0);
end; {keyIsPressed}

end.
