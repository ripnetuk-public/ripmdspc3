unit UfrmRipSetupBase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls;

type
  TfrmRipSetupBase = class(TForm)
    pctrlSettings: TPageControl;
    Panel1: TPanel;
    btnCancel: TButton;
    btnOK: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
  private
    procedure loadControl(AControl: TControl);
    procedure saveControl(AControl: TControl);
  public
    procedure execute; virtual;
    procedure loadFromIni;
    procedure saveToIni;
  end;

{var
  frmRipSetupBase: TfrmRipSetupBase;}

implementation
uses URipIni;
{$R *.DFM}

procedure TfrmRipSetupBase.execute;
var res: TModalResult;
begin
  res := showModal;
  case res of
    mrCancel: loadFromIni;
    mrOK: saveToIni;
  end; {case}
end;


procedure TfrmRipSetupBase.loadFromIni;
  procedure doControl(AControl: TControl);
  var i:integer;
  begin;
    if AControl.tag > 0 then
      loadControl(AControl);
    if AControl is TWinControl then
      with TWinControl(AControl) do
        for i := 0 to controlCount-1 do
          doControl(controls[i]);
  end; {doCOntrol}
begin
  doControl(pctrlSettings);
end;

procedure TfrmRipSetupBase.saveToIni;
  procedure doControl(AControl: TControl);
  var i:integer;
  begin;
    if AControl.tag > 0 then
      saveControl(AControl);
    if AControl is TWinControl then
      with TWinControl(AControl) do
        for i := 0 to controlCount-1 do
          doControl(controls[i]);
  end; {doCOntrol}
begin
  doControl(pctrlSettings);
end;

function getFullControlName(AControl: TControl):string;
begin
  result := AControl.name;
  if AControl.parent is TControl then
    result := getFullControlName(TControl(AControl.parent))+result;
end; {getFullControlName}

procedure TfrmRipSetupBase.loadControl(AControl: TControl);
  function fullControlName: String;
  begin
    result := getFullControlName(AControl);
  end; {fullControlName}
begin
  if AControl is TListBox then
    with TListBox(AControl) do
    begin
      Items.commaText := RipIni.ReadString('Settings', fullControlName+'ITEMS', Items.commaText);
      ItemIndex := RipIni.ReadInteger('Settings', fullControlName+'ITEMINDEX', ItemIndex);
    end; {TListBox}
  if AControl is TMemo then
    with TMemo(AControl) do
      lines.commaText := RipIni.ReadString('Settings', fullControlName+'LINES', lines.commaText);
  if AControl is TEdit then
    with Tedit(AControl) do
      Text := RipIni.ReadString('Settings', fullControlName+'TEXT', Text);
  if AControl is TCheckBox then
    with TCheckbox(AControl) do
      Checked := RipIni.ReadBool('Settings', fullControlName+'CHECKED', checked);
end;

procedure TfrmRipSetupBase.saveControl(AControl: TControl);
  function fullControlName: String;
  begin
    result := getFullControlName(AControl);
  end; {fullControlName}
begin
  if AControl is TListBox then
    with TListBox(AControl) do
    begin
      RipIni.WriteString('Settings', fullControlName+'ITEMS', Items.commaText);
      RipIni.WriteInteger('Settings', fullControlName+'ITEMINDEX', ItemIndex);
    end; {TListBox}
  if AControl is TMemo then
    with TMemo(AControl) do
      RipIni.WriteString('Settings', fullControlName+'LINES', Lines.commaText);
  if AControl is TEdit then
    with TEdit(AControl) do
      RipIni.WriteString('Settings', fullControlName+'TEXT', Text);
  if AControl is TCheckBox then
    with TCheckBox(AControl) do
      RipIni.WriteBool('Settings', fullControlName+'CHECKED', checked);
end;

procedure TfrmRipSetupBase.FormCreate(Sender: TObject);
begin
  loadFromIni;
  pctrlSettings.ActivePageIndex := 0;
end;

procedure TfrmRipSetupBase.btnOKClick(Sender: TObject);
begin
  saveToIni;
  close;
end;

procedure TfrmRipSetupBase.btnCancelClick(Sender: TObject);
begin
  loadFromIni;
  close;
end;

end.
