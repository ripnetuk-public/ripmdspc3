unit URipBriefMessage;

interface

procedure RipBriefMessage(s: String); {'' to blank}

implementation
uses forms, sysutils, classes;

var frm: TForm;

procedure RipBriefMessage(s: String); {'' to blank}
begin
  if s = '' then
  begin
    frm.visible := false;
    exit;
  end; {if}
  frm.caption := s;
  frm.visible := true;
  application.processmessages;
end; {RipBriefMessage}

initialization
  frm := TForm.create(application);
  with frm do
  begin
    position := poScreenCenter;
    formstyle := fsStayOnTop;
  end; {with}
end.
