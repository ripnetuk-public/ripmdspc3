unit URipFormShaper;

interface
uses SysUtils, Classes, graphics, windows, forms;

type TRipFormShaper = class
  private
    FXList: TList;
    FYList: TList;
    function getPointCount: integer;
    function getPointx(idx: integer): integer;
    function getPointy(idx: integer): integer;
    procedure shapeForm(AWindow: TForm);
  public
    constructor Create;
    destructor Destroy; override;
    procedure AddPoint(x: integer; y:integer);
    procedure drawShape(ACanvas: TCanvas; AScalePercent: integer);
    property PointCount: integer read GetPointCount;
    property PointX[idx: integer]: integer read getPointX;
    property PointY[idx: integer]: integer read getPointY;
end; {TRipShapedForm}

implementation

{ TRipFormShaper }

procedure TRipFormShaper.AddPoint(x, y: integer);
begin
  FXList.add(Pointer(x));
  FYList.add(Pointer(y));
end;

constructor TRipFormShaper.Create;
begin
  inherited;
  FXList := TList.create;
  FYList := TList.create;
end;

destructor TRipFormShaper.Destroy;
begin
  FXList.free;
  FYList.free;
  inherited;
end;

procedure TRipFormShaper.drawShape(ACanvas: TCanvas;
  AScalePercent: integer);
var x:integer; y:integer;
    i: integer;
begin
  for i := 0 to PointCount-1 do
  begin
    x := pointx[i];
    y := pointy[i];
    x := (x * AScalePercent) div 100;
    y := (y * AScalePercent) div 100;
    Acanvas.Ellipse(x,y,x+4,y+4);
  end; {for}
end;

function TRipFormShaper.getPointCount: integer;
begin
  result := FXList.count;
end;

function TRipFormShaper.getPointx(idx: integer): integer;
begin
  result := integer(FXList[idx]);
end;

function TRipFormShaper.getPointy(idx: integer): integer;
begin
  result := integer(FYList[idx]);
end;

procedure TRipFormShaper.shapeForm(AWindow: TForm);
var Region: Array of TPoint;
    i: integer;
    Region1hrgn : hRgn;    
begin
  SetLength(Region,PointCount);
  for i := 0 to PointCount-1 do
  begin
    Region[i].x := pointx[i];
    Region[i].y := pointy[i];
  end; {for}
  Region1hrgn:=CreatePolygonRgn(Region[0],PointCount,2);
  SetWindowRgn(AWindow.Handle, Region1hrgn, True);
end;

end.
