unit URipPort;

(*
#define ai port_b
#define data port_c
*)

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

procedure RipOutportb(portNumber: word; data: byte);
function RipGiveIO:integer;

implementation

var GiveIODone: boolean;

procedure RipOutPortB(portNumber: word; data: byte);
begin;
  RipGiveIO;
  asm
    mov  dx,portNumber
    mov  al,Data
    out  dx,al
  end; {asm}
end; {outportb}

function RipInPortB(portNumber: word):byte;
begin;
  RipGiveIO;
  asm
    mov dx,portNumber
    in  al,dx
    mov result,al
  end; {asm}
end; {inportb}

function RipGiveIO:integer;
begin;
  result := -1;
  if GiveIODone then
    exit;
  { Check for WinNT here }
  try
    result := CreateFile('\\.\giveio', GENERIC_READ, 0, Nil, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  except
   { Well at least we tried }
  end; {Try}
(*  if result = INVALID_HANDLE_VALUE then
    raise exception.create('Failed to start GIVEIO service.');*)
  GiveIODone := true;
end; // giveIO

initialization
  GiveIODone := false;
end.
