unit UShellExec;

interface

procedure runProgram(strProgramName: String; strParams: String; strWorkingDir: String);

implementation
uses shellAPI, windows, forms;

procedure runProgram(strProgramName: String; strParams: String; strWorkingDir: String);
begin;
  shellExecute(application.handle, pchar('open'), pchar(strProgramName), pchar(strParams), pchar(strWorkingDir), SW_NORMAL);  
end; {runProgam}

end.
