unit URipBaseTask;

interface
uses forms, sysutils, classes, dialogs, stdCtrls, windows, graphics;

type TProgressEvent = procedure(Sender: TObject; intCurrent: integer; intMax: integer) of object;
type TChangeEvent = TNotifyEvent;

type TRipTaskStatus = (tsIdle, tsRunning, tsStopping, tsStopped, tsComplete, tsFailed);
type TRipBaseTask = class
  private
    FTaskManager: TForm;
    FStopped: boolean;
    FOnProgress: TProgressEvent;
    FOnChange: TChangeEvent;
    FStatus: TRipTaskStatus;
    procedure setStopped(const Value: boolean);
    procedure setStatus(const Value: TRipTaskStatus);
  protected
    procedure doRun; virtual;
    procedure doProgress(intCurrent: integer; intMax: integer);
    procedure doChange;
    function getTaskDescription: String; virtual;
    function getStatusBarText: String; virtual;
    procedure doOnCreate; virtual;
    procedure doOnDestroy; virtual;
    procedure doReset; virtual;
  public
    constructor create;
    destructor destroy; override;
    property TaskManager: TForm read FTaskMAnager write FTaskManager;
    property TaskDescription: String read getTaskDescription;
    property StatusBarText: String read getStatusBarText;
    function run:boolean;
    procedure drawInListbox(AListBox: TListBox; ARect: TRect; State: TOwnerDrawState); virtual;
    function measureForListbox: integer; virtual;
    procedure reset;
    property Stopped: boolean read FStopped write setStopped;
    property OnProgress: TProgressEvent read FOnProgress write FOnProgress;
    property OnChange: TChangeEvent read FOnChange write FOnChange;
    property Status: TRipTaskStatus read FStatus write setStatus;
end; {type}

implementation

{ TRipBaseTask }


{ TRipBaseTask }

procedure TRipBaseTask.doChange;
begin
  if assigned(onChange) then
    onChange(self);
end;

procedure TRipBaseTask.doProgress(intCurrent, intMax: integer);
begin
  if assigned(OnProgress) then
    OnProgress(self, intCurrent, intMax);
end;

procedure TRipBaseTask.doRun;
begin
  { Do nothing}
end;

function TRipBaseTask.getTaskDescription: String;
begin
  result := className;
end;


function TRipBaseTask.run:boolean;
begin
  result := true; {default to OK}
  Status := tsRunning;
  try
    doRun;
    Status := tsComplete;
    if Stopped then
      Status := tsStopped;
  except
    on e:exception do
    begin
      showMessage('Error: '+e.message);
      result := false;
      Status := tsFailed;
    end; {on}
  end; {try}
end;

procedure TRipBaseTask.setStatus(const Value: TRipTaskStatus);
begin
  FStatus := Value;
  doChange;
end;

procedure TRipBaseTask.setStopped(const Value: boolean);
begin
  FStopped := Value;
  if Value then
    if Status = tsRunning then {Only stopping if were running:}
      Status :=  tsStopping;
end;


function TRipBaseTask.measureForListbox: integer;
begin
  result := 30;
end;

procedure TRipBaseTask.drawInListbox(AListBox: TListBox; ARect: TRect; State: TOwnerDrawState);
  function getStatusColor:TColor;
  begin
    result := clBlack;
    case Status of
      tsIdle: result := clBlack;
      tsRunning: result := clBlue;
      tsStopping: result := clRed;
      tsStopped: result := clRed;
      tsComplete: result := clGreen;
      tsFailed: result := clRed;
    end; {case}
  end; {getStatusColor}
  function getStatusWingDing: String;
  begin
    case Status of
      tsIdle: result := chr(32);
      tsRunning: result := chr($f0); {Arrow}
      tsStopping: result := chr($6C); {Stop circle?}
      tsStopped: result := chr($6E); {Stop square?}
      tsComplete: result := chr($fc); {Tick}
      tsFailed: result := chr($fb); {Cross}
    end; {case}
  end; {getStatusColor}

begin
  with AListBox.canvas do
  begin
    brush.color := clBtnFace;
    
    font.name := 'WingDings';
    font.color := getStatusColor;
    font.size := 12;
    textRect(ARect, 2, ARect.top, getStatusWingDing);

    ARect.left := aRect.left + 32;

    font.size := 10;
    font.name := 'Arial';
    font.color := clBlack;
    textRect(ARect, ARect.left, Arect.top, TaskDescription);
  end; {with}
end;

procedure TRipBaseTask.doReset;
begin
  Status := tsIdle;
end;

procedure TRipBaseTask.reset;
begin
  doReset;
end;

procedure TRipBaseTask.doOnCreate;
begin
  {dummy}
end;

constructor TRipBaseTask.create;
begin
  inherited;
  doOnCreate;
end;

destructor TRipBaseTask.destroy;
begin
  doOnDestroy;
  inherited;
end;

procedure TRipBaseTask.doOnDestroy;
begin
  {dummy}
end;

function TRipBaseTask.getStatusBarText: String;
begin
  {Dummy}
end;

end.
