unit UfrmRipTaskManager;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, URipBaseTask, ComCtrls, URipStrip;

type
  TfrmRipTaskManager = class(TForm)
    lbTasks: TListBox;
    Panel1: TPanel;
    btnStop: TButton;
    ProgressBar1: TProgressBar;
    btnStart: TButton;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel5: TPanel;
    btnClose: TButton;
    Label1: TLabel;
    lblStepNo: TLabel;
    Label3: TLabel;
    lblStepCount: TLabel;
    timerAutoStart: TTimer;
    StatusBar1: TStatusBar;
    procedure btnStopClick(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure lbTasksDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure lbTasksMeasureItem(Control: TWinControl; Index: Integer;
      var Height: Integer);
    procedure lbTasksEnter(Sender: TObject);
    procedure timerAutoStartTimer(Sender: TObject);
  private
    FTaskBeingAdded: TRipBaseTask; {Bodge - because addObject calls measureItem before it sets the object :) }
    FIgnoreChange: boolean; {Flag to ignore status change - done all at once whe finished loop }
    FCurrentTaskIndex: integer;
    FStopped: boolean;
    FTaskIsRunning: boolean;
    function getTaskCount: integer;
    function getTask(idx: integer): TRipBaseTask;
    procedure setCurrentTaskIndex(const Value: integer);
    procedure setStopped(const Value: boolean);
    procedure taskProgress(Sender: TObject; intCurrent: integer; intMax: integer);
    procedure taskChange(Sender: TObject);
    procedure setTaskIsRunning(const Value: boolean);
    procedure updateStepCount;
    function getCurrentTask: TRipBaseTask;
    procedure setStatusBarText(s: String);
  public
    AutoClose: boolean;
    AutoStart: boolean;
    procedure runTasks;
    procedure addTask(ATask: TRipBaseTask);
    property taskCount: integer read getTaskCount;
    property Task[idx: integer]: TRipBaseTask read getTask;
    property CurrentTaskIndex: integer read FCurrentTaskIndex write setCurrentTaskIndex;
    function runCurrentTask: boolean;
    property Stopped: boolean read FStopped write setStopped;
    property TaskIsRunning: boolean read FTaskIsRunning write setTaskIsRunning;
    property CurrentTask: TRipBaseTask read getCurrentTask;
    procedure addDelay(AperiodMS: integer; ACaption: String);
    procedure resetTasks;
    procedure coverControl(AControl: TControl);
  end;

{var
  frmRipTaskManager: TfrmRipTaskManager;}

implementation

uses URTDelay;

{$R *.DFM}

{ TfrmRipTaskManager }

procedure TfrmRipTaskManager.addTask(ATask: TRipBaseTask);
begin
  FTaskBeingAdded := ATask;
  lbTasks.items.addObject(ATask.taskDescription, ATask);
  FTaskBeingAdded := nil;
  ATask.TaskManager := self;
  ATask.onProgress := TaskProgress;
  ATask.onChange := TaskChange;
  updateStepCount;  
end;

procedure TfrmRipTaskManager.runTasks;
var bolFail: boolean;
begin
  bolFail := false;
  if taskCount = 0 then
    raise exception.create('No tasks');
  CurrentTaskIndex := 0;
  Stopped := false;
  while (CurrentTaskIndex < TaskCount) and (not Stopped) and (not bolFail) do
  begin
    TaskIsRunning := true;
    if not runCurrentTask then
      bolFail := true;
    TaskIsRunning := false;
    if not Stopped then
      CurrentTaskIndex := CurrentTaskIndex + 1;
  end; {while}
  if AutoClose then
    if not bolFail then
    begin
      close;
      exit;
    end; {if}
  if Stopped then
    showMessage('Stopped.')
  else if bolFail then
    showMEssage('Failed')
  else
    showMessage('Finished.');
end;

function TfrmRipTaskManager.getTask(idx: integer): TRipBaseTask;
begin
  result := TRipBaseTask(lbTasks.items.objects[idx]);
end;

function TfrmRipTaskManager.getTaskCount: integer;
begin
  result := lbTasks.items.count;
end;

function TfrmRipTaskManager.runCurrentTask:boolean;
var t: TRipBaseTask;
begin
  updateStepCount;
  t := CurrentTask;
  result := t.run;
end;

procedure TfrmRipTaskManager.setCurrentTaskIndex(const Value: integer);
begin
  FCurrentTaskIndex := Value;
  lbTasks.ItemIndex := Value;
end;

procedure TfrmRipTaskManager.btnStopClick(Sender: TObject);
begin
  Stopped := true;
end;

procedure TfrmRipTaskManager.setStopped(const Value: boolean);
var i:integer;
begin
  FStopped := Value;
  FIgnoreChange := true;
  try
    { Tell each task the stopped status }
    for i := 0 to taskCount-1 do
      task[i].Stopped := Value;
  finally
    FIgnoreChange := false;
  end; {try}
  { Gray out stop button to prevent repeated press }
  btnStop.enabled := not value;
  application.processMessages;
end;

procedure TfrmRipTaskManager.taskProgress(Sender: TObject; intCurrent, intMax: integer);
begin
  progressBar1.max := intMax;
  progressBar1.position := intCurrent;

  if currentTask <> nil then
    setStatusBarText(currentTask.statusBarText);

  application.processMessages;
end;

procedure TfrmRipTaskManager.setStatusBarText(s: String);
var idx: integer;
begin
  idx := 0;
  while s <> '' do
  begin
    statusBar1.panels[idx].text := stripSemiList(s);
    inc(idx);
  end; {while}
end; {setStatusBarText}

procedure TfrmRipTaskManager.btnStartClick(Sender: TObject);
begin
  resetTasks;
  runTasks;
end;

procedure TfrmRipTaskManager.FormCreate(Sender: TObject);
begin
  lbTasks.items.clear;
  Stopped := false;
  TaskIsRunning := false;
end;

procedure TfrmRipTaskManager.setTaskIsRunning(const Value: boolean);
begin
  FTaskIsRunning := Value;
  btnStart.enabled := not Value;
  btnStop.enabled := value;
end;

procedure TfrmRipTaskManager.addDelay(AperiodMS: integer;
  ACaption: String);
var t: TRTDelay;
begin
  t := TRTDelay.create;
  t.periodMS := APeriodMS;
  t.caption := ACaption;
  addTask(t);
end;

procedure TfrmRipTaskManager.btnCloseClick(Sender: TObject);
begin
  if btnStop.enabled = true then
  begin
    AutoClose := true;
    btnStop.click;
  end
  else
    close; {not running - just close}
end;

procedure TfrmRipTaskManager.lbTasksDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var aTask: TRipBaseTask;
begin
  aTask := TRipBaseTask(lbTasks.items.objects[Index]);
  if aTask = nil then
    exit;
  aTask.DrawInListbox(TListBox(Control), Rect, State);
end;

procedure TfrmRipTaskManager.lbTasksMeasureItem(Control: TWinControl; Index: Integer; var Height: Integer);
var aTask: TRipBaseTask;
begin
  { First see if we can get a task - first time round need to use bodge }
  aTask := TRipBaseTask(lbTasks.items.objects[Index]);
  if aTask <> nil then
  begin
    height := aTask.measureForListBox;
    exit;
  end; {if}

  if FTaskBeingAdded = nil then
    exit; {Nothing being added yer}
    
  height := FTaskBeingAdded.measureForListBox;
end;

procedure TfrmRipTaskManager.taskChange(Sender: TObject);
begin
  if FIgnoreChange then
    exit;
  lbTasks.invalidate;
  application.processmessages;
end;

procedure TfrmRipTaskManager.resetTasks;
var i: integer;
begin
  FIgnoreChange := true;
  try
    for i := 0 to taskCount-1 do
      task[i].reset;
  finally
    FIgnoreChange := false;
  end; {try..finally}
  updateStepCount;
end;

procedure TfrmRipTaskManager.lbTasksEnter(Sender: TObject);
begin
  panel2.setfocus;
end;

procedure TfrmRipTaskManager.updateStepCount;
begin
  lblStepNo.Caption := intToSTr(FCurrentTaskIndex+1);
  lblStepCount.caption := intTOStr(TaskCount);
  application.processmessages;
end;

procedure TfrmRipTaskManager.coverControl(AControl: TControl);
begin
  with AControl do
    self.setBounds(clientOrigin.x, clientOrigin.y, clientWidth, clientHeight);
end;

procedure TfrmRipTaskManager.timerAutoStartTimer(Sender: TObject);
begin
  timerAutoStart.enabled := false;
  if AutoStart then
    btnStart.click;  
end;

function TfrmRipTaskManager.getCurrentTask: TRipBaseTask;
begin
  result := nil;
  if not TaskIsRunning then
    exit;
  result := task[CurrentTaskIndex];
end;

end.
