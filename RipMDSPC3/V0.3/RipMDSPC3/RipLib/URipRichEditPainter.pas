unit URipRichEditPainter;

interface
uses Graphics, windows, sysutils, classes, controls,
     forms, extCtrls, comCtrls, richEdit, wwRichEd;

type TRipRichEditPainter = class
  public
    Canvas: TCanvas;
    RichEdit: TwwDBRichEdit;
    RenderArea: TRect;
    BorderX: integer;
    BorderY: integer;
    procedure paint;
    procedure paintSimple;
end; {TRipRichEditPainter}

implementation


(*
const FIXED_X_SCALE_FRONT = 25;
const FIXED_Y_SCALE_FRONT = 25;
const FIXED_X_SCALE_BACK = 1;
const FIXED_Y_SCALE_BACK = 1;

const MARGIN = 32;

type TCoverDrawType = (cdFront, cdBack);

type TCoverDraw = class
  private
    function scaleX(x: double): integer;
    function scaleY(y: double): integer;
    function scaleXM(x: double): integer;
    function scaleYM(y: double): integer;
    procedure moveTo(x,y: double);
    procedure lineTo(x,y: double);
    procedure drawFront;
    procedure drawBack;
    procedure fiTwwDBRichEditInBox(ARichEdit: TwwDBRichEdit; x0, y0, x1,      y1: double);
    function canvas: TCanvas;
    function richEditByName(ANAme: String):TwwDBRichEdit;
  public
    RichEditList: TList;
    DrawType: TCoverDrawType;
    Scale: double;
    Paintbox: TPaintbox;
    procedure draw;
    constructor create;
    destructor destroy; override;
end; {TCoverDraw}

implementation
uses UfrmDesignCover, URipCopyOLEContainer;

{ TCoverDraw }

///////////////////////////////////////////////////////////////////////////
// SCALING CODE

function TCoverDraw.scaleX(x: double):integer;
var fixedScale: double;
begin
  fixedScale := 1;
  case drawType of
    cdFront: fixedScale := FIXED_X_SCALE_FRONT;
    cdBack: fixedScale := FIXED_X_SCALE_BACK;
  end; {case}
  result := round(x*Scale*fixedScale);
end; {scaleX}

function TCoverDraw.scaleY(y: double):integer;
var fixedScale: double;
begin
  fixedScale := 1;
  case drawType of
    cdFront: fixedScale := FIXED_Y_SCALE_FRONT;
    cdBack: fixedScale := FIXED_Y_SCALE_BACK;
  end; {case}
  result := round(y*Scale*fixedScale);
end; {scaleY}

///////////////////////////////////////////////////////////////////////////
// DRAWING METHODS USING SCALING

procedure TCoverDraw.lineTo(x, y: double);
begin
  canvas.lineTo(scaleXM(x), scaleYM(y));
end;

procedure TCoverDraw.moveTo(x, y: double);
begin
  canvas.moveTo(scaleXM(x), scaleYM(y));
end;


///////////////////////////////////////////////////////////////////////////
// MAIN DRAWING ENTRY POINT
procedure TCoverDraw.draw;
var i:integer;
begin
  { Clear background to white}
  with canvas do
  begin
    brush.color := clWhite;
    FillRect(clipRect);
    case drawType of
      cdFront: drawFront;
      cdBack: drawBack;
    end; {case}
    pen.color := clRed;
    i := 0;
    while (i < PaintBox.width) or (i < paintBox.height) do
    begin
      moveTo(0,i);
      lineTo(paintbox.width, i);
      moveTo(i,0);
      lineTo(i, paintbox.height);
      inc(i,100);
    end; {whilt}
  end; {with}
end;

///////////////////////////////////////////////////////////////////////////
// DRAW BACK

procedure TCoverDraw.drawBack;
begin
  moveTo(0,0);
  lineTo(100,100);
end;

///////////////////////////////////////////////////////////////////////////
// DRAW FRONT

procedure TCoverDraw.drawFront;
var intPBWidth,intPBHeight: integer;
begin
  canvas.pen.color := clBlack;
  { Set size of paintbox so parent scrollbox behaves correctly}
  intPBWidth  := scalex(1)+2*MARGIN;
  intPBHeight := scaley(2)+2*MARGIN;
  if intPBWidth < paintbox.parent.ClientWidth then
    intPBWidth := paintbox.parent.ClientWidth;
  if intPBheight < paintbox.parent.Clientheight then
    intPBHeight := paintbox.parent.Clientheight;

  paintbox.width := intPBWidth;
  paintbox.height := intPBHeight;

  fiTwwDBRichEditInBox(richEditByName('FrontInside'), 0, 0, 1, 1);
  moveTo(0,0);
  lineTo(1,0);
  lineTo(1,2);
  lineTo(0,2);
  lineTo(0,0);
  moveTo(0,1);
  lineTo(1,1);
end;

(*procedure TCoverDraw.fiTwwDBRichEditInBox(ARichEdit: TwwDBRichEdit; x0, y0, x1,  y1: double);
begin
{  if AObject.state = osEMpty then
    exit;
  oleDraw := TOLEContainer.create(AObject.owner);
  with oleDraw do
  begin
    Parent := AObject.parent;
    SizeMode := smScale;
    borderStyle := bsNone;
    width := scaleX(x1-x0);
    height := scaleY(y1-y0);
    copyOLEContainer(AObject, OLEDraw);
    PaintTo(Canvas.handle, scaleXM(x0), scaleYM(y0));
    free;
  end; {with OLEDraw}
end;

* )

function TCoverDraw.canvas: TCanvas;
begin
  result := paintbox.canvas;
end;

function TCoverDraw.scaleXM(x: double): integer;
begin
  result := scaleX(x)+MARGIN;
end;

function TCoverDraw.scaleYM(y: double): integer;
begin
  result := scaleY(y)+MARGIN;
end;

function TCoverDraw.richEditByName(ANAme: String): TwwDBRichEdit;
var i:integer;
begin
  result := nil;
  for i := 0 to RichEditList.count-1 do
    if uppercase(TwwDBRichEdit(RichEditList[i]).name) = uppercase(AName) then
      result := TwwDBRichEdit(RichEditList[i]);

  if result = nil then
    raise exception.create(classname+': Rich Edit "'+ANAme+'" not in list.');
end;

constructor TCoverDraw.create;
begin
  RichEditList := TList.create;
end;

destructor TCoverDraw.destroy;
begin
  richEditList.free;
  inherited;
end;


(*procedure TCoverDraw.fiTwwDBRichEditInBox(ARichEdit: TwwDBRichEdit; x0, y0, x1,  y1: double);
var fmtRange: FormatRange;
begin
  fmtRange.hdc := Canvas.handle;
  fmtRange.hdcTarget := Canvas.handle;
  fmtRange.rc := RECT(scaleXM(x0), scaleYM(y0), scaleXM(x1), scaleYM(y1));
  fmtRange.rcPage := ARichEdit.pageRect;
  fmtRange.chrg.cpMin := 0;
  fmtRange.chrg.cpMax := ARichEdit.getTextLen-1;

  { Send this message to the Rich Edit }
  ARichEdit.perform( EM_FORMATRANGE, 1, LongInt(@fmtRange));
  { Print it }
  ARichEdit.perform( EM_DISPLAYBAND, 0, LongInt(@fmtRange.rc));
  { Free cached info }
  ARichEdit.Perform(EM_FORMATRANGE, 0, 0);
end;* )



procedure TCoverDraw.fiTwwDBRichEditInBox(ARichEdit: TwwDBRichEdit; x0, y0, x1,  y1: double);
var r: TRect;
    richedit_outputarea: TRect;
    printresX, printresY: Integer;
    fmtRange: TFormatRange;
    intPageWidth, intPageHeight: integer;
    intWindowExtentX, intWindowExtentY: integer;
    intViewportExtentX, intViewportExtentY: integer;
    intOldMapMode: integer;
begin
  intPageWidth := 10000;
  intPageHeight := 10000;

  { Store old map mode }
  intOldMapMode := GetMapMode(canvas.handle);
  { Set r to be the output area }
  r.Left := scaleXM(x0);
  r.top := scaleYM(y0);
  r.right := scaleXM(x1);
  r.bottom := scaleYM(y1);
//  r:= Rect( 1000 div 13, 1000 div 13, Round((intPageWidth) / 1.3), Round((intPageheight) / 1.3));
  { Set to arbitary scaling mode }
  SetMapMode( canvas.handle, MM_ANISOTROPIC );
  { Calculate and set window extent }
  intWindowExtentX := GetDeviceCaps(canvas.handle, LOGPIXELSX);
  intWindowExtentY := GetDeviceCaps(canvas.handle, LOGPIXELSY);
  SetWindowExtEx(canvas.handle, intWindowExtentX, intWindowExtentY, Nil);
  { Calculate and set viewport extent }
  intViewportExtentX := Round(GetDeviceCaps(canvas.handle, LOGPIXELSX));
  intViewportExtentY := Round(GetDeviceCaps(canvas.handle, LOGPIXELSY));
  SetViewportExtEx(canvas.handle, intViewportExtentX, intViewportExtentY, Nil);
  with canvas do
  begin
    printresX := Round(GetDeviceCaps( handle, LOGPIXELSX){/ 1.3} );
    printresY := Round(GetDeviceCaps( handle, LOGPIXELSY){/ 1.3} );

    // Define a rectangle for the rich edit text. The height is set to the
    // maximum. But we need to convert from device units to twips,       // 1 twip = 1/1440 inch or 1/20 point.
    with richedit_outputarea do
    begin
      left := r.left * 1440 div printresX;
      top := r.top * 1440 div printresY;
      right := r.right * 1440 div printresX;
      bottom := r.bottom* 1440 div printresY
    end; {with};

    // Tell rich edit to format its text to the printer. First set       // up data record for message:
    fmtRange.hDC := Handle;            // printer handle
    fmtRange.hdcTarget := Handle;    // ditto
    fmtRange.rc := richedit_outputarea;
    fmtRange.rcPage := Rect( 0, 0, intPageWidth * 1440 div printresX, intPageHeight * 1440 div printresY );
//    fmtRange.rcPage := Rect( 0, 0, intPageWidth * 1440 div printresX, intPageHeight * 1440 div printresY );
{    with fmtRange.rcPage do
    begin
      left := scaleXM(0) * 1440 div printresX;
      top := scaleYM(y0) * 1440 div printresX;
      right := scaleXM(x1) * 1440 div printresX;
      bottom := scaleYM(y1) * 1440 div printresX;
    end; {with}


    { Range of text to print }
    fmtRange.chrg.cpMin := 0;
    fmtRange.chrg.cpMax := Arichedit.GetTextLen-1;

    // format the text
    ARichEdit.Perform( EM_FORMATRANGE, 1, Longint(@fmtRange));       // and print it
    ARichEdit.Perform( EM_DISPLAYBAND, 0, Longint( @fmtRange.rc ));

    // Free cached information
    ARichEdit.Perform( EM_FORMATRANGE, 0, 0);
  End; {with canvas}
  SetMapMode( canvas.handle, intOldMapMode );
end;



(*procedure TCoverDraw.fiTwwDBRichEditInBox(ARichEdit: TwwDBRichEdit; x0, y0, x1,  y1: double);
var
    richedit_outputarea: TRect;
    fmtRange: TFormatRange;
    intWindowExtentX, intWindowExtentY: integer;
    intViewportExtentX, intViewportExtentY: integer;
    intOldMapMode: integer;
begin
  intOldMapMode := GetMapMode(canvas.handle);
  { Set to arbitary scaling mode }
  SetMapMode( canvas.handle, MM_ANISOTROPIC );
  { Calculate and set window extent }
  intWindowExtentX := 1000;
  intWindowExtentY := 1000;
  SetWindowExtEx(canvas.handle, intWindowExtentX, intWindowExtentY, Nil);
  { Calculate and set viewport extent }
  intViewportExtentX := scaleXM(2);
  intViewportExtentY := scaleYM(2);
  SetViewportExtEx(canvas.handle, intViewportExtentX, intViewportExtentY, Nil);
  with richedit_outputarea do
  begin
    left := 0;
    top := 0;
    right := 10000;
    bottom := 10000;
  end; {with};

  // Tell rich edit to format its text to the printer. First set       // up data record for message:
  fmtRange.hDC := Canvas.Handle;            // printer handle
  fmtRange.hdcTarget := Canvas.Handle;    // ditto
  fmtRange.rc := richedit_outputarea;
  fmtRange.rcPage := Rect( 0, 0, 1000, 1000);

  { Range of text to print }
  fmtRange.chrg.cpMin := 0;
  fmtRange.chrg.cpMax := Arichedit.GetTextLen-1;

  // format the text
  ARichEdit.Perform( EM_FORMATRANGE, 1, Longint(@fmtRange));       // and print it
  ARichEdit.Perform( EM_DISPLAYBAND, 0, Longint( @fmtRange.rc ));

  // Free cached information
  ARichEdit.Perform( EM_FORMATRANGE, 0, 0);

  { Return map mode to what it was before }
  SetMapMode( canvas.handle, intOldMapMode );
end;*)


{ TRipRichEditPainter }

procedure TRipRichEditPainter.paintSimple;
var r: TRect;
    i: integer;
    x,y: integer;
    s: String;
    intTextHeight: integer;
begin
  with renderArea do
  begin
    r.Left := left + borderX;
    r.top := top - borderY;
    r.right := right -borderX;
    r.bottom := bottom + borderY;
  end;{with}
  { Print text in renderarea }
  y := r.top;
  x := r.left;
  for i := 0 to richEdit.lines.count-1 do
  begin
    intTextHeight := canvas.TextHeight(s);
    s := richEdit.lines[i];
    canvas.TextRect(r, x, y, s);
    y := y - intTextHeight;
  end; {for}
end;


procedure TRipRichEditPainter.paint;
var fmtRange: FormatRange;
    logX, logY: integer;
    r: TRect;
begin
  if canvas = nil then
    raise exception.create('No canvas.');

  logX := GetDeviceCaps(canvas.handle, LOGPIXELSX);
  logY := GetDeviceCaps(canvas.handle, LOGPIXELSY);

  fmtRange.hdc := Canvas.handle;
  fmtRange.hdcTarget := Canvas.handle;

  r.left := ((RenderArea.left+borderX)*(1440)) div logX;
  r.right := ((RenderArea.right-borderX)*(1440)) div logX;
  r.top := ((RenderArea.top+borderY)*(1440)) div logY;
  r.bottom := ((RenderArea.bottom-borderY)*(1440)) div logY;

  fmtRange.rc := r;
  fmtRange.rcPage := r;
  fmtRange.chrg.cpMin := 0;
  fmtRange.chrg.cpMax := RichEdit.getTextLen;

  { Send this message to the Rich Edit }
  RichEdit.perform( EM_FORMATRANGE, 1, LongInt(@fmtRange));
  { Print it }
  RichEdit.perform( EM_DISPLAYBAND, 0, LongInt(@fmtRange.rc));
  { Free cached info }
  RichEdit.Perform(EM_FORMATRANGE, 0, 0);
end;


end.
