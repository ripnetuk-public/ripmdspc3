unit URipRectUtils;

interface
uses windows;

function RipScaleRect(ARect: TRect; AScale: double):TRect;

implementation

function RipScaleRect(ARect: TRect; AScale: double):TRect;
begin
  with result do
  begin
    left := round(ARect.left * AScale);
    top := round(ARect.top * AScale);
    right := round(ARect.right * AScale);
    bottom := round(Arect.bottom * AScale);
  end; {with}
end; {RipScaleRect}
end.
