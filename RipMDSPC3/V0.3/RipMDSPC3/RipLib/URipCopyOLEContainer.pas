unit URipCopyOLEContainer;

interface
uses olectnrs, classes;
procedure copyOLEContainer(Source: TOLEContainer; Dest: TOLEContainer);

implementation

procedure copyOLEContainer(Source: TOLEContainer; Dest: TOLEContainer);
var msTemp: TMemoryStream;
begin
  msTemp := TMemoryStream.create;
  Source.SaveToStream(msTemp);
  msTemp.Seek(0, soFromBeginning);
  Dest.loadFromStream(msTemp);
  msTemp.free;
end;

end.
