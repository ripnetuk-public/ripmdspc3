object frTrackList: TfrTrackList
  Left = 0
  Top = 0
  Width = 646
  Height = 400
  Color = clRed
  ParentColor = False
  TabOrder = 0
  OnResize = FrameResize
  object dbGrid: TDBCtrlGrid
    Left = 0
    Top = 29
    Width = 646
    Height = 347
    Align = alClient
    ColCount = 1
    Color = clBtnFace
    DataSource = dsTracks
    PanelBorder = gbNone
    PanelHeight = 17
    PanelWidth = 630
    ParentColor = False
    PopupMenu = pmTidy
    TabOrder = 0
    RowCount = 20
    SelectedColor = clYellow
    object Shape1: TShape
      Left = 0
      Top = 16
      Width = 630
      Height = 1
      Align = alBottom
    end
    object pnlOnCtrlGrid: TPanel
      Left = 0
      Top = 0
      Width = 630
      Height = 16
      Align = alClient
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object dbeTrackNo: TDBText
        Left = 8
        Top = 1
        Width = 33
        Height = 18
        Alignment = taRightJustify
        DataField = 'TrackNo'
        DataSource = dsTracks
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbeLengthString: TDBText
        Left = 544
        Top = 1
        Width = 57
        Height = 18
        Alignment = taRightJustify
        DataField = 'CalcLengthString'
        DataSource = dsTracks
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsItalic]
        ParentFont = False
      end
      object dbeTrackName: TDBEdit
        Left = 48
        Top = 1
        Width = 481
        Height = 18
        AutoSize = False
        BorderStyle = bsNone
        Ctl3D = False
        DataField = 'TrackName'
        DataSource = dsTracks
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = True
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object edtHint: TEdit
    Left = 144
    Top = 88
    Width = 257
    Height = 19
    BorderStyle = bsNone
    Color = clBtnFace
    Ctl3D = False
    ParentCtl3D = False
    ReadOnly = True
    TabOrder = 1
    Text = 'Drag Files Here To Record...'
  end
  object Panel1: TPanel
    Left = 0
    Top = 376
    Width = 646
    Height = 24
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object lblTotal: TLabel
      Left = 560
      Top = 8
      Width = 97
      Height = 13
      AutoSize = False
      Caption = '00:00:00'
    end
    object Label1: TLabel
      Left = 488
      Top = 8
      Width = 53
      Height = 13
      Caption = 'Total Time:'
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 646
    Height = 29
    Align = alTop
    TabOrder = 3
    object Label3: TLabel
      Left = 8
      Top = 8
      Width = 63
      Height = 13
      Caption = 'Album Name:'
    end
    object edtAlbumName: TEdit
      Left = 80
      Top = 4
      Width = 545
      Height = 21
      TabOrder = 0
    end
  end
  object dsTracks: TDataSource
    DataSet = tblTracks
    Left = 48
    Top = 48
  end
  object tblTracks: TRxMemoryData
    FieldDefs = <
      item
        Name = 'TrackNo'
        DataType = ftInteger
      end
      item
        Name = 'TrackName'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'LengthHours'
        DataType = ftInteger
      end
      item
        Name = 'LengthMins'
        DataType = ftInteger
      end
      item
        Name = 'LengthSecs'
        DataType = ftInteger
      end
      item
        Name = 'SourceFilename'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'MP3Artist'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'MP3Album'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'MP3Title'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'MP3Track'
        DataType = ftString
        Size = 255
      end
      item
        Name = 'MP3Bitrate'
        DataType = ftString
        Size = 255
      end>
    AfterPost = tblTracksAfterPost
    OnCalcFields = tblTracksCalcFields
    OnNewRecord = tblTracksNewRecord
    Left = 32
    Top = 40
    object tblTracksTrackNo: TIntegerField
      DisplayWidth = 10
      FieldName = 'TrackNo'
    end
    object MemoryTable1TrackName: TStringField
      DisplayWidth = 50
      FieldName = 'TrackName'
      Size = 255
    end
    object tblTracksLengthHours: TIntegerField
      FieldName = 'LengthHours'
    end
    object MemoryTable1LengthMins: TIntegerField
      DisplayWidth = 10
      FieldName = 'LengthMins'
    end
    object MemoryTable1LengthSecs: TIntegerField
      DisplayWidth = 10
      FieldName = 'LengthSecs'
    end
    object tblTracksSourceFilename: TStringField
      FieldName = 'SourceFilename'
      Size = 255
    end
    object tblTracksMP3Artist: TStringField
      FieldName = 'MP3Artist'
      Size = 255
    end
    object tblTracksMP3Album: TStringField
      FieldName = 'MP3Album'
      Size = 255
    end
    object tblTracksMP3Title: TStringField
      FieldName = 'MP3Title'
      Size = 255
    end
    object tblTracksMP3Track: TStringField
      FieldName = 'MP3Track'
      Size = 255
    end
    object tblTracksMP3Bitrate: TStringField
      FieldName = 'MP3Bitrate'
      Size = 255
    end
    object tblTracksCalcLengthString: TStringField
      FieldKind = fkCalculated
      FieldName = 'CalcLengthString'
      Size = 50
      Calculated = True
    end
  end
  object pmTidy: TPopupMenu
    Left = 312
    Top = 200
    object RemoveJunkFromTitle1: TMenuItem
      Caption = 'Remove Junk From Title'
      OnClick = RemoveJunkFromTitle1Click
    end
  end
end
