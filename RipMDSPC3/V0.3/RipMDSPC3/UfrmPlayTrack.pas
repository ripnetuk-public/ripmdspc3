unit UfrmPlayTrack;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MPlayer, ExtCtrls, StdCtrls, UfrmRipCountdown;

type
  TfrmPlayTrack = class(TForm)
    mpPlay: TMediaPlayer;
    Timer1: TTimer;
    Timer2: TTimer;
    lblTime: TLabel;
    lblFilename: TLabel;
    procedure Timer1Timer(Sender: TObject);
    procedure mpPlayNotify(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
  private
    { Private declarations }
  public
    Filename: String;
    procedure play;
    procedure prepare;
  end;

var
  frmPlayTrack: TfrmPlayTrack;

implementation

{$R *.DFM}

{ TfrmPlayTrack }

procedure TfrmPlayTrack.Timer1Timer(Sender: TObject);
begin
  close;
end;

procedure TfrmPlayTrack.mpPlayNotify(Sender: TObject);
begin
  if mpPlay.Mode = mpPlaying then
    timer1.enabled := true;
end;

procedure TfrmPlayTrack.Timer2Timer(Sender: TObject);
var timeSecs: longint;
    s: String;
begin
  timeSecs  := mpPlay.position div 1000;
  s := intToStr(timeSecs div 60)+':'+IntToStr(timeSecs mod 60); { Display Seconds in Label3 }
  lblTime.caption := s;
end;

var PreparedBefore: boolean;

procedure TfrmPlayTrack.prepare;
begin
  mpPlay.FileName := Filename;
  lblFilename.caption := extractFilename(Filename);
  mpPlay.Wait := true;
  mpPlay.open;
  if not PreparedBefore then
  begin
    mpPlay.play;
    ripCountdown(3, 'Practice Run...');
    mpPlay.stop;
    mpPlay.Rewind;
    PreparedBefore := true;
  end; {if}
  mpPlay.Wait := false;
end;

procedure TfrmPlayTrack.play;
begin
  mpPlay.play;
  showModal;
end;

initialization
  PReparedBefore := false;
end.
