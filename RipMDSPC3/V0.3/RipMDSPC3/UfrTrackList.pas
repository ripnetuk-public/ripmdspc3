unit UfrTrackList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, Grids, Wwdbigrd, Wwdbgrid, DBCGrids, StdCtrls,
  Mask, DBCtrls, ExtCtrls, RxMemDS, Menus;

type
  TfrTrackList = class(TFrame)
    dsTracks: TDataSource;
    tblTracks: TRxMemoryData;
    MemoryTable1TrackName: TStringField;
    MemoryTable1LengthMins: TIntegerField;
    MemoryTable1LengthSecs: TIntegerField;
    tblTracksTrackNo: TIntegerField;
    dbGrid: TDBCtrlGrid;
    Shape1: TShape;
    pnlOnCtrlGrid: TPanel;
    dbeTrackNo: TDBText;
    dbeTrackName: TDBEdit;
    dbeLengthString: TDBText;
    tblTracksSourceFilename: TStringField;
    tblTracksMP3Artist: TStringField;
    tblTracksMP3Album: TStringField;
    tblTracksMP3Title: TStringField;
    tblTracksMP3Track: TStringField;
    tblTracksMP3Bitrate: TStringField;
    edtHint: TEdit;
    Panel1: TPanel;
    lblTotal: TLabel;
    Label1: TLabel;
    Panel2: TPanel;
    edtAlbumName: TEdit;
    Label3: TLabel;
    tblTracksLengthHours: TIntegerField;
    tblTracksCalcLengthString: TStringField;
    pmTidy: TPopupMenu;
    RemoveJunkFromTitle1: TMenuItem;
    procedure FrameResize(Sender: TObject);
    procedure tblTracksNewRecord(DataSet: TDataSet);
    procedure tblTracksAfterPost(DataSet: TDataSet);
    procedure tblTracksCalcFields(DataSet: TDataSet);
    procedure RemoveJunkFromTitle1Click(Sender: TObject);
  private
    FBookmark: TBookmark;
    procedure doTotal;
  protected
    procedure loaded; override;
  public
    procedure emptyTable;
    procedure beginWork;
    procedure endWork;
  end;

implementation
uses URipTimeString;

{$R *.DFM}

procedure TfrTrackList.FrameResize(Sender: TObject);
begin
  dbGrid.RowCount := clientHeight div 20;
end;

procedure TfrTrackList.tblTracksNewRecord(DataSet: TDataSet);
begin
  edtHint.visible := false;
end;

procedure TfrTrackList.doTotal;
var bm: TBookMark;
    totalSecs: integer;
begin
  totalSecs := 0;
  with tblTracks do
  begin
    disableControls;
    bm := getBookmark;
    first;
    while not eof do
    begin
      totalSecs := totalSecs + fieldByNAme('LengthMins').asInteger*60+fieldByName('LengthSecs').asInteger;
      next;
    end; {while}
    gotoBookmark(bm);
    freeBookmark(bm);
    enableControls;
  end; {with}
  lblTotal.Caption := RipTimeString(totalSecs);
end; {doTotal}

procedure TfrTrackList.tblTracksAfterPost(DataSet: TDataSet);
begin
  doTotal;
end;

procedure TfrTrackList.emptyTable;
begin
  tblTracks.emptyTable;
  doTotal;
end;

procedure TfrTrackList.tblTracksCalcFields(DataSet: TDataSet);
begin
  with dataSet do
    fieldByName('CalcLengthString').asString := RipTimeString(fieldByNAme('LengthHours').asInteger, fieldByName('LengthMins').asINteger, fieldByNAme('LengthSecs').asINteger);
end;

procedure TfrTrackList.loaded;
begin
  inherited;
  tblTracks.active := true;
end;

procedure TfrTrackList.beginWork;
begin
  tblTracks.disableControls;
  FBookmark := tblTracks.getBookmark;
  screen.cursor := crHourglass;
end;

procedure TfrTrackList.endWork;
begin
  tblTracks.gotoBookmark(FBookmark);
  tblTRacks.enableControls;
  tblTracks.freeBookmark(FBookmark);
  screen.cursor := crDefault;
end;

procedure TfrTrackList.RemoveJunkFromTitle1Click(Sender: TObject);
  function isJunk(c: char):boolean;
  begin
    result := c in ['0'..'9', '-', ' '];
  end; {isJunk}
  function removeJunk(s: String):String;
  begin
    result := s;
    while (length(result) > 0) and (isJunk(result[1])) do
      result := copy(result, 2, length(result));
  end; {removeJunk}
begin
  beginWork;
  try
    with tblTracks do
    begin
      first;
      while not eof do
      begin
        edit;
          with fieldByNAme('TrackName') do
            asString := removeJunk(asString);
        next;
      end; {while}
    end; {with}
  finally
    endWork;
  end; {try..finally}
end;

end.
