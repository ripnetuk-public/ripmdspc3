program RipMDSPC3;

{$R 'icons.res' 'icons.rc'}

uses
  Forms,
  UfrmMain in 'UfrmMain.pas' {frmMain},
  UfrTrackList in 'UfrTrackList.pas' {frTrackList: TFrame},
  UfrmPlayTrack in 'UfrmPlayTrack.pas' {frmPlayTrack},
  UfrmRipCountdown in '..\RipLib\UfrmRipCountdown.pas' {frmRipCountdown},
  URipTimeString in '..\RipLib\URipTimeString.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Rip MDS-PC3';
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
